# FASC OpenApi Go SDK v5.1 说明

# 简介

欢迎使用法大大开发者工具套件（SDK），Go SDK 是法大大电子合同和电子签云服务开放平台（FASC OPEN API）的配套工具。



# 版本说明

FASC.openAPI 产品目前存在两个子版本号：v5.0、v5.1， 均在持续迭代维护。 

当前页面SDK基于FASC.openAPI v5.1子版本开发，如需使用FASC.openAPI v5.0版本SDK，请访问：

https://gitee.com/fadada-cloud/fasc-openapi-go-sdk/tree/v5.0



# 目录结构
- SDK项目层级     
```go
client  // 为客户端 。
common  // 包含了http,hash工具类方法 和全局常量等。
model   // 主要常用数据模型定义 。
test    // 里面是接口的调用示例，接入方开发人员可以参考    
```

- SDK 目前支持以下模块，对应 client 可支持具体的业务方法：

| 模块               | 模块中文名       | 模块说明                                                     |
| ------------------ | ---------------- | ------------------------------------------------------------ |
| UserManageClient   | 个人用户管理     | 包含应用的个人用户信息查询、禁用、恢复、解绑、获取授权链接 |
| CorpManageClient   | 企业用户账号管理 | 包含应用的企业用户信息查询、禁用、恢复、解绑   |
| DocumentManageClient  | 文件及模板管理         | 包含网络文件上传、获取文件上传地址、文件处理<br />包含文档模板查询、签署模板查询   |
| SignTaskClient        | 签署任务管理     | 包含签署任务的创建、维护、各个流程节点的流转操作，以及签署任务文件下载 |
| EUIClient            | EUI页面链接管理  | 对EUI页面链接进行管理操作，如获取个人授权链接、获取企业授权链接、获取计费链接、获取签署任务编辑链接、印章管理、组织管理链接等 |


# 依赖环境

go 1.16 版本及以上

# 调用示例
```go
package main

import (
    "fmt"
    "gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
)

func main()  {
    //获取accessToken
    c := client.NewClient("AppId值","AppSecret值","请求api地址")
    accessTokenRes := c.GetAuthToken()
    fmt.Println(accessTokenRes.Data.AccessToken)
}
```

第一步：导入包gitee.com/fadada-cloud/fasc-openapi-go-sdk/client
第二步：终端下执行go mod tidy 下载sdk依赖包
第三步：开始调用

更多调用示例详情请参考代码目录test。



# 版本更新日志

5.1.0 - 2022-07-28   基于FASC OpenAPI 5.1.0版本开发，初始版本。



# 参考

FASC OpenAPI (服务端) 接口文档 v5.1

https://dev.fadada.com/api-doc/MTE9YIK1SP/QMMRYYN5RMPREAZH/5-1 