package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/draftRequestModel"
	"testing"
)

// 发起合同协商
func TestDraftCreate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.DraftCreateReq{
		OpenCorpId:        "",
		InitiatorMemberId: "",
		ContractSubject:   "",
		FileId:            "",
	}

	res := c.DraftCreate(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取合同协商邀请链接
func TestDraftGetInviteUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.GetDraftInviteUrlReq{
		ContractConsultId: "",
		ExpireTime:        "",
		InnerPermission:   "",
		OuterPermission:   "",
		TouristView:       "",
		RedirectUrl:       "",
	}

	res := c.DraftGetInviteUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取合同协商编辑链接
func TestDraftGetEditUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.GetDraftEditUrlReq{
		OpenCorpId:        "",
		ClientUserId:      "",
		ContractConsultId: "",
		UrlType:           "",
		RedirectUrl:       "",
	}

	res := c.DraftGetEditUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取合同起草管理链接
func TestDraftGetManageUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.GetDraftManageUrlReq{
		OpenCorpId:   "",
		ClientUserId: "",
		RedirectUrl:  "",
	}

	res := c.DraftGetManageUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 合同协商文件定稿
func TestDraftFinalize(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.DocFinalizeReq{
		ContractConsultId: "",
	}

	res := c.DraftFinalize(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询已定稿的合同文件
func TestDraftGetFinishFile(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &draftRequestModel.GetDraftFinishedFileReq{ContractConsultId: ""}

	res := c.DraftGetFinishFile(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
