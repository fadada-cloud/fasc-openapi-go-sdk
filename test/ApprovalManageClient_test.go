package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/approvalRequestModel"
	"testing"
)

// 查询审批列表
func TestGetApprovalListUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &approvalRequestModel.GetApprovalListReq{
		OpenCorpId:   OpenCorpId,
		ApprovalType: "sign_task_start",
		ListPageNo:   1,
		ListPageSize: 10,
	}

	res := c.GetApprovalList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询审批详情
func TestGetApprovalDetailUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &approvalRequestModel.GetApprovalDetailReq{
		OpenCorpId:   OpenCorpId,
		ApprovalType: "sign_task_start",
		ApprovalId:   "1686813279919185045",
	}

	res := c.GetApprovalDetail(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取审批链接
func TestGetApprovalUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &approvalRequestModel.GetApprovalUrlReq{
		OpenCorpId:   "",
		ClientUserId: "",
		ApprovalId:   "",
		RedirectUrl:  "",
	}
	res := c.GetApprovalUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询审批流程列表
func TestGetApprovalFlowList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &approvalRequestModel.GetApprovalFlowListReq{
		OpenCorpId:   OpenCorpId,
		ApprovalType: "",
	}
	res := c.GetApprovalFlowList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询审批流程详情
func TestGetApprovalFlowDetail(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &approvalRequestModel.GetApprovalFlowDetailReq{
		OpenCorpId:     OpenCorpId,
		ApprovalFlowId: "1670479611903115468",
		ApprovalType:   "sign_task_finalize",
	}
	res := c.GetApprovalFlowDetail(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
