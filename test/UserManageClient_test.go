package test

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/userManageRequestModel"
	"strconv"
	"testing"
	"time"
)

func TestGetUserAuthUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.GetUserAuthUrlReq{
		ClientUserId:       ClientUserId + strconv.FormatInt(time.Now().Unix(), 10),
		UserName:           "",
		UserIdentType:      "",
		UserIdentNo:        "",
		UserIdentInfoMatch: false,
		AuthScopes:         []string{"ident_info", "signtask_info", "seal_info", "signtask_init", "signtask_file"},
		RedirectUrl:        "http://www.baidu.com",
		AccountName:        "",
		UnbindAccount:      true,
		UserIdentInfo: &userManageRequestModel.UserIdentInfo{
			UserName:      "",
			UserIdentType: "",
			UserIdentNo:   "",
			Mobile:        "",
			BankAccountNo: "",
			IdentMethod:   []string{},
		},
		NonEditableInfo: []string{},
		FreeSignInfo: &userManageRequestModel.FreeSignInfo{
			BusinessId: "23124",
			Email:      "2342424@qq.com",
		},
	}
	res := c.GetUserAuthUrlResponse(req, accessToken)
	jsonData, _ := json.Marshal(res)
	fmt.Println(string(jsonData))
}

// 禁用个人用户
func TestGetUserDisableResponse(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.UserDisableReq{
		OpenUserId: OpenUserId,
	}
	response := c.GetUserDisableResponse(req, accessToken)
	jsonData, _ := json.Marshal(response)
	fmt.Println(string(jsonData))
}

// 回复个人用户
func TestGetUserEnableResponse(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.UserEnableReq{
		OpenUserId: OpenUserId,
	}
	response := c.GetUserEnableResponse(req, accessToken)
	jsonData, _ := json.Marshal(response)
	fmt.Println(string(jsonData))
}

// 解绑个人用户
func TestGetUnBindUserResponse(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.UnBindUserReq{
		OpenUserId: OpenUserId,
	}
	response := c.GetUnBindUserResponse(req, accessToken)
	jsonData, _ := json.Marshal(response)
	fmt.Println(string(jsonData))
}

// 查询个人用户基本信息
func TestGetUserInfoResponse(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.GetUserReq{
		OpenUserId:   OpenUserId,
		ClientUserId: "",
	}
	response := c.GetUserInfoResponse(req, accessToken)
	jsonData, _ := json.Marshal(response)
	fmt.Println(string(jsonData))
}

// 获取个人用户身份信息
func TestGetUserIdentityResponse(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &userManageRequestModel.GetUserIdentifyInfoReq{
		OpenUserId: OpenUserId,
	}
	response := c.GetUserIdentityResponse(req, accessToken)
	jsonResData, _ := json.Marshal(response)
	fmt.Println(string(jsonResData))
}
