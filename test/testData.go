package test

const (
	APPID             = "APPID值"
	APPSECRET         = "APPSecret值"
	SERVERURL         = "服务请求api地址"
	ClientCorpId      = "企业主体在应用中的唯一标识"
	OpenCorpId        = "企业id"
	ClientUserId      = "个人主体在应用中的唯一标识"
	OpenUserId        = "个人id"
	DocTemplateId     = "文档模板id"
	SignTemplateId    = "签署模板id"
	AppDocTemplateId  = "应用文档模板id"
	AppSignTemplateId = "应用签署模板id"
	DocFileId         = "文档field"
	AttachFileId      = "签署任务附件field"
	AttachId          = "签署任务内指定附件序号"
	SignTaskId        = "签署任务id"
)
