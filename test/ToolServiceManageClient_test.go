package test

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/toolServiceManageRequestModel"
	"testing"
)

// 获取个人三要素链接
func TestGetThreeElementVerifyUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetThreeElementVerifyUrlReq{
		ClientUserId: "ClientUserId34304924",
		UserName:     "",
		UserIdentNo:  "",
		Mobile:       "",
		RedirectUrl:  "http://www.baidu.com",
	}
	response := c.GetThreeElementVerifyUrl(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 获取个人四要素链接
func TestGetFourElementVerifyUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetFourElementVerifyUrlReq{
		ClientUserId:  "ClientUserId34304924",
		UserName:      "",
		UserIdentNo:   "",
		IdCardImage:   true,
		BankAccountNo: "",
		Mobile:        "13138799005",
		RedirectUrl:   "http://www.baidu.com",
	}
	response := c.GetFourElementVerifyUrl(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 三四要素下载图片
func TestGetIdCardImageDownloadUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetIdCardImageDownloadUrlReq{VerifyId: "62645ca9defc44b4ac91ce6f6de05f4c"}
	response := c.GetIdCardImageDownloadUrl(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 身份证OCR
func TestUserIdCardOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.UserIdCardOcrReq{
		FaceSide:           "",
		NationalEmblemSide: "",
	}
	response := c.UserIdCardOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 个人二要素校验
func TestIdentityTwoElementsVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.IdentityTwoElementVerifyReq{
		UserName:    "",
		UserIdentNo: "",
	}
	response := c.IdentityTwoElementsVerify(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 个人运营商三要素校验
func TestTelecomThreeElementsVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.TelecomThreeElementVerifyReq{
		UserName:    "",
		UserIdentNo: "",
		Mobile:      "",
	}
	response := c.TelecomThreeElementsVerify(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 个人银行卡四要素校验
func TestBankFourElementsVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.BankFourElementVerifyReq{
		UserName:      "成",
		UserIdentNo:   "",
		Mobile:        "",
		BankAccountNo: "",
	}
	response := c.BankFourElementsVerify(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 个人银行卡三要素校验
func TestBankThreeElementVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.BankThreeElementVerifyReq{
		UserName:      "福成",
		UserIdentNo:   "",
		BankAccountNo: "",
	}
	response := c.BankThreeElementVerify(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 人脸图片比对校验
func TestIdentityIdCardThreeElementVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetIdCardThreeElementVerifyReq{
		UserName:    "",
		UserIdentNo: "",
		ImgBase64:   "",
	}
	response := c.IdentityIdCardThreeElementVerify(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 获取人脸核验链接
func TestFaceRecognition(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetFaceRecognitionUrlReq{
		UserName:     "周福成",
		UserIdentNo:  "440981199904038619",
		FaceAuthMode: "TENCENT",
		RedirectUrl:  "http://www.baidu.com",
	}

	response := c.FaceRecognition(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 获取人脸核验状态详情
func TestFaceRecognitionGetStatus(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetFaceRecognitionStatusReq{
		SerialNo: "ce1ed35e2fba2734dfbca9c3e429751a",
		GetFile:  2,
	}

	response := c.FaceRecognitionGetStatus(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 银行卡OCR
func TestBankcardOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.BankCardOcrReq{
		ImageBase64: "",
	}

	response := c.BankcardOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 营业执照OCR
func TestLicenseCardOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.LicenseOcrReq{
		ImageBase64: "",
	}
	response := c.LicenseCardOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 驾驶证OCR
func TestDrivinglicenseOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.DriveLicenseOcrReq{
		ImageBase64:     "",
		BackImageBase64: "",
	}

	response := c.DrivinglicenseOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 行驶证OCR
func TestVehiclelicenseOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.VehicleLicenseOcrReq{
		ImageBase64:     "",
		BackImageBase64: "",
	}

	response := c.VehiclelicenseOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 通行证OCR
func TestMainlandPermitOcr(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.MainlandPermitOcrReq{
		ImageBase64:     "",
		BackImageBase64: "",
	}
	response := c.MainlandPermitOcr(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 银行卡四要素核验-创建订单
func TestVerifyFourElementTransactionCreate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetBankcardFourElementTransactionIdReq{
		UserName:       "",
		UserIdentNo:    "",
		BankAccountNo:  "",
		Mobile:         "",
		CreateSerialNo: "22dfd23fsf2",
	}
	response := c.VerifyFourElementTransactionCreate(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 手机号三要素核验-创建订单
func TestVerifyThreeElementTransactionCreate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetMobileThreeElementTransactionIdReq{
		UserName:       "",
		UserIdentNo:    "",
		Mobile:         "",
		CreateSerialNo: "22dfd23fsf2",
	}
	response := c.VerifyThreeElementTransactionCreate(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 验证码校验
func TestVerifyAuthCodeCheck(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.VerifyAuthCodeReq{
		TransactionId: "22dfd23fsf2",
		AuthCode:      "234242",
	}
	response := c.VerifyAuthCodeCheck(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 获取验证码
func TestGetVerifyAuthCode(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetAuthCodeReq{TransactionId: "234242"}
	response := c.GetVerifyAuthCode(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 获取身份核验详情
func TestVerifyGetDetailByAuthCode(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetVerifyDetailReq{TransactionId: "sfsffsfsfs"}
	response := c.VerifyGetDetailByAuthCode(req, accessToken)
	fmt.Println(ModelToJsonString(response, false))
}

// 企业组织三要素校验
func TestGetBusinessThreeElementVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetBusinessThreeElementReq{
		CorpName:     "企业",
		CorpIdentNo:  "91230227MA1D0WA28L",
		LegalRepName: "243242",
	}
	response := c.GetBusinessThreeElementVerify(req, accessToken)
	jsonResData, _ := json.Marshal(response)
	fmt.Println(string(jsonResData))
}

// 企业组织四要素校验
func TestGetBusinessFourElementVerify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetBusinessFourElementReq{
		CorpName:         "企业",
		CorpIdentNo:      "91230227MA1D0WA28L",
		LegalRepName:     "243242",
		LegalRepIdCertNo: "440981199904038619",
	}
	response := c.GetBusinessFourElementVerify(req, accessToken)
	jsonResData, _ := json.Marshal(response)
	fmt.Println(string(jsonResData))
}

// 企业工商信息查询
func TestGetBusinessInformation(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	req := &toolServiceManageRequestModel.GetBusinessInfoReq{
		CorpName:    "深圳法大大网络科技有限公司",
		CorpIdentNo: "914403003195749464",
	}
	response := c.GetBusinessInformation(req, accessToken)
	jsonResData, _ := json.Marshal(response)
	fmt.Println(string(jsonResData))
}
