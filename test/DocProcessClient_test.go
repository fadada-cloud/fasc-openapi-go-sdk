package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/docProcessRequestModel"
	"testing"
)

// 通过网络文件地址上传
func TestGetFddFileUploadUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.UploadFileByUrlReq{
		FileType: "doc",
		FileUrl:  "http://static.fadada.com/fortest20170901/协议书范本.pdf",
	}

	res := c.GetFddFileUploadUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取本地文件上传url
func TestGetLocalFileUploadUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetLocalUploadFileUrlReq{
		FileType: "doc",
	}

	res := c.GetLocalFileUploadUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 本地文件上传
func TestPutFileUpload(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	er := c.PutFileUpload("D:\\Desktop\\测试合同.docx", "https://doc-test-os1.fadada.com/6c0bcadb51f34e3eb01710927ff28f7e?sign=q-sign-algorithm%3Dsha1%26q-ak%3DAKIDUmoZ3GI3XkgKhmb9QP9dqlPmshFXXGit%26q-sign-time%3D1709517684%3B1709518584%26q-key-time%3D1709517684%3B1709518584%26q-header-list%3Dhost%26q-url-param-list%3D%26q-signature%3D552c1d7ecd54234e9952a10b510c23978b1b4a3a")
	if er != nil {
		fmt.Println("文件put失败")
	}
}

// 文件处理
func TestFileProcess(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.FileProcessReq{FddFileUrlList: []docProcessRequestModel.FddUploadFile{
		{FileType: "doc",
			FddFileUrl: "https://doc-test-os1.fadada.com/6c0bcadb51f34e3eb01710927ff28f7e",
			FileName:   "测试模板.docx",
			//FileFormat: "ofd",
		},
	}}

	res := c.FileProcess(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 文档验签
func TestVerifyFileSign(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.VerifyFileSignReq{
		FileId:   DocFileId,
		FileHash: "fhaoifhiowhofowfohwfohowohfwhohwfo",
	}
	res := c.VerifyFileSign(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取文件对比页面链接
func TestGetCompareUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetCompareUrlReq{
		Initiator: &commonModel.OpenId{
			IdType: "corp",
			OpenId: OpenCorpId,
		},
		OriginFileId: "",
		TargetFileId: "",
	}

	res := c.GetCompareUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取历史文件对比页面链接
func TestGetCompareResultUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetCompareResultUrlReq{
		Initiator: &commonModel.OpenId{
			IdType: "corp",
			OpenId: OpenCorpId,
		},
		CompareId: "",
	}

	res := c.GetCompareResultUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取合同智审页面链接
func TestGetExamineUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetExamineUrlReq{
		Initiator: &commonModel.OpenId{
			IdType: "corp",
			OpenId: OpenCorpId,
		},
		FileId: "",
	}

	res := c.GetExamineUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取历史合同智审页面链接
func TestGetExamineResultUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetExamineResultUrlReq{
		Initiator: &commonModel.OpenId{
			IdType: "corp",
			OpenId: OpenCorpId,
		},
		ExamineId: "",
	}

	res := c.GetExamineResultUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取历史合同智审数据
func TestGetOcrEditExamineResultData(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetOcrEditExamineResultDataReq{
		Initiator: &commonModel.OpenId{
			IdType: "",
			OpenId: "",
		},
		ExamineId: "",
	}

	res := c.GetOcrEditExamineResultData(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取历史合同比对数据
func TestGetOcrEditCompareResultData(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetCompareResultDataReq{
		Initiator: &commonModel.OpenId{
			IdType: "",
			OpenId: "",
		},
		CompareId: "",
	}

	res := c.GetOcrEditCompareResultData(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询文档关键字坐标
func TestGetKeywordPositions(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &docProcessRequestModel.GetKeywordPositionReq{
		FileId:   "",
		Keywords: []string{"一页"},
	}

	res := c.GetKeywordPositions(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
