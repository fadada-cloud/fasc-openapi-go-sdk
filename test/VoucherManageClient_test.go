package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/voucherRequestModel"
	"testing"
)

// 创建单据
func TestCreateVoucherTask(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.VoucherSignTaskCreateReq{
		Initiator: &voucherRequestModel.OpenId{
			IdType: "corp",
			OpenId: "c5aa3ea8db994fb3b8138a5ba86a63f4",
		},
		SignTaskSubject: "原始文件发起V5.1签署任务-企业1694680462116",
		Docs: []voucherRequestModel.Doc{
			voucherRequestModel.Doc{
				DocId:         "docId",
				DocName:       "原始文件发起V5.1签署任务",
				DocFileId:     "1656590076218129298",
				DocTemplateId: "",
				DocFields: []voucherRequestModel.DocField{
					voucherRequestModel.DocField{
						FieldId:   "单行文本",
						FieldName: "单行文本",
						FieldType: "text_single_line",
						FieldKey:  "",
						Position: &voucherRequestModel.Position{
							PositionMode:   "pixel",
							PositionPageNo: 1,
							PositionX:      "100",
							PositionY:      "100",
						},
						Moveable:        false,
						FieldCorpSeal:   nil,
						FieldPersonSign: nil,
						FieldRemarkSign: nil,
						FieldTextSingleLine: &voucherRequestModel.FieldTextSingleLine{
							DefaultValue: "",
							Tips:         "",
							Required:     false,
							FontType:     "",
							FontSize:     12,
							Width:        100,
							Height:       50,
							Alignment:    "",
						},
						FieldTextMultiLine: nil,
						FieldNumber:        nil,
						FieldIdCard:        nil,
						FieldFillDate:      nil,
						FieldSelectBox:     nil,
						FieldPicture:       nil,
						FieldTable:         nil,
						FieldMultiCheckbox: nil,
					},
					voucherRequestModel.DocField{
						FieldId:   "个人签名",
						FieldName: "个人签名",
						FieldType: "person_sign",
						FieldKey:  "",
						Position: &voucherRequestModel.Position{
							PositionMode:   "pixel",
							PositionPageNo: 1,
							PositionX:      "200",
							PositionY:      "200",
						},
						Moveable:        false,
						FieldCorpSeal:   nil,
						FieldPersonSign: nil,
						FieldRemarkSign: nil,
						FieldTextSingleLine: &voucherRequestModel.FieldTextSingleLine{
							DefaultValue: "",
							Tips:         "",
							Required:     false,
							FontType:     "",
							FontSize:     12,
							Width:        100,
							Height:       50,
							Alignment:    "",
						},
						FieldTextMultiLine: nil,
						FieldNumber:        nil,
						FieldIdCard:        nil,
						FieldFillDate:      nil,
						FieldSelectBox:     nil,
						FieldPicture:       nil,
						FieldTable:         nil,
						FieldMultiCheckbox: nil,
					},
				},
			},
		},
		Actors: []voucherRequestModel.Actor{
			voucherRequestModel.Actor{
				ActorId:   "个人方",
				ActorName: "个人方",
				FillFields: []voucherRequestModel.FillField{
					voucherRequestModel.FillField{
						FieldDocId: "docId",
						FieldId:    "单行文本",
					},
				},
				SignFields: []voucherRequestModel.SignField{
					voucherRequestModel.SignField{
						FieldDocId: "docId",
						FieldId:    "个人签名",
						FieldName:  "",
					},
				},
				SignConfigInfo: nil,
			},
		},
		Attachs: nil,
	}

	res := c.CreateVoucherTask(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询单据签署任务列表
func TestQueryVoucherTaskList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.QueryVoucherTaskListReq{
		Initiator: &voucherRequestModel.OpenId{
			IdType: "corp",
			OpenId: "c5aa3ea8db994fb3b8138a5ba86a63f4",
		},
		ListFilter:   nil,
		ListPageNo:   1,
		ListPageSize: 10,
	}

	res := c.QueryVoucherTaskList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询单据签署任务详情
func TestQueryVoucherTaskDetail(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.QueryVoucherTaskDetailReq{SignTaskId: "1702252815770619904"}

	res := c.QueryVoucherTaskDetail(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 下载单据签署任务
func TestGetVoucherTaskDownloadUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.GetVoucherTaskDownloadUrlReq{
		OwnerId: &voucherRequestModel.OpenId{
			IdType: "corp",
			OpenId: "c5aa3ea8db994fb3b8138a5ba86a63f4",
		},
		SignTaskId: "1702252815770619904",
		FileType:   "",
		Id:         "",
		CustomName: "",
	}
	res := c.GetVoucherTaskDownloadUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 撤销单据签署任务
func TestVoucherTaskCancel(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.CancelVoucherTaskReq{
		SignTaskId:      "1702252815770619904",
		TerminationNote: "",
	}
	res := c.VoucherTaskCancel(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取单据签署链接
func TestGetVoucherActorUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &voucherRequestModel.GetVoucherTaskActorUrlReq{
		SignTaskId:  "1702252815770619904",
		ActorId:     "个人方",
		RedirectUrl: "",
	}
	res := c.GetVoucherActorUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
