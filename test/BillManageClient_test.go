package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/billManageRequestModel"
	"testing"
)

// 获取计费链接
func TestGetBillUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &billManageRequestModel.GetBillingUrlReq{
		ClientUserId: "AutoClientIdUser1684377326820",
		OpenId: &commonModel.OpenId{
			IdType: "corp",
			OpenId: OpenCorpId,
		},
		UrlType:     "",
		RedirectUrl: "",
	}

	res := c.GetBillUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
