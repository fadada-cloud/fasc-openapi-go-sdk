package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/archivesRequestModel"
	"testing"
)

// 合同归档
func TestContactArchived(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.ContactArchivedReq{
		OpenCorpId:   "",
		ContractType: "",
		ContractNo:   "",
		CatalogId:    "",
		SignTaskId:   "",
		FileId:       "",
		MemberId:     "",
		Attachments:  nil,
	}
	res := c.ContactArchived(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询已归档合同列表
func TestGetArchivedList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.ArchivedListReq{
		OpenCorpId:   "",
		ContractType: "",
		SignTaskId:   "",
		ListPageNo:   1,
		ListPageSize: 100,
	}
	res := c.GetArchivedList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询归档文件夹列表
func TestGetArchivesCatalogList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.ArchivesCatalogListReq{
		OpenCorpId: "",
		CatalogId:  "",
	}
	res := c.GetArchivesCatalogList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询归档详情
func TestGetArchivesDetail(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.ArchivesDetailReq{
		OpenCorpId: "",
		ArchivesId: "",
	}
	res := c.GetArchivesDetail(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 履约修改
func TestPerformanceModify(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.CreateOrModifyPerformanceReq{
		OpenCorpId:      OpenCorpId,
		ArchivesId:      "1721778441299111936",
		PerformanceId:   "",
		PerformanceType: "pay",
		PerformanceName: "pay提醒1111",
		ExpireTime:      "1701309945000",
		RemindStartDate: "1698922394000",
		RemindFrequency: "daily",
		CycleDays:       1,
		RemindTime:      "",
		Amount:          "5.1",
		Reminder:        []string{"1659338591596134709"},
		IsRemind:        false,
	}
	res := c.PerformanceModify(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 履约删除
func TestPerformanceDelete(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.DeletePerformanceReq{
		OpenCorpId:    OpenCorpId,
		ArchivesId:    "",
		PerformanceId: "1714560642832994306",
	}
	res := c.PerformanceDelete(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询履约列表
func TestPerformanceGetList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &archivesRequestModel.GetPerformanceListReq{
		OpenCorpId: OpenCorpId,
		MemberId:   "",
		ArchivesId: "",
	}
	res := c.PerformanceGetList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
