package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/callbackRequestModel"
	"testing"
)

// 查询回调列表
func TestGetCallBackList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &callbackRequestModel.GetCallBackListReq{
		CallbackType: "approval",
		StartTime:    "1681460275000",
		EndTime:      "1686730675824",
		ListPageNo:   1,
		ListPageSize: 10,
	}

	res := c.GetCallBackList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
