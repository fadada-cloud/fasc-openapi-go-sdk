package test

import (
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/client"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/sealRequestModel"
	"strconv"
	"testing"
	"time"
)

// 创建模板印章
func TestCreateSealByTemplate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.CreateSealByTemplateReq{
		OpenCorpId:         OpenCorpId,
		SealName:           "印章名称1",
		CategoryType:       "hr_seal",
		SealTag:            "印章Tag",
		SealTemplateStyle:  "oval",
		SealSize:           "round_40",
		SealHorizontalText: "横排文字ZFC",
		SealBottomText:     "ZFC",
		SealColor:          "black",
		CreateSerialNo:     "CreateSerialNo4242ZFC",
	}
	res := c.CreateSealByTemplate(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 创建图片印章
func TestCreateSealByImage(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	var sealWidth = 45
	var sealHeight = 45
	var sealOldStyle = 1.6

	req := &sealRequestModel.CreateSealByImageReq{
		OpenCorpId:     OpenCorpId,
		SealName:       "SealNameZFC",
		CategoryType:   "other",
		SealTag:        "SealTagZFC",
		CreateSerialNo: "CreateSerialNo4234ZFC",
		SealImage:      "",
		SealWidth:      &sealWidth,
		SealHeight:     &sealHeight,
		SealOldStyle:   &sealOldStyle,
		SealColor:      "red",
	}
	res := c.CreateSealByImage(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 创建法定代表人模板印章
func TestCreateLegalRepresentativeSealByTemplate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.CreateLegalRepresentativeSealByTemplateReq{
		OpenCorpId:        OpenCorpId,
		OpenUserId:        OpenUserId,
		SealName:          "SealNameZFC",
		SealTag:           "SealTagZFC",
		SecurityCode:      "",
		CreateSerialNo:    "CreateSerialNo24224",
		SealTemplateStyle: "",
		SealSize:          "",
		SealSuffix:        "name",
		SealColor:         "red",
	}
	res := c.CreateLegalRepresentativeSealByTemplate(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 创建法定代表人图片印章
func TestCreateLegalRepresentativeSealByImage(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken
	var sealWidth = 45
	var sealHeight = 45
	var sealOldStyle = 1.6

	req := &sealRequestModel.CreateLegalRepresentativeSealByImageReq{
		OpenCorpId:     OpenCorpId,
		OpenUserId:     OpenUserId,
		SealName:       "SealName名称",
		SealTag:        "SealTagTag",
		CreateSerialNo: "CreateSerialNo23942323",
		SealImage:      "",
		SealWidth:      &sealWidth,
		SealHeight:     &sealHeight,
		SealOldStyle:   &sealOldStyle,
		SealColor:      "",
	}
	res := c.CreateLegalRepresentativeSealByImage(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询印章列表
func TestGetSealList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealListReq{
		OpenCorpId: OpenCorpId,
		ListFilter: &sealRequestModel.SealListFilter{
			CategoryType: []string{},
			SealTags:     []string{"印章标签"},
		},
	}
	res := c.GetSealList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询印章详情
func TestGetSealDetail(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealDetailReq{
		OpenCorpId: OpenCorpId,
		SealId:     1690969771442173103,
	}

	res := c.GetSealDetail(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取指定印章详情链接
func TestGetAppointedSeal(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetAppointedSealUrlReq{
		OpenCorpId:   OpenCorpId,
		SealId:       0,
		ClientUserId: "clientUserId" + strconv.FormatInt(time.Now().Unix(), 10),
		RedirectUrl:  "http://www.baidu.com",
	}

	res := c.GetAppointedSeal(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询企业用印员列表
func TestGetSealUserList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealUserListReq{
		OpenCorpId: OpenCorpId,
		ListFilter: &sealRequestModel.SealListFilter{
			[]string{
				"official_seal",
			},
			[]string{}},
	}

	res := c.GetSealUserList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询指定成员的印章列表
func TestGetUserSealList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetUserSealListReq{
		OpenCorpId: OpenCorpId,
		MemberId:   1621394271628156700,
	}

	res := c.GetUserSealList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取印章创建链接
func TestGetSealCreate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealCreateUrlReq{
		OpenCorpId:     OpenCorpId,
		SealName:       "",
		CategoryType:   "",
		SealTag:        "",
		CreateSerialNo: "",
		ClientUserId:   "clientUserId" + strconv.FormatInt(time.Now().Unix(), 10),
		RedirectUrl:    "",
	}

	res := c.GetSealCreate(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询审核中的印章列表
func TestGetVerifySealList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealVerifyListReq{
		OpenCorpId: OpenCorpId,
	}

	res := c.GetVerifySealList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 修改印章基本信息
func TestModifySealInfo(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.ModifySealReq{
		OpenCorpId:   OpenCorpId,
		SealId:       0,
		SealName:     "修改后的印章名称",
		SealTag:      "",
		CategoryType: "",
	}

	res := c.ModifySealInfo(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取设置用印员链接
func TestGetSealGrant(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealGrantUrlReq{
		OpenCorpId: OpenCorpId,
		SealId:     0,
		MemberInfo: &commonModel.MemberInfo{
			MemberIds:      []int64{},
			GrantStartTime: "",
			GrantEndTime:   "",
		},
		ClientUserId: "clientUserId" + strconv.FormatInt(time.Now().Unix(), 10),
		RedirectUrl:  "http://www.baidu.com",
	}

	res := c.GetSealGrant(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取设置企业印章免验证签链接
func TestGetSealFreeSign(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealFreeSignUrlReq{
		OpenCorpId:         OpenCorpId,
		SealIds:            []int64{1669342356577150613},
		BusinessId:         BusinessId,
		Email:              "",
		ClientUserId:       "clientUserId" + strconv.FormatInt(time.Now().Unix(), 10),
		RedirectUrl:        "",
		RedirectMiniAppUrl: "24324234",
	}

	res := c.GetSealFreeSign(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 解除印章授权
func TestCancelCorpSealFreeSign(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	sealId := int64(1660640001534147)
	req := &sealRequestModel.CancelCorpSealFreeSignReq{
		OpenCorpId: OpenCorpId,
		SealId:     &sealId,
		BusinessId: "da0c95c4b5806f4ed489e4b4e8cf2710",
	}
	res := c.CancelCorpSealFreeSign(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 解除印章授权
func TestCancelSealGrant(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.CancelSealGrantReq{
		OpenCorpId: OpenCorpId,
		SealId:     0,
		MemberId:   0,
	}

	res := c.CancelSealGrant(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 设置印章状态
func TestSetSealStatus(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.SetSealStatusReq{
		OpenCorpId: OpenCorpId,
		SealId:     0,
		SealStatus: "disable",
	}

	res := c.SetSealStatus(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 删除印章
func TestDeleteSeal(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.SealDeleteReq{
		OpenCorpId: OpenCorpId,
		SealId:     0,
	}

	res := c.DeleteSeal(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取印章标签列表
func TestGetSealTagList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealTagListReq{OpenCorpId: OpenCorpId}
	res := c.GetSealTagList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取印章管理链接
func TestGetSealManage(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetSealManageUrlReq{
		OpenCorpId:   OpenCorpId,
		ClientUserId: "clientUserId" + strconv.FormatInt(time.Now().Unix(), 10),
		RedirectUrl:  "",
	}

	res := c.GetSealManage(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 创建模板签名
func TestCreatePersonalSealByTemplate(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.CreatePersonalSealByTemplateReq{
		OpenUserId:        OpenUserId,
		SealName:          "SealNamePerson",
		CreateSerialNo:    "CreateSerialNo242342",
		SealTemplateStyle: "square_left_bigword",
		SealSize:          "square_18_18",
		SealSuffix:        "name_with_suffix_seal",
		SealColor:         "black",
	}

	res := c.CreatePersonalSealByTemplate(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 创建图片签名
func TestCreatePersonalSealByImage(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	var sealWidth = 45
	var sealHeight = 45
	var sealOldStyle = 1.6

	req := &sealRequestModel.CreatePersonalSealByImageReq{
		OpenUserId:     OpenUserId,
		SealName:       "SealNameTest",
		CreateSerialNo: "",
		SealImage:      "",
		SealWidth:      &sealWidth,
		SealHeight:     &sealHeight,
		SealOldStyle:   &sealOldStyle,
		SealColor:      "red",
	}

	res := c.CreatePersonalSealByImage(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 查询个人签名列表
func TestGetPersonalSealList(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetPersonalSealListReq{
		OpenUserId: OpenUserId,
	}

	res := c.GetPersonalSealList(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取设置个人签名免验证签链接
func TestGetPersonalSealFreeSign(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetPersonalSealFreeSignUrlReq{
		OpenUserId:         "aeeb1f1bf6f349e89915a9701793d1bd",
		SealIds:            []int64{1669342356577150613},
		BusinessId:         BusinessId,
		ExpiresTime:        "",
		Email:              "",
		RedirectUrl:        "",
		RedirectMiniAppUrl: "21342424",
	}

	res := c.GetPersonalSealFreeSign(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 解除印章授权
func TestCancelPersonSealFreeSign(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.CancelPersonSealFreeSignReq{
		OpenUserId: OpenUserId,
		SealId:     nil,
		BusinessId: "",
	}
	res := c.CancelPersonSealFreeSign(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取签名管理链接
func TestGetPersonSealManageUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetPersonSealManageUrlReq{
		ClientUserId: "${clientUserId}",
		RedirectUrl:  "${redirectUrl}",
	}
	res := c.GetPersonSealManageUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 获取签名创建链接
func TestGetPersonSealCreateUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.GetPersonSealCreateUrlReq{
		ClientUserId:   "",
		CreateSerialNo: "",
		RedirectUrl:    "",
	}
	res := c.GetPersonSealCreateUrl(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}

// 删除签名
func TestDeletePersonSealUrl(t *testing.T) {
	c := client.NewClient(APPID, APPSECRET, SERVERURL)
	accessTokenRes := c.GetAuthToken()
	accessToken := accessTokenRes.Data.AccessToken

	req := &sealRequestModel.DeletePersonSealReq{
		OpenUserId: OpenUserId,
		SealId:     0,
	}
	res := c.DeletePersonSeal(req, accessToken)
	fmt.Println(ModelToJsonString(res, false))
}
