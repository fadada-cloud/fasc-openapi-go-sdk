package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/templateRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/templateResponseModel"
)

const (
	GetDocTemplateListReqUrl    = "/doc-template/get-list"        //查询文档模板列表
	GetDocTemplateDetailReqUrl  = "/doc-template/get-detail"      //获取文档模板详情
	SetDocTemplateStatusUrl     = "/doc-template/set-status"      //启用/停用主体文档模板
	CreateDocTemplateUrl        = "/doc-template/create"          //创建主体文档模板
	CopyCreateDocTemplateUrl    = "/doc-template/copy-create"     //复制创建文档模板
	FillValuesDocTemplateUrl    = "/doc-template/fill-values"     //填写文档模板生成文件
	DeleteDocTemplateUrl        = "/doc-template/delete"          //删除主体文档模板
	GetSignTemplateListReqUrl   = "/sign-template/get-list"       //获取签署模板列表
	GetSignTemplateDetailReqUrl = "/sign-template/get-detail"     //获取签署模板详情
	SetSignTemplateStatusUrl    = "/sign-template/set-status"     //启用/停用主体签署模板
	DeleteSignTemplateUrl       = "/sign-template/delete"         //删除主体签署模板
	CopyCreateSignTemplateUrl   = "/sign-template/copy-create"    //复制创建签署模板
	GetTemplateCreateUrl        = "/template/create/get-url"      //获取模板新增链接
	GetTemplateEditUrl          = "/template/edit/get-url"        //获取模板编辑链接
	GetTemplatePreviewUrl       = "/template/preview/get-url"     //获取模板预览链接
	GetTemplateManageUrl        = "/template/manage/get-url"      //获取模板管理链接
	GetAppDocTemplatesUrl       = "/app-doc-template/get-list"    //查询应用文档模板列表
	GetAppDocTemplateDetailUrl  = "/app-doc-template/get-detail"  //获取应用文档模板详情
	SetAppDocTemplateStatusUrl  = "/app-doc-template/set-status"  //启用或停用应用文档模板
	DeleteAppDocTemplateUrl     = "/app-doc-template/delete"      //删除应用文档模板
	GetAppSignTemplatesUrl      = "/app-sign-template/get-list"   //查询应用签署任务模板列表
	GetAppSignTemplateDetailUrl = "/app-sign-template/get-detail" //获取应用签署任务模板详情
	SetAppSignTemplateStatusUrl = "/app-sign-template/set-status" //启用或停用应用签署任务模板
	DeleteAppSignTemplateUrl    = "/app-sign-template/delete"     //删除应用签署任务模板
	GetAppTemplateCreateUrl     = "/app-template/create/get-url"  //获取应用模板新增链接
	GetAppTemplateEditUrl       = "/app-template/edit/get-url"    //获取应用模板编辑链接
	GetAppTemplatePreviewUrl    = "/app-template/preview/get-url" //获取应用模板预览链接
	CreateAppFieldUrl           = "/app-field/create"             //创建业务控件
	ModifyAppFieldUrl           = "/app-field/modify"             //修改业务控件
	SetAppFieldStatusUrl        = "/app-field/set-status"         //设置业务控件状态
	GetAppFieldListUrl          = "/app-field/get-list"           //查询业务控件列表
	CreateCorpFieldUrl          = "/corp-field/create"            //创建企业业务控件
	GetCorpFieldListUrl         = "/corp-field/get-list"          //获取企业业务控件列表信息
	DeleteCorpFieldUrl          = "/corp-field/delete"            //删除企业业务控件列表信息
)

// GetDocTemplateListResponse 查询文档模板列表
func (o *openApiClient) GetDocTemplateList(docTemListReq *templateRequestModel.GetDocTemplateListReq, accessToken string) templateResponseModel.GetDocTemplateListRes {
	var docTemListRes templateResponseModel.GetDocTemplateListRes
	reqStr, err := json.Marshal(docTemListReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetDocTemplateListReqUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &docTemListRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	docTemListRes.RequestId = requestId
	return docTemListRes
}

// GetDocTemplateDetailResponse 获取文档模板详情
func (o *openApiClient) GetDocTemplateDetail(docTemDetailReq *templateRequestModel.GetDocTemplateDetailReq, accessToken string) templateResponseModel.GetDocTemplateDetailRes {
	var docTemDetailRes templateResponseModel.GetDocTemplateDetailRes
	reqStr, err := json.Marshal(docTemDetailReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetDocTemplateDetailReqUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &docTemDetailRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	docTemDetailRes.RequestId = requestId
	return docTemDetailRes
}

// 启用/停用文档模板
func (o *openApiClient) SetDocTemplateStatus(req *templateRequestModel.SetDocTemplateStatusReq, accessToken string) templateResponseModel.SetDocTemplateStatusRes {
	var res templateResponseModel.SetDocTemplateStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetDocTemplateStatusUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建文档模板
func (o *openApiClient) CreateDocTemplate(req *templateRequestModel.CreateDocTemplateReq, accessToken string) templateResponseModel.CreateDocTemplateRes {
	var res templateResponseModel.CreateDocTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateDocTemplateUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 复制创建文档模板
func (o *openApiClient) CopyCreateDocTemplate(req *templateRequestModel.CopyCreateDocTemplateReq, accessToken string) templateResponseModel.CopyCreateDocTemplateRes {
	var res templateResponseModel.CopyCreateDocTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CopyCreateDocTemplateUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 填写文档模板生成文件
func (o *openApiClient) FillValuesDocTemplate(req *templateRequestModel.DocTemplateFillValuesReq, accessToken string) templateResponseModel.DocTemplateFillValuesRes {
	var res templateResponseModel.DocTemplateFillValuesRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FillValuesDocTemplateUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除文档模板
func (o *openApiClient) DeleteDocTemplate(req *templateRequestModel.DeleteDocTemplateReq, accessToken string) templateResponseModel.DeleteDocTemplateRes {
	var res templateResponseModel.DeleteDocTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteDocTemplateUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// GetSignTemplateListResponse 查询签署模板列表
func (o *openApiClient) GetSignTemplateList(signTemplateReq *templateRequestModel.GetSignTemplateListReq, accessToken string) templateResponseModel.GetSignTemplateListRes {
	var signTemplateRes templateResponseModel.GetSignTemplateListRes
	reqStr, err := json.Marshal(signTemplateReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTemplateListReqUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &signTemplateRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	signTemplateRes.RequestId = requestId
	return signTemplateRes
}

// GetSignTemplateDetailResponse 获取签署模板详情
func (o *openApiClient) GetSignTemplateDetail(signTemDetailReq *templateRequestModel.GetSignTemplateDetailReq, accessToken string) templateResponseModel.GetSignTemplateDetailRes {
	var signTemDetailRes templateResponseModel.GetSignTemplateDetailRes
	reqStr, err := json.Marshal(signTemDetailReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTemplateDetailReqUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &signTemDetailRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	signTemDetailRes.RequestId = requestId
	return signTemDetailRes
}

// 启用/停用签署模板
func (o *openApiClient) SetSignTemplateStatus(req *templateRequestModel.SetSignTemplateStatusReq, accessToken string) templateResponseModel.SetSignTemplateStatusRes {
	var res templateResponseModel.SetSignTemplateStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetSignTemplateStatusUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除文档模板
func (o *openApiClient) DeleteSignTemplate(req *templateRequestModel.DeleteSignTemplateReq, accessToken string) templateResponseModel.DeleteSignTemplateRes {
	var res templateResponseModel.DeleteSignTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTemplateUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 复制创建签署模板
func (o *openApiClient) CopyCreateSignTemplate(req *templateRequestModel.CopyCreateSignTemplateReq, accessToken string) templateResponseModel.CopyCreateSignTemplateRes {
	var res templateResponseModel.CopyCreateSignTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CopyCreateSignTemplateUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取模板新增链接
func (o *openApiClient) GetTemplateCreate(req *templateRequestModel.GetTemplateCreateUrlReq, accessToken string) templateResponseModel.GetTemplateCreateUrlRes {
	var res templateResponseModel.GetTemplateCreateUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetTemplateCreateUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取模板编辑链接
func (o *openApiClient) GetTemplateEdit(req *templateRequestModel.GetTemplateEditUrlReq, accessToken string) templateResponseModel.GetTemplateEditUrlRes {
	var res templateResponseModel.GetTemplateEditUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetTemplateEditUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取模板预览链接
func (o *openApiClient) GetTemplatePreview(req *templateRequestModel.GetTemplatePreviewUrlReq, accessToken string) templateResponseModel.GetTemplatePreviewUrlRes {
	var res templateResponseModel.GetTemplatePreviewUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetTemplatePreviewUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取模板管理链接
func (o *openApiClient) GetTemplateManage(req *templateRequestModel.GetTemplateManageUrlReq, accessToken string) templateResponseModel.GetTemplateManageUrlRes {
	var res templateResponseModel.GetTemplateManageUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetTemplateManageUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询应用文档模板列表
func (o *openApiClient) GetAppDocTemplates(req *templateRequestModel.GetAppDocTemplateListReq, accessToken string) templateResponseModel.AppDocTemplatePageListRes {
	var res templateResponseModel.AppDocTemplatePageListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppDocTemplatesUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取应用文档模板详情
func (o *openApiClient) GetAppDocTemplateDetail(req *templateRequestModel.GetAppDocTemplateDetailReq, accessToken string) templateResponseModel.AppDocTemplateDetailRes {
	var res templateResponseModel.AppDocTemplateDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppDocTemplateDetailUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 启用/停用应用文档模板
func (o *openApiClient) SetAppDocTemplateStatus(req *templateRequestModel.SetAppDocTemplateStatusReq, accessToken string) templateResponseModel.SetAppDocTemplateStatusRes {
	var res templateResponseModel.SetAppDocTemplateStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetAppDocTemplateStatusUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除应用文档模板
func (o *openApiClient) DeleteAppDocTemplate(req *templateRequestModel.DeleteAppDocTemplateReq, accessToken string) templateResponseModel.DeleteAppDocTemplateRes {
	var res templateResponseModel.DeleteAppDocTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteAppDocTemplateUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询应用签署任务模板列表
func (o *openApiClient) GetAppSignTemplates(req *templateRequestModel.GetAppSignTemplateListReq, accessToken string) templateResponseModel.AppSignTemplatePageListRes {
	var res templateResponseModel.AppSignTemplatePageListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppSignTemplatesUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取应用签署任务模板详情
func (o *openApiClient) GetAppSignTemplateDetail(req *templateRequestModel.GetAppSignTemplateDetailReq, accessToken string) templateResponseModel.AppSignTemplateDetailRes {
	var res templateResponseModel.AppSignTemplateDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppSignTemplateDetailUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 启用/停用应用签署任务模板
func (o *openApiClient) SetAppSignTemplateStatus(req *templateRequestModel.SetAppSignTemplateStatusReq, accessToken string) templateResponseModel.SetAppSignTemplateStatusRes {
	var res templateResponseModel.SetAppSignTemplateStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetAppSignTemplateStatusUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除应用签署任务模板
func (o *openApiClient) DeleteAppSignTemplate(req *templateRequestModel.DeleteAppSignTemplateReq, accessToken string) templateResponseModel.DeleteAppSignTemplateRes {
	var res templateResponseModel.DeleteAppSignTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteAppSignTemplateUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取应用模板新增链接
func (o *openApiClient) GetAppTemplateCreate(req *templateRequestModel.GetAppTemplateCreateUrlReq, accessToken string) templateResponseModel.GetAppTemplateCreateUrlRes {
	var res templateResponseModel.GetAppTemplateCreateUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppTemplateCreateUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取应用模板编辑链接
func (o *openApiClient) GetAppTemplateEdit(req *templateRequestModel.GetAppTemplateEditUrlReq, accessToken string) templateResponseModel.GetAppTemplateEditUrlRes {
	var res templateResponseModel.GetAppTemplateEditUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppTemplateEditUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取应用模板预览链接
func (o *openApiClient) GetAppTemplatePreview(req *templateRequestModel.GetAppTemplatePreviewUrlReq, accessToken string) templateResponseModel.GetAppTemplatePreviewUrlRes {
	var res templateResponseModel.GetAppTemplatePreviewUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppTemplatePreviewUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建业务控件
func (o *openApiClient) CreateAppField(req *templateRequestModel.CreateAppFieldReq, accessToken string) templateResponseModel.CreateAppFieldRes {
	var res templateResponseModel.CreateAppFieldRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateAppFieldUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 修改业务控件
func (o *openApiClient) ModifyAppField(req *templateRequestModel.ModifyAppFieldReq, accessToken string) templateResponseModel.ModifyAppFieldRes {
	var res templateResponseModel.ModifyAppFieldRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ModifyAppFieldUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 设置业务控件状态
func (o *openApiClient) SetAppFieldStatus(req *templateRequestModel.SetAppFieldStatusReq, accessToken string) templateResponseModel.SetAppFieldStatusRes {
	var res templateResponseModel.SetAppFieldStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetAppFieldStatusUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询业务控件列表
func (o *openApiClient) GetAppFieldList(req *templateRequestModel.GetAppFieldListReq, accessToken string) templateResponseModel.GetAppFieldListRes {
	var res templateResponseModel.GetAppFieldListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppFieldListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建企业业务控件
func (o *openApiClient) CreateCorpField(req *templateRequestModel.BatchCreateCorpFieldReq, accessToken string) templateResponseModel.BatchCreateCorpFieldRes {
	var res templateResponseModel.BatchCreateCorpFieldRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateCorpFieldUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取企业业务控件列表
func (o *openApiClient) GetCorpFieldList(req *templateRequestModel.GetCorpFieldListReq, accessToken string) templateResponseModel.GetCorpFieldListRes {
	var res templateResponseModel.GetCorpFieldListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetCorpFieldListUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除企业业务控件
func (o *openApiClient) DeleteCorpField(req *templateRequestModel.DeleteCorpFieldReq, accessToken string) templateResponseModel.DeleteCorpFieldRes {
	var res templateResponseModel.DeleteCorpFieldRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteCorpFieldUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
