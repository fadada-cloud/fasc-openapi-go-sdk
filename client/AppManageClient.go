package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/appRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/appResponseModel"
)

const (
	GetOpenIdListUrl     = "/app/get-openId-list"  //查询应用用户列表
	GetAppDevelopListUrl = "/app-develop/get-list" //查询委托代开发应用列表

)

// 查询应用用户列表
func (o *openApiClient) getOpenIdList(req *appRequestModel.GetAppOpenIdListReq, accessToken string) appResponseModel.GetAppOpenIdListRes {
	var res appResponseModel.GetAppOpenIdListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetOpenIdListUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询委托代开发应用列表
func (o *openApiClient) GetAppDevelopList(req *appRequestModel.GetAppDevelopListReq, accessToken string) appResponseModel.GetAppDevelopListRes {
	var res appResponseModel.GetAppDevelopListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppDevelopListUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
