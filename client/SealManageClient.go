package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/sealRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/sealResponseModel"
)

const (
	CreateSealByTemplateUrl                    = "/seal/create-by-template"                      //创建模板印章
	CreateSealByImageUrl                       = "/seal/create-by-image"                         //创建图片印章
	CreateLegalRepresentativeSealByTemplateUrl = "/seal/create-legal-representative-by-template" //创建法定代表人模板印章
	CreateLegalRepresentativeSealByImageUrl    = "/seal/create-legal-representative-by-image"    //创建法定代表人图片印章
	GetSealListUrl                             = "/seal/get-list"                                //查询印章列表
	GetSealDetailUrl                           = "/seal/get-detail"                              //查询印章详情
	GetAppointedSealUrl                        = "/seal/manage/get-appointed-seal-url"           //获取指定印章详情链接
	GetSealUserListUrl                         = "/seal/get-user-list"                           //查询企业用印员列表
	GetUserSealListUrl                         = "/seal/user/get-list"                           //查询指定成员的印章列表
	GetSealCreateUrl                           = "/seal/create/get-url"                          //获取印章创建链接
	GetVerifySealListUrl                       = "/seal/verify/get-list"                         //查询审核中的印章列表
	ModifySealInfoUrl                          = "/seal/modify"                                  //修改印章基本信息
	GetSealGrantUrl                            = "/seal/grant/get-url"                           //获取设置用印员链接
	GetSealFreeSignUrl                         = "/seal/free-sign/get-url"                       //获取设置企业印章免验证签链接
	CancelCorpSealFreeSignUrl                  = "/seal/free-sign/cancel"                        //解除印章免验证签
	CancelSealGrantUrl                         = "/seal/grant/cancel"                            //解除印章授权
	SetSealStatusUrl                           = "/seal/set-status"                              //设置印章状态
	DeleteSealUrl                              = "/seal/delete"                                  //删除印章
	GetSealTagListUrl                          = "/seal/tag/get-list"                            //查询印章标签列表
	GetSealManageUrl                           = "/seal/manage/get-url"                          //获取印章管理链接
	CreatePersonalSealByTemplateUrl            = "/personal-seal/create-by-template"             //创建模板签名
	CreatePersonalSealByImageUrl               = "/personal-seal/create-by-image"                //创建图片签名
	GetPersonalSealListUrl                     = "/personal-seal/get-list"                       //查询个人签名列表
	GetPersonalSealFreeSignUrl                 = "/personal-seal/free-sign/get-url"              //获取设置个人签名免验证签链接
	CancelPersonSealFreeSignUrl                = "/personal-seal/free-sign/cancel"               //解除签名免验证签
	GetPersonSealManageUrl                     = "/personal-seal/manage/get-url"                 //获取签名管理链接
	GetPersonSealCreateUrl                     = "/personal-seal/create/get-url"                 //获取签名创建链接
	DeletePersonSealUrl                        = "/personal-seal/delete"                         //删除签名

)

// 创建模板印章
func (o *openApiClient) CreateSealByTemplate(req *sealRequestModel.CreateSealByTemplateReq, accessToken string) sealResponseModel.CreateSealByTemplateRes {
	var res sealResponseModel.CreateSealByTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateSealByTemplateUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建图片印章
func (o *openApiClient) CreateSealByImage(req *sealRequestModel.CreateSealByImageReq, accessToken string) sealResponseModel.CreateSealByImageRes {
	var res sealResponseModel.CreateSealByImageRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateSealByImageUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建法定代表人模板印章
func (o *openApiClient) CreateLegalRepresentativeSealByTemplate(req *sealRequestModel.CreateLegalRepresentativeSealByTemplateReq, accessToken string) sealResponseModel.CreateLegalRepresentativeSealByTemplateRes {
	var res sealResponseModel.CreateLegalRepresentativeSealByTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateLegalRepresentativeSealByTemplateUrl //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr)     //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建法定代表人图片印章
func (o *openApiClient) CreateLegalRepresentativeSealByImage(req *sealRequestModel.CreateLegalRepresentativeSealByImageReq, accessToken string) sealResponseModel.CreateLegalRepresentativeSealByImageRes {
	var res sealResponseModel.CreateLegalRepresentativeSealByImageRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateLegalRepresentativeSealByImageUrl //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr)  //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// GetSealListResponse 查询印章列表
func (o *openApiClient) GetSealList(sealListReq *sealRequestModel.GetSealListReq, accessToken string) sealResponseModel.GetSealListRes {
	var sealListRes sealResponseModel.GetSealListRes
	reqStr, err := json.Marshal(sealListReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealListUrl                         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &sealListRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	sealListRes.RequestId = requestId
	return sealListRes
}

// 查询印章详情
func (o *openApiClient) GetSealDetail(req *sealRequestModel.GetSealDetailReq, accessToken string) sealResponseModel.GetSealDetailRes {
	var res sealResponseModel.GetSealDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealDetailUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取指定印章详情链接
func (o *openApiClient) GetAppointedSeal(req *sealRequestModel.GetAppointedSealUrlReq, accessToken string) sealResponseModel.GetAppointedSealUrlRes {
	var res sealResponseModel.GetAppointedSealUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetAppointedSealUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// GetSealUserListResponse 查询企业用印员列表
func (o *openApiClient) GetSealUserList(sealUserListReq *sealRequestModel.GetSealUserListReq, accessToken string) sealResponseModel.GetSealUserListRes {
	var sealUserListRes sealResponseModel.GetSealUserListRes
	reqStr, err := json.Marshal(sealUserListReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealUserListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &sealUserListRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	sealUserListRes.RequestId = requestId
	return sealUserListRes
}

// 查询指定成员的印章列表
func (o *openApiClient) GetUserSealList(req *sealRequestModel.GetUserSealListReq, accessToken string) sealResponseModel.GetUserSealListRes {
	var res sealResponseModel.GetUserSealListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetUserSealListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取印章创建链接
func (o *openApiClient) GetSealCreate(req *sealRequestModel.GetSealCreateUrlReq, accessToken string) sealResponseModel.GetSealCreateUrlRes {
	var res sealResponseModel.GetSealCreateUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealCreateUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询审核中的印章列表
func (o *openApiClient) GetVerifySealList(req *sealRequestModel.GetSealVerifyListReq, accessToken string) sealResponseModel.GetSealVerifyListRes {
	var res sealResponseModel.GetSealVerifyListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetVerifySealListUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 修改印章基本信息
func (o *openApiClient) ModifySealInfo(req *sealRequestModel.ModifySealReq, accessToken string) sealResponseModel.ModifySealRes {
	var res sealResponseModel.ModifySealRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ModifySealInfoUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取设置用印员链接
func (o *openApiClient) GetSealGrant(req *sealRequestModel.GetSealGrantUrlReq, accessToken string) sealResponseModel.GetSealGrantUrlRes {
	var res sealResponseModel.GetSealGrantUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealGrantUrl                        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取设置企业印章免验证签链接
func (o *openApiClient) GetSealFreeSign(req *sealRequestModel.GetSealFreeSignUrlReq, accessToken string) sealResponseModel.GetSealFreeSignUrlRes {
	var res sealResponseModel.GetSealFreeSignUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealFreeSignUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 解除印章免验证签
func (o *openApiClient) CancelCorpSealFreeSign(req *sealRequestModel.CancelCorpSealFreeSignReq, accessToken string) sealResponseModel.CancelCorpSealFreeSignRes {
	var res sealResponseModel.CancelCorpSealFreeSignRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CancelCorpSealFreeSignUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 解除印章授权
func (o *openApiClient) CancelSealGrant(req *sealRequestModel.CancelSealGrantReq, accessToken string) sealResponseModel.CancelSealGrantRes {
	var res sealResponseModel.CancelSealGrantRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CancelSealGrantUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 设置印章状态
func (o *openApiClient) SetSealStatus(req *sealRequestModel.SetSealStatusReq, accessToken string) sealResponseModel.SetSealStatusRes {
	var res sealResponseModel.SetSealStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SetSealStatusUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除印章
func (o *openApiClient) DeleteSeal(req *sealRequestModel.SealDeleteReq, accessToken string) sealResponseModel.SealDeleteRes {
	var res sealResponseModel.SealDeleteRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSealUrl                          //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询印章标签列表
func (o *openApiClient) GetSealTagList(req *sealRequestModel.GetSealTagListReq, accessToken string) sealResponseModel.GetSealTagListRes {
	var res sealResponseModel.GetSealTagListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealTagListUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取印章管理链接
func (o *openApiClient) GetSealManage(req *sealRequestModel.GetSealManageUrlReq, accessToken string) sealResponseModel.GetSealManageUrlRes {
	var res sealResponseModel.GetSealManageUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSealManageUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建模板签名
func (o *openApiClient) CreatePersonalSealByTemplate(req *sealRequestModel.CreatePersonalSealByTemplateReq, accessToken string) sealResponseModel.CreatePersonalSealByTemplateRes {
	var res sealResponseModel.CreatePersonalSealByTemplateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreatePersonalSealByTemplateUrl        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 创建图片签名
func (o *openApiClient) CreatePersonalSealByImage(req *sealRequestModel.CreatePersonalSealByImageReq, accessToken string) sealResponseModel.CreatePersonalSealByImageRes {
	var res sealResponseModel.CreatePersonalSealByImageRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreatePersonalSealByImageUrl           //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询个人签名列表
func (o *openApiClient) GetPersonalSealList(req *sealRequestModel.GetPersonalSealListReq, accessToken string) sealResponseModel.GetPersonalSealListRes {
	var res sealResponseModel.GetPersonalSealListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetPersonalSealListUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取设置个人签名免验证签链接
func (o *openApiClient) GetPersonalSealFreeSign(req *sealRequestModel.GetPersonalSealFreeSignUrlReq, accessToken string) sealResponseModel.GetPersonalSealFreeSignRes {
	var res sealResponseModel.GetPersonalSealFreeSignRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetPersonalSealFreeSignUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 解除个人签名免验证签
func (o *openApiClient) CancelPersonSealFreeSign(req *sealRequestModel.CancelPersonSealFreeSignReq, accessToken string) sealResponseModel.CancelPersonSealFreeSignRes {
	var res sealResponseModel.CancelPersonSealFreeSignRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CancelPersonSealFreeSignUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签名管理链接
func (o *openApiClient) GetPersonSealManageUrl(req *sealRequestModel.GetPersonSealManageUrlReq, accessToken string) sealResponseModel.GetPersonSealManageUrlRes {
	var res sealResponseModel.GetPersonSealManageUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetPersonSealManageUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签名创建链接
func (o *openApiClient) GetPersonSealCreateUrl(req *sealRequestModel.GetPersonSealCreateUrlReq, accessToken string) sealResponseModel.GetSealCreateUrlRes {
	var res sealResponseModel.GetSealCreateUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetPersonSealCreateUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签名创建链接
func (o *openApiClient) DeletePersonSeal(req *sealRequestModel.DeletePersonSealReq, accessToken string) sealResponseModel.DeletePersonSealRes {
	var res sealResponseModel.DeletePersonSealRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeletePersonSealUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
