package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/archivesRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/archivesResponseModel"
)

const (
	ContactArchivedUrl        = "/archives/contact-archived"    //合同归档
	GetArchivedListUrl        = "/archives/get-archived-list"   //查询已归档合同列表
	GetArchivesCatalogListUrl = "/archives/catalog-list"        //查询归档文件夹列表
	GetArchivesDetailUrl      = "/archives/get-archived-detail" //查询归档详情

	PerformanceModifyUrl  = "/archives/performance/modify" //履约修改
	PerformanceDeleteUrl  = "/archives/performance/delete" //履约删除
	PerformanceGetListUrl = "/archives/performance/list"   //查询履约列表

)

// 合同归档
func (o *openApiClient) ContactArchived(req *archivesRequestModel.ContactArchivedReq, accessToken string) archivesResponseModel.ContactArchivedRes {
	var res archivesResponseModel.ContactArchivedRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ContactArchivedUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询已归档合同列表
func (o *openApiClient) GetArchivedList(req *archivesRequestModel.ArchivedListReq, accessToken string) archivesResponseModel.ArchivedListRes {
	var res archivesResponseModel.ArchivedListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetArchivedListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询归档文件夹列表
func (o *openApiClient) GetArchivesCatalogList(req *archivesRequestModel.ArchivesCatalogListReq, accessToken string) archivesResponseModel.ArchivesCatalogListRes {
	var res archivesResponseModel.ArchivesCatalogListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetArchivesCatalogListUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询归档详情
func (o *openApiClient) GetArchivesDetail(req *archivesRequestModel.ArchivesDetailReq, accessToken string) archivesResponseModel.ArchivedDetailInfoRes {
	var res archivesResponseModel.ArchivedDetailInfoRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetArchivesDetailUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 履约修改
func (o *openApiClient) PerformanceModify(req *archivesRequestModel.CreateOrModifyPerformanceReq, accessToken string) archivesResponseModel.CreateOrModifyPerformanceRes {
	var res archivesResponseModel.CreateOrModifyPerformanceRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + PerformanceModifyUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 履约删除
func (o *openApiClient) PerformanceDelete(req *archivesRequestModel.DeletePerformanceReq, accessToken string) archivesResponseModel.DeletePerformanceRes {
	var res archivesResponseModel.DeletePerformanceRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + PerformanceDeleteUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询履约列表
func (o *openApiClient) PerformanceGetList(req *archivesRequestModel.GetPerformanceListReq, accessToken string) archivesResponseModel.GetPerformanceListRes {
	var res archivesResponseModel.GetPerformanceListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + PerformanceGetListUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
