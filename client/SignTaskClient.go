package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/signtaskRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/signtaskResponseModel"
)

const (
	CreateSignTaskUrl                     = "/sign-task/create"                             //创建签署任务
	CreateSignTaskWithTemplateUrl         = "/sign-task/create-with-template"               //创建签署任务基于签署模板
	AddSignTaskDocUrl                     = "/sign-task/doc/add"                            //添加签署任务文档
	DeleteSignTaskDocUrl                  = "/sign-task/doc/delete"                         //移除签署任务文档
	AddSignTaskFieldUrl                   = "/sign-task/field/add"                          //添加签署任务控件
	DeleteSignTaskFieldUrl                = "/sign-task/field/delete"                       //移除签署任务控件
	AddSignTaskAttachUrl                  = "/sign-task/attach/add"                         //添加签署任务附件
	DeleteSignTaskAttachUrl               = "/sign-task/attach/delete"                      //移除签署任务附件
	AddSignTaskActorUrl                   = "/sign-task/actor/add"                          //添加签署任务参与方
	ModifySignTaskActorUrl                = "/sign-task/actor/modify"                       //修改签署任务参与方
	DeleteSignTaskActorUrl                = "/sign-task/actor/delete"                       //移除签署任务参与方
	FillSignTaskFieldValuesUrl            = "/sign-task/field/fill-values"                  //填写签署任务控件内容
	IgnoreFillValueSignTaskUrl            = "/sign-task/ignore"                             //驳回填写签署任务
	StartSignTaskUrl                      = "/sign-task/start"                              //提交签署任务
	GetSignTaskActorUrl                   = "/sign-task/actor/get-url"                      //获取签署任务参与方专属链接
	SignTaskActorRemoveUrl                = "/sign-task/actor/remove"                       //移除已加入的参与方
	FinalizeSignTaskDocUrl                = "/sign-task/doc-finalize"                       //定稿签署任务文档
	FinishSignTaskUrl                     = "/sign-task/finish"                             //结束签署任务                   //
	DeleteSignTaskUrl                     = "/sign-task/delete"                             //删除签署任务
	AbolishSignTaskUrl                    = "/sign-task/abolish"                            //作废签署任务
	UrgeSignTaskUrl                       = "/sign-task/urge"                               //催办签署任务
	BlockSignTaskUrl                      = "/sign-task/block"                              //阻塞签署任务
	UnblockSignTaskUrl                    = "/sign-task/unblock"                            //解阻签署任务
	CancelSignTaskUrl                     = "/sign-task/cancel"                             //撤销签署任务
	ExtensionSignTaskUrl                  = "/sign-task/extension"                          //签署任务延期
	GetSignTaskEditUrl                    = "/sign-task/get-edit-url"                       //获取签署任务编辑链接
	GetSignTaskPreviewUrl                 = "/sign-task/get-preview-url"                    //获取签署任务预览链接
	GetSignTaskFillUrl                    = "/sign-task/get-prefill-url"                    //获取签署任务预填充链接
	GetBatchSignUrl                       = "/sign-task/get-batch-sign-url"                 //获取签署任务批量签署链接
	GetOwnerSignTaskListUrl               = "/sign-task/owner/get-list"                     //获取指定归属方的签署任务列表
	GetSignTaskDetailUrl                  = "/sign-task/app/get-detail"                     //获取签署任务详情
	GetOwnerSignTaskUrlUrl                = "/sign-task/owner/get-download-url"             //获取指定归属方的签署任务文档下载地址
	GetSignTaskCatalogListUrl             = "/sign-task-catalog/list"                       //查询企业签署任务文件夹
	GetSignTaskFieldListUrl               = "/sign-task/field/list"                         //查询签署任务控件信息
	GetSignTaskActorListUrl               = "/sign-task/actor/list"                         //查询签署任务参与方信息
	GetSignTaskActorFacePictureUrl        = "/sign-task/actor/get-face-picture"             //查询参与方的签署刷脸底图
	GetSignTaskSlicingTicketIdUrl         = "/sign-task/owner/get-slicing-ticket-id"        //签署文档切图
	GetSignTaskPicDownloadUrl             = "/sign-task/owner/get-pic-download-url"         //获取图片版签署文档下载地址
	GetApprovalInfoUrl                    = "/sign-task/get-approval-info"                  //查询签署任务审批信息
	GetSignTaskDownLoadEvidenceReportUrl  = "/sign-task/evidence-report/get-download-url"   //获取签署任务公证处保全报告下载地址
	GetSignTaskMessageReportDownloadUrl   = "/sign-task/message-report/get-download-url"    //获取消息送达阅读保全报告下载地址
	GetSignTaskActorAudioVideoDownloadUrl = "/sign-task/actor/get-audio-video-download-url" //获取参与方签署音视频下载地址
	GetSignTaskBusinessTypeListUrl        = "/sign-task/business-type/get-list"             //查询签署业务类型列表      //
	GetV3ActorSignTaskUrl                 = "/sign-task/actor/v3/get-url"                   //获取参与方签署链接（API3.0任务专属）

	SignTaskDownLoadReportUrl  = "/sign-task/download-report"    //签署任务证据中心报告下载
	ApplySignTaskReportUrl     = "/sign-task/apply-report"       //申请证据报告
	GetSignTaskOwnerFileUrl    = "/sign-task/owner/get-file"     //查询签署完成的文件
	GetSignTaskActorCerInfoUrl = "/sign-task/actor/get-cer-info" //查询证书

)

// CreateSignTask 创建签署任务
func (o *openApiClient) CreateSignTask(createSignTaskReq *signtaskRequestModel.CreateSignTaskReq, accessToken string) signtaskResponseModel.CreateSignTaskRes {
	var createSignTaskRes signtaskResponseModel.CreateSignTaskRes
	reqStr, err := json.Marshal(createSignTaskReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateSignTaskUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &createSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	createSignTaskRes.RequestId = requestId
	return createSignTaskRes
}

// CreateSignTaskWithTemplate 创建签署任务基于签署模板
func (o *openApiClient) CreateSignTaskWithTemplate(addSignTaskReq *signtaskRequestModel.CreateSignTaskWithTemplateReq, accessToken string) signtaskResponseModel.CreateSignTaskWithTemplateRes {
	var addSignTaskRes signtaskResponseModel.CreateSignTaskWithTemplateRes
	reqStr, err := json.Marshal(addSignTaskReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateSignTaskWithTemplateUrl          //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &addSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	addSignTaskRes.RequestId = requestId
	return addSignTaskRes
}

// AddSignTaskDoc 添加签署任务文档
func (o *openApiClient) AddSignTaskDoc(addSignTaskDocReq *signtaskRequestModel.AddSignTaskDocReq, accessToken string) signtaskResponseModel.AddSignTaskDocRes {
	var addSignTaskDocRes signtaskResponseModel.AddSignTaskDocRes
	reqStr, err := json.Marshal(addSignTaskDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + AddSignTaskDocUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &addSignTaskDocRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	addSignTaskDocRes.RequestId = requestId
	return addSignTaskDocRes
}

// DeleteSignTaskDoc 移除签署任务文档
func (o *openApiClient) DeleteSignTaskDoc(deleteSignTaskDocReq *signtaskRequestModel.DeleteSignTaskDocReq, accessToken string) signtaskResponseModel.DeleteSignTaskDocRes {
	var deleteSignTaskDocRes signtaskResponseModel.DeleteSignTaskDocRes
	reqStr, err := json.Marshal(deleteSignTaskDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTaskDocUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &deleteSignTaskDocRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	deleteSignTaskDocRes.RequestId = requestId
	return deleteSignTaskDocRes
}

// AddSignTaskField 添加签署任务控件
func (o *openApiClient) AddSignTaskField(addSignTaskFieldReq *signtaskRequestModel.AddSignTaskFieldReq, accessToken string) signtaskResponseModel.AddSignTaskFieldRes {
	var addSignTaskFieldRes signtaskResponseModel.AddSignTaskFieldRes
	reqStr, err := json.Marshal(addSignTaskFieldReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + AddSignTaskFieldUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &addSignTaskFieldRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	addSignTaskFieldRes.RequestId = requestId
	return addSignTaskFieldRes
}

// DeleteSignTaskField 移除签署任务控件
func (o *openApiClient) DeleteSignTaskField(deleteSignTaskFieldReq *signtaskRequestModel.DeleteSignTaskFieldReq, accessToken string) signtaskResponseModel.DeleteSignTaskFieldRes {
	var deleteSignTaskFieldRes signtaskResponseModel.DeleteSignTaskFieldRes
	reqStr, err := json.Marshal(deleteSignTaskFieldReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTaskFieldUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &deleteSignTaskFieldRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	deleteSignTaskFieldRes.RequestId = requestId
	return deleteSignTaskFieldRes
}

// AddSignTaskAttach 添加签署任务附件
func (o *openApiClient) AddSignTaskAttach(addAttachReq *signtaskRequestModel.AddSignTaskAttachReq, accessToken string) signtaskResponseModel.AddSignTaskAttachRes {
	var addAttachRes signtaskResponseModel.AddSignTaskAttachRes
	reqStr, err := json.Marshal(addAttachReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + AddSignTaskAttachUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &addAttachRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	addAttachRes.RequestId = requestId
	return addAttachRes
}

// DeleteSignTaskAttach 移除签署任务附件
func (o *openApiClient) DeleteSignTaskAttach(deleteAttachReq *signtaskRequestModel.DeleteSignTaskAttachReq, accessToken string) signtaskResponseModel.DeleteSignTaskAttachRes {
	var deleteAttachRes signtaskResponseModel.DeleteSignTaskAttachRes
	reqStr, err := json.Marshal(deleteAttachReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTaskAttachUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &deleteAttachRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	deleteAttachRes.RequestId = requestId
	return deleteAttachRes
}

// AddSignTaskActor 添加签署任务参与方
func (o *openApiClient) AddSignTaskActor(addActorReq *signtaskRequestModel.AddSignTaskActorReq, accessToken string) signtaskResponseModel.AddSignTaskActorRes {
	var addActorRes signtaskResponseModel.AddSignTaskActorRes
	reqStr, err := json.Marshal(addActorReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + AddSignTaskActorUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &addActorRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	addActorRes.RequestId = requestId
	return addActorRes
}

// 修改参与方
func (o *openApiClient) ModifySignTaskActor(req *signtaskRequestModel.UpdateActorInfoReq, accessToken string) signtaskResponseModel.UpdateActorInfoRes {
	var res signtaskResponseModel.UpdateActorInfoRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ModifySignTaskActorUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// DeleteSignTaskActor 移除签署任务参与方
func (o *openApiClient) DeleteSignTaskActor(deleteActorReq *signtaskRequestModel.DeleteSignTaskActorReq, accessToken string) signtaskResponseModel.DeleteSignTaskActorRes {
	var deleteActorRes signtaskResponseModel.DeleteSignTaskActorRes
	reqStr, err := json.Marshal(deleteActorReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTaskActorUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &deleteActorRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	deleteActorRes.RequestId = requestId
	return deleteActorRes
}

// FillSignTaskFieldValues 填写签署任务控件内容
func (o *openApiClient) FillSignTaskFieldValues(fillFieldReq *signtaskRequestModel.FillFieldValuesReq, accessToken string) signtaskResponseModel.FillFieldValuesRes {
	var fillFieldRes signtaskResponseModel.FillFieldValuesRes
	reqStr, err := json.Marshal(fillFieldReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FillSignTaskFieldValuesUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &fillFieldRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	fillFieldRes.RequestId = requestId
	return fillFieldRes
}

// 驳回填写签署任务
func (o *openApiClient) IgnoreFillValueSignTask(req *signtaskRequestModel.IgnoreFillValueSignTaskReq, accessToken string) signtaskResponseModel.IgnoreFillValueSignTaskRes {
	var res signtaskResponseModel.IgnoreFillValueSignTaskRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + IgnoreFillValueSignTaskUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// StartSignTask 提交签署任务
func (o *openApiClient) StartSignTask(req *signtaskRequestModel.StartSignTaskReq, accessToken string) signtaskResponseModel.StartSignTaskRes {
	var startSignTaskRes signtaskResponseModel.StartSignTaskRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + StartSignTaskUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &startSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	startSignTaskRes.RequestId = requestId
	return startSignTaskRes
}

// 获取参与方专属链接
func (o *openApiClient) GetSignTaskActorUrl(req *signtaskRequestModel.SignTaskActorGetUrlReq, accessToken string) signtaskResponseModel.SignTaskActorGetUrlRes {
	var res signtaskResponseModel.SignTaskActorGetUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskActorUrl                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 移除已加入的参与方
func (o *openApiClient) RemoveSignTaskActor(req *signtaskRequestModel.SignTaskActorRemoveReq, accessToken string) signtaskResponseModel.SignTaskActorRemoveRes {
	var res signtaskResponseModel.SignTaskActorRemoveRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SignTaskActorRemoveUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// FinalizeSignTaskDoc 定稿签署任务文档
func (o *openApiClient) FinalizeSignTaskDoc(finalizeDocReq *signtaskRequestModel.FinalizeSignTaskReq, accessToken string) signtaskResponseModel.FinalizeSignTaskRes {
	var finalizeDocRes signtaskResponseModel.FinalizeSignTaskRes
	reqStr, err := json.Marshal(finalizeDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FinalizeSignTaskDocUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &finalizeDocRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	finalizeDocRes.RequestId = requestId
	return finalizeDocRes
}

// 结束签署任务
func (o *openApiClient) FinishSignTask(finalizeDocReq *signtaskRequestModel.FinishSignTaskReq, accessToken string) signtaskResponseModel.FinishSignTaskRes {
	var res signtaskResponseModel.FinishSignTaskRes
	reqStr, err := json.Marshal(finalizeDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FinishSignTaskUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 删除签署任务
func (o *openApiClient) DeleteSignTask(finalizeDocReq *signtaskRequestModel.DeleteSignTaskReq, accessToken string) signtaskResponseModel.DeleteSignTaskRes {
	var res signtaskResponseModel.DeleteSignTaskRes
	reqStr, err := json.Marshal(finalizeDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DeleteSignTaskUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 作废签署任务
func (o *openApiClient) AbolishSignTask(finalizeDocReq *signtaskRequestModel.AbolishSignTaskReq, accessToken string) signtaskResponseModel.AbolishSignTaskRes {
	var res signtaskResponseModel.AbolishSignTaskRes
	reqStr, err := json.Marshal(finalizeDocReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + AbolishSignTaskUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 催办签署任务
func (o *openApiClient) UrgeSignTask(req *signtaskRequestModel.SignTaskUrgeReq, accessToken string) signtaskResponseModel.SignTaskUrgeRes {
	var res signtaskResponseModel.SignTaskUrgeRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + UrgeSignTaskUrl                        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// BlockSignTask 阻塞签署任务
func (o *openApiClient) BlockSignTask(blockSignTaskReq *signtaskRequestModel.BlockSignTaskReq, accessToken string) signtaskResponseModel.BlockSignTaskRes {
	var blockSignTaskRes signtaskResponseModel.BlockSignTaskRes
	reqStr, err := json.Marshal(blockSignTaskReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + BlockSignTaskUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &blockSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	blockSignTaskRes.RequestId = requestId
	return blockSignTaskRes
}

// UnBlockSignTask 解阻签署任务
func (o *openApiClient) UnBlockSignTask(unBlockSignTaskReq *signtaskRequestModel.UnBlockSignTaskReq, accessToken string) signtaskResponseModel.UnBlockSignTaskRes {
	var unBlockSignTaskRes signtaskResponseModel.UnBlockSignTaskRes
	reqStr, err := json.Marshal(unBlockSignTaskReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + UnblockSignTaskUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &unBlockSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	unBlockSignTaskRes.RequestId = requestId
	return unBlockSignTaskRes
}

// CancelSignTask 撤销签署任务
func (o *openApiClient) CancelSignTask(cancelSignTaskReq *signtaskRequestModel.CancelSignTaskReq, accessToken string) signtaskResponseModel.CancelSignTaskRes {
	var cancelSignTaskRes signtaskResponseModel.CancelSignTaskRes
	reqStr, err := json.Marshal(cancelSignTaskReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CancelSignTaskUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &cancelSignTaskRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	cancelSignTaskRes.RequestId = requestId
	return cancelSignTaskRes
}

// 签署任务延期
func (o *openApiClient) ExtensionSignTask(req *signtaskRequestModel.SignTaskExtensionReq, accessToken string) signtaskResponseModel.SignTaskExtensionRes {
	var res signtaskResponseModel.SignTaskExtensionRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ExtensionSignTaskUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签署任务编辑链接
func (o *openApiClient) GetSignTaskEditUrl(req *signtaskRequestModel.GetSignTaskUrlReq, accessToken string) signtaskResponseModel.GetSignTaskEditUrlRes {
	var res signtaskResponseModel.GetSignTaskEditUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskEditUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签署任务预览链接
func (o *openApiClient) GetSignTaskPreviewUrl(req *signtaskRequestModel.GetSignTaskUrlReq, accessToken string) signtaskResponseModel.GetSignTaskPreviewUrlRes {
	var res signtaskResponseModel.GetSignTaskPreviewUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskPreviewUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签署任务预填充链接
func (o *openApiClient) GetSignTaskFillUrl(req *signtaskRequestModel.GetSignTaskUrlReq, accessToken string) signtaskResponseModel.GetSignTaskPreFillUrlRes {
	var res signtaskResponseModel.GetSignTaskPreFillUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskFillUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签署任务批量签署链接
func (o *openApiClient) GetBatchSignUrl(req *signtaskRequestModel.GetBatchSignUrlReq, accessToken string) signtaskResponseModel.BatchSignUrlRes {
	var res signtaskResponseModel.BatchSignUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBatchSignUrl                        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// GetOwnerSignTaskListResponse 获取指定归属方的签署任务列表
func (o *openApiClient) GetOwnerSignTaskListResponse(ownerSignTaskListReq *signtaskRequestModel.GetOwnerSignTaskListReq, accessToken string) signtaskResponseModel.GetOwnerSignTaskListRes {
	var ownerSignTaskListRes signtaskResponseModel.GetOwnerSignTaskListRes
	reqStr, err := json.Marshal(ownerSignTaskListReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetOwnerSignTaskListUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &ownerSignTaskListRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	ownerSignTaskListRes.RequestId = requestId
	return ownerSignTaskListRes
}

// GetSignTaskDetailResponse 获取签署任务详情
func (o *openApiClient) GetSignTaskDetailResponse(signTaskDetailReq *signtaskRequestModel.GetSignTaskDetailReq, accessToken string) signtaskResponseModel.GetSignTaskDetailRes {
	var signTaskDetailRes signtaskResponseModel.GetSignTaskDetailRes
	reqStr, err := json.Marshal(signTaskDetailReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskDetailUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &signTaskDetailRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	signTaskDetailRes.RequestId = requestId
	return signTaskDetailRes
}

// GetOwnerSignTaskUrlResponse 获取指定归属方的签署任务文档下载地址
func (o *openApiClient) GetOwnerSignTaskUrlResponse(ownerSignTaskUrlReq *signtaskRequestModel.GetOwnerSignTaskUrlReq, accessToken string) signtaskResponseModel.GetSignTaskFieldUrlRes {
	var ownerSignTaskUrlRes signtaskResponseModel.GetSignTaskFieldUrlRes
	reqStr, err := json.Marshal(ownerSignTaskUrlReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetOwnerSignTaskUrlUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &ownerSignTaskUrlRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	ownerSignTaskUrlRes.RequestId = requestId
	return ownerSignTaskUrlRes
}

// 查询企业签署任务文件夹
func (o *openApiClient) GetSignTaskCatalogList(req *signtaskRequestModel.SignTaskCatalogListReq, accessToken string) signtaskResponseModel.SignTaskCatalogListRes {
	var res signtaskResponseModel.SignTaskCatalogListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskCatalogListUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询签署任务控件信息
func (o *openApiClient) GetSignTaskFieldList(req *signtaskRequestModel.ListSignTaskFieldReq, accessToken string) signtaskResponseModel.ListSignTaskFieldRes {
	var res signtaskResponseModel.ListSignTaskFieldRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskFieldListUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询签署任务参与方信息
func (o *openApiClient) GetSignTaskActorList(req *signtaskRequestModel.ListSignTaskActorReq, accessToken string) signtaskResponseModel.ListSignTaskActorRes {
	var res signtaskResponseModel.ListSignTaskActorRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskActorListUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询参与方的签署刷脸底图
func (o *openApiClient) GetSignTaskActorFacePicture(req *signtaskRequestModel.GetActorFacePictureReq, accessToken string) signtaskResponseModel.GetActorFacePictureRes {
	var res signtaskResponseModel.GetActorFacePictureRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskActorFacePictureUrl         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 签署任务文档切图
func (o *openApiClient) GetSignTaskSlicingTicketId(req *signtaskRequestModel.GetSignTaskSlicingTicketIdReq, accessToken string) signtaskResponseModel.GetSignTaskSlicingTicketIdRes {
	var res signtaskResponseModel.GetSignTaskSlicingTicketIdRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskSlicingTicketIdUrl          //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取图片版签署文档下载地址
func (o *openApiClient) GetSignTaskPicDownloadUrl(req *signtaskRequestModel.GetSignTaskPicDownloadUrlReq, accessToken string) signtaskResponseModel.GetSignTaskPicDownloadUrlRes {
	var res signtaskResponseModel.GetSignTaskPicDownloadUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskPicDownloadUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询签署任务审批信息
func (o *openApiClient) GetApprovalInfo(req *signtaskRequestModel.GetApprovalInfoReq, accessToken string) signtaskResponseModel.GetApprovalInfoRes {
	var res signtaskResponseModel.GetApprovalInfoRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalInfoUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取签署任务公证处保全报告下载地址
func (o *openApiClient) GetSignTaskDownLoadEvidenceReport(req *signtaskRequestModel.GetEvidenceReportReq, accessToken string) signtaskResponseModel.GetEvidenceReportRes {
	var res signtaskResponseModel.GetEvidenceReportRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskDownLoadEvidenceReportUrl   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取消息送达阅读保全报告下载地址
func (o *openApiClient) GetSignTaskMessageReportDownloadUrl(req *signtaskRequestModel.GetSignTaskMessageReportDownloadUrlReq, accessToken string) signtaskResponseModel.GetSignTaskMessageReportDownloadUrlRes {
	var res signtaskResponseModel.GetSignTaskMessageReportDownloadUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskMessageReportDownloadUrl    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取参与方签署音视频下载地址
func (o *openApiClient) GetSignTaskActorAudioVideoDownloadUrl(req *signtaskRequestModel.GetActorAudioVideoDownloadUrlReq, accessToken string) signtaskResponseModel.GetActorAudioVideoDownloadUrlRes {
	var res signtaskResponseModel.GetActorAudioVideoDownloadUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskActorAudioVideoDownloadUrl  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询签署业务类型列表
func (o *openApiClient) GetSignTaskBusinessTypeList(req *signtaskRequestModel.GetSignTaskBusinessTypeListReq, accessToken string) signtaskResponseModel.GetSignTaskBusinessTypeListRes {
	var res signtaskResponseModel.GetSignTaskBusinessTypeListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskBusinessTypeListUrl         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取参与方签署链接（API3.0任务专属）
func (o *openApiClient) GetV3ActorSignUrl(req *signtaskRequestModel.GetV3ActorSignTaskUrlReq, accessToken string) signtaskResponseModel.GetV3ActorSignTaskUrlRes {
	var res signtaskResponseModel.GetV3ActorSignTaskUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetV3ActorSignTaskUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 签署任务证据中心报告下载
func (o *openApiClient) SignTaskDownLoadReport(req *signtaskRequestModel.ReportDownloadReq, accessToken string) signtaskResponseModel.ReportDownloadRes {
	var res signtaskResponseModel.ReportDownloadRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SignTaskDownLoadReportUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 申请证据报告
func (o *openApiClient) ApplySignTaskReport(req *signtaskRequestModel.GetSignTaskEvidenceReportReq, accessToken string) signtaskResponseModel.GetSignTaskEvidenceReportRes {
	var res signtaskResponseModel.GetSignTaskEvidenceReportRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + ApplySignTaskReportUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询签署完成的文件
func (o *openApiClient) GetSignTaskOwnerFile(req *signtaskRequestModel.GetSignTaskFileReq, accessToken string) signtaskResponseModel.GetSignTaskFileRes {
	var res signtaskResponseModel.GetSignTaskFileRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskOwnerFileUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询证书
func (o *openApiClient) GetSignTaskActorCerInfo(req *signtaskRequestModel.SignTaskGetCerInfoReq, accessToken string) signtaskResponseModel.SignTaskGetCerInfoRes {
	var res signtaskResponseModel.SignTaskGetCerInfoRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetSignTaskActorCerInfoUrl             //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
