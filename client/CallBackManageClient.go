package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/callbackRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/callbackResponseModel"
)

const (
	GetCallBackListUrl = "/callback/get-list" //查询回调列表

)

// 查询审批列表
func (o *openApiClient) GetCallBackList(req *callbackRequestModel.GetCallBackListReq, accessToken string) callbackResponseModel.GetCallBackListRes {
	var res callbackResponseModel.GetCallBackListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetCallBackListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
