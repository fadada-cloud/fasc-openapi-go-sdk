package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/voucherRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/voucherResponseModel"
)

const (
	CreateVoucherTaskUrl      = "/voucher-sign-task/create"                 //创建单据任务
	QueryVoucherTaskListUrl   = "/voucher-sign-task/owner/get-list"         //查询单据签署任务列表
	QueryVoucherTaskDetailUrl = "/voucher-sign-task/app/get-detail"         //查询单据签署任务详情
	GetVoucherTaskDownloadUrl = "/voucher-sign-task/owner/get-download-url" //下载单据签署任务
	VoucherTaskCancelUrl      = "/voucher-sign-task/cancel"                 //撤销单据签署任务
	GetVoucherActorUrl        = "/voucher-sign-task/actor/get-url"          //获取单据签署链接

)

// 创建单据任务
func (o *openApiClient) CreateVoucherTask(req *voucherRequestModel.VoucherSignTaskCreateReq, accessToken string) voucherResponseModel.BaseVoucherTaskRes {
	var res voucherResponseModel.BaseVoucherTaskRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + CreateVoucherTaskUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询单据签署任务列表
func (o *openApiClient) QueryVoucherTaskList(req *voucherRequestModel.QueryVoucherTaskListReq, accessToken string) voucherResponseModel.VoucherTaskListInfoRes {
	var res voucherResponseModel.VoucherTaskListInfoRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + QueryVoucherTaskListUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询单据签署任务详情
func (o *openApiClient) QueryVoucherTaskDetail(req *voucherRequestModel.QueryVoucherTaskDetailReq, accessToken string) voucherResponseModel.QueryVoucherTaskDetailRes {
	var res voucherResponseModel.QueryVoucherTaskDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + QueryVoucherTaskDetailUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 下载单据签署任务
func (o *openApiClient) GetVoucherTaskDownloadUrl(req *voucherRequestModel.GetVoucherTaskDownloadUrlReq, accessToken string) voucherResponseModel.GetVoucherTaskDownloadUrlRes {
	var res voucherResponseModel.GetVoucherTaskDownloadUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetVoucherTaskDownloadUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 撤销单据签署任务
func (o *openApiClient) VoucherTaskCancel(req *voucherRequestModel.CancelVoucherTaskReq, accessToken string) voucherResponseModel.CancelVoucherTaskRes {
	var res voucherResponseModel.CancelVoucherTaskRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VoucherTaskCancelUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取单据签署链接
func (o *openApiClient) GetVoucherActorUrl(req *voucherRequestModel.GetVoucherTaskActorUrlReq, accessToken string) voucherResponseModel.GetVoucherTaskActorUrlRes {
	var res voucherResponseModel.GetVoucherTaskActorUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetVoucherActorUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
