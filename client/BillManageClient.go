package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/billManageRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/billManageResponseModel"
)

const (
	GetBillUrlReqUrl            = "/billing/get-bill-url"                //获取计费链接
	GetBillUsageAvailableNumUrl = "/bill-account/get-usage-availablenum" //查询账户可用余量

)

// GetBillUrlResponse 获取计费链接
func (o *openApiClient) GetBillUrl(billUrlReq *billManageRequestModel.GetBillingUrlReq, accessToken string) billManageResponseModel.GetBillingUrlRes {
	var billUrlRes billManageResponseModel.GetBillingUrlRes
	reqStr, err := json.Marshal(billUrlReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBillUrlReqUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &billUrlRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	billUrlRes.RequestId = requestId
	return billUrlRes
}

// 查询账户可用余量
func (o *openApiClient) GetUsageAvailablenum(billUrlReq *billManageRequestModel.GetUsageAvailablenumReq, accessToken string) billManageResponseModel.GetUsageAvailablenumRes {
	var billUrlRes billManageResponseModel.GetUsageAvailablenumRes
	reqStr, err := json.Marshal(billUrlReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBillUsageAvailableNumUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &billUrlRes)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	billUrlRes.RequestId = requestId
	return billUrlRes
}
