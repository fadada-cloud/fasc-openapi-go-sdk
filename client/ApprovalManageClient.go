package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/approvalRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/approvalResponseModel"
)

const (
	GetApprovalListUrl       = "/approval/get-list"        //查询审批列表
	GetApprovalDetailUrl     = "/approval/get-detail"      //查询审批详情
	GetApprovalUrl           = "/approval/get-url"         //获取审批链接
	GetApprovalFlowListUrl   = "/approval-flow/get-list"   //查询审批流程列表
	GetApprovalFlowDetailUrl = "/approval-flow/get-detail" //查询审批流程详情
)

// 查询审批列表
func (o *openApiClient) GetApprovalList(req *approvalRequestModel.GetApprovalListReq, accessToken string) approvalResponseModel.GetApprovalListRes {
	var res approvalResponseModel.GetApprovalListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalListUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询审批详情
func (o *openApiClient) GetApprovalDetail(req *approvalRequestModel.GetApprovalDetailReq, accessToken string) approvalResponseModel.GetApprovalDetailRes {
	var res approvalResponseModel.GetApprovalDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalDetailUrl                   //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取审批链接
func (o *openApiClient) GetApprovalUrl(req *approvalRequestModel.GetApprovalUrlReq, accessToken string) approvalResponseModel.GetApprovalUrlRes {
	var res approvalResponseModel.GetApprovalUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalUrl                         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询审批流程列表
func (o *openApiClient) GetApprovalFlowList(req *approvalRequestModel.GetApprovalFlowListReq, accessToken string) approvalResponseModel.GetApprovalFlowListRes {
	var res approvalResponseModel.GetApprovalFlowListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalFlowListUrl                 //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询审批流程详情
func (o *openApiClient) GetApprovalFlowDetail(req *approvalRequestModel.GetApprovalFlowDetailReq, accessToken string) approvalResponseModel.GetApprovalFlowDetailRes {
	var res approvalResponseModel.GetApprovalFlowDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetApprovalFlowDetailUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
