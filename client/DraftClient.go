package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/draftRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/drafResponseModel"
)

const (
	DraftCreateUrl              = "/draft/create"                   //发起合同协商
	DraftGetInviteUrl           = "/draft/get-invite-url"           //获取合同协商邀请链接
	DraftGetEditUrl             = "/draft/get-edit-url"             //获取合同协商编辑链接
	DraftGetManageUrl           = "/draft/get-manage-url"           //获取合同起草管理链接
	DraftFinalizeUrl            = "/draft/doc-finalize"             //合同协商文件定稿
	DraftGetFinishFileUrl       = "/draft/get-finished-file"        //查询已定稿的合同文件
	DraftAddMembersUrl          = "/draft/members/add"              //添加协商成员
	SearchInitiatedDraftListUrl = "/draft/owner/get-initiated-list" //查询发起的协商列表
	SearchJoinedDraftListUrl    = "/draft/owner/get-joined-list"    //查询参与的协商列表

)

// 发起合同协商
func (o *openApiClient) DraftCreate(req *draftRequestModel.DraftCreateReq, accessToken string) drafResponseModel.DraftCreateRes {
	var res drafResponseModel.DraftCreateRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftCreateUrl                         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取合同协商邀请链接
func (o *openApiClient) DraftGetInviteUrl(req *draftRequestModel.GetDraftInviteUrlReq, accessToken string) drafResponseModel.GetDraftInviteUrlRes {
	var res drafResponseModel.GetDraftInviteUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftGetInviteUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取合同协商编辑链接
func (o *openApiClient) DraftGetEditUrl(req *draftRequestModel.GetDraftEditUrlReq, accessToken string) drafResponseModel.GetDraftEditUrlRes {
	var res drafResponseModel.GetDraftEditUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftGetEditUrl                        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取合同起草管理链接
func (o *openApiClient) DraftGetManageUrl(req *draftRequestModel.GetDraftManageUrlReq, accessToken string) drafResponseModel.GetDraftManageUrlRes {
	var res drafResponseModel.GetDraftManageUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftGetManageUrl                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 合同协商文件定稿
func (o *openApiClient) DraftFinalize(req *draftRequestModel.DocFinalizeReq, accessToken string) drafResponseModel.DocFinalizeRes {
	var res drafResponseModel.DocFinalizeRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftFinalizeUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询已定稿的合同文件
func (o *openApiClient) DraftGetFinishFile(req *draftRequestModel.GetDraftFinishedFileReq, accessToken string) drafResponseModel.GetDraftFinishedFileRes {
	var res drafResponseModel.GetDraftFinishedFileRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftGetFinishFileUrl                  //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 添加协商成员
func (o *openApiClient) DraftAddMembers(req *draftRequestModel.DraftAddMembersReq, accessToken string) drafResponseModel.DraftAddMembersRes {
	var res drafResponseModel.DraftAddMembersRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DraftAddMembersUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询发起的协商列表
func (o *openApiClient) searchInitiatedDraftList(req *draftRequestModel.SearchInitiatedDraftListReq, accessToken string) drafResponseModel.SearchInitiatedDraftListRes {
	var res drafResponseModel.SearchInitiatedDraftListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SearchInitiatedDraftListUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 查询参与的协商列表
func (o *openApiClient) SearchJoinedDraftList(req *draftRequestModel.SearchJoinedDraftListReq, accessToken string) drafResponseModel.SearchJoinedDraftListRes {
	var res drafResponseModel.SearchJoinedDraftListRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + SearchJoinedDraftListUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}
