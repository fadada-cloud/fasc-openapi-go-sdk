package client

import (
	"encoding/json"
	"fmt"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/common"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/requestModel/toolServiceManageRequestModel"
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/responseModel/toolServiceManageResponseModel"
)

const (
	GetThreeElementVerifyUrl            = "/user/three-element-verify/get-url"                 //获取个人运营商三要素校验链接
	GetFourElementVerifyUrl             = "/user/four-element-verify/get-url"                  //获取个人银行卡四要素校验链接
	GetIdCardImageDownloadUrl           = "/user/element-verify/get-idcard-image-download-url" //根据verifyId获取下载链接
	UserIdCardOcrUrl                    = "/user/ocr/id-card"                                  //身份证OCR
	IdentityTwoElementsVerifyUrl        = "/user/identity/two-element-verify"                  //个人二要素校验
	TelecomThreeElementsVerifyUrl       = "/user/telecom/three-element-verify"                 //个人运营商三要素校验
	BankFourElementsVerifyUrl           = "/user/bank/four-element-verify"                     //个人银行卡四要素校验
	BankThreeElementVerifyUrl           = "/user/identity/bank-three-element-verify"           //个人银行卡三要素校验
	IdentityIdCardThreeElementVerifyUrl = "/user/identity/idcard-three-element-verify"         //人脸图片比对校验
	FaceRecognitionUrl                  = "/user/verify/face-recognition"                      //获取人脸核验链接
	FaceRecognitionGetStatusUrl         = "/user/verify/face-status-query"                     //获取人脸核验状态
	BankCardOcrUrl                      = "/user/ocr/bankcard"                                 //银行卡OCR
	LicenseCardOcr                      = "/user/ocr/license"                                  //营业执照OCR
	DrivingLicenseOcr                   = "/user/ocr/drivinglicense"                           //驾驶证OCR
	VehicleLicenseOcr                   = "/user/ocr/vehiclelicense"                           //行驶证OCR
	MainlandPermitOcr                   = "/user/ocr/mainland-permit"                          //通行证OCR
	VerifyFourElementTransactionCreate  = "/user/verify/bankcard-four-element/create"          //银行卡四要素核验-创建订单
	VerifyThreeElementTransactionCreate = "/user/verify/telecom-three-element/create"          //手机号三要素核验-创建订单
	VerifyAuthCodeCheck                 = "/user/verify/auth-code/check"                       //验证码校验
	VerifyGetAuthCode                   = "/user/verify/auth-code/get"                         //获取验证码
	VerifyGetDetailByAuthCode           = "/user/verify/get-detail"                            //获取身份核验详情
	GetBusinessThreeElementVerifyUrl    = "/corp/identity/business-three-element-verify"       //企业组织三要素校验
	GetBusinessFourElementVerifyUrl     = "/corp/identity/business-four-element-verify"        //企业组织四要素校验
	GetBusinessInformationUrl           = "/corp/identity/business-info-query"                 //企业工商信息查询

)

// 获取个人三要素校验链接
func (o *openApiClient) GetThreeElementVerifyUrl(req *toolServiceManageRequestModel.GetThreeElementVerifyUrlReq, accessToken string) toolServiceManageResponseModel.GetThreeElementVerifyUrlRes {
	var res toolServiceManageResponseModel.GetThreeElementVerifyUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetThreeElementVerifyUrl               //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取个人四要素校验链接
func (o *openApiClient) GetFourElementVerifyUrl(req *toolServiceManageRequestModel.GetFourElementVerifyUrlReq, accessToken string) toolServiceManageResponseModel.GetFourElementVerifyUrlRes {
	var res toolServiceManageResponseModel.GetFourElementVerifyUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetFourElementVerifyUrl                //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取要素校验身份证图片下载链接
func (o *openApiClient) GetIdCardImageDownloadUrl(req *toolServiceManageRequestModel.GetIdCardImageDownloadUrlReq, accessToken string) toolServiceManageResponseModel.GetIdCardImageDownloadUrlRes {
	var res toolServiceManageResponseModel.GetIdCardImageDownloadUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetIdCardImageDownloadUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 身份证OCR
func (o *openApiClient) UserIdCardOcr(req *toolServiceManageRequestModel.UserIdCardOcrReq, accessToken string) toolServiceManageResponseModel.UserIdCardOcrRes {
	var res toolServiceManageResponseModel.UserIdCardOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + UserIdCardOcrUrl                       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 个人二要素校验
func (o *openApiClient) IdentityTwoElementsVerify(req *toolServiceManageRequestModel.IdentityTwoElementVerifyReq, accessToken string) toolServiceManageResponseModel.IdentityTwoElementVerifyRes {
	var res toolServiceManageResponseModel.IdentityTwoElementVerifyRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + IdentityTwoElementsVerifyUrl           //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 个人运营商三要素校验
func (o *openApiClient) TelecomThreeElementsVerify(req *toolServiceManageRequestModel.TelecomThreeElementVerifyReq, accessToken string) toolServiceManageResponseModel.TelecomThreeElementVerifyRes {
	var res toolServiceManageResponseModel.TelecomThreeElementVerifyRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + TelecomThreeElementsVerifyUrl          //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 个人银行卡四要素校验
func (o *openApiClient) BankFourElementsVerify(req *toolServiceManageRequestModel.BankFourElementVerifyReq, accessToken string) toolServiceManageResponseModel.BankFourElementVerifyRes {
	var res toolServiceManageResponseModel.BankFourElementVerifyRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + BankFourElementsVerifyUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 个人银行卡三要素校验
func (o *openApiClient) BankThreeElementVerify(req *toolServiceManageRequestModel.BankThreeElementVerifyReq, accessToken string) toolServiceManageResponseModel.BankThreeElementVerifyRes {
	var res toolServiceManageResponseModel.BankThreeElementVerifyRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + BankThreeElementVerifyUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 人脸图片比对校验
func (o *openApiClient) IdentityIdCardThreeElementVerify(req *toolServiceManageRequestModel.GetIdCardThreeElementVerifyReq, accessToken string) toolServiceManageResponseModel.GetIdCardThreeElementVerifyRes {
	var res toolServiceManageResponseModel.GetIdCardThreeElementVerifyRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + IdentityIdCardThreeElementVerifyUrl    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取人脸核验链接
func (o *openApiClient) FaceRecognition(req *toolServiceManageRequestModel.GetFaceRecognitionUrlReq, accessToken string) toolServiceManageResponseModel.GetFaceRecognitionUrlRes {
	var res toolServiceManageResponseModel.GetFaceRecognitionUrlRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FaceRecognitionUrl                     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取人脸核验状态
func (o *openApiClient) FaceRecognitionGetStatus(req *toolServiceManageRequestModel.GetFaceRecognitionStatusReq, accessToken string) toolServiceManageResponseModel.GetFaceRecognitionStatusRes {
	var res toolServiceManageResponseModel.GetFaceRecognitionStatusRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + FaceRecognitionGetStatusUrl            //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 银行卡OCR
func (o *openApiClient) BankcardOcr(req *toolServiceManageRequestModel.BankCardOcrReq, accessToken string) toolServiceManageResponseModel.BankCardOcrRes {
	var res toolServiceManageResponseModel.BankCardOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + BankCardOcrUrl                         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 营业执照OCR
func (o *openApiClient) LicenseCardOcr(req *toolServiceManageRequestModel.LicenseOcrReq, accessToken string) toolServiceManageResponseModel.LicenseOcrRes {
	var res toolServiceManageResponseModel.LicenseOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + LicenseCardOcr                         //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 驾驶证OCR
func (o *openApiClient) DrivinglicenseOcr(req *toolServiceManageRequestModel.DriveLicenseOcrReq, accessToken string) toolServiceManageResponseModel.DriveLicenseOcrRes {
	var res toolServiceManageResponseModel.DriveLicenseOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + DrivingLicenseOcr                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 行驶证OCR
func (o *openApiClient) VehiclelicenseOcr(req *toolServiceManageRequestModel.VehicleLicenseOcrReq, accessToken string) toolServiceManageResponseModel.VehicleLicenseOcrRes {
	var res toolServiceManageResponseModel.VehicleLicenseOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VehicleLicenseOcr                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 通行证OCR
func (o *openApiClient) MainlandPermitOcr(req *toolServiceManageRequestModel.MainlandPermitOcrReq, accessToken string) toolServiceManageResponseModel.MainlandPermitOcrRes {
	var res toolServiceManageResponseModel.MainlandPermitOcrRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + MainlandPermitOcr                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 银行卡四要素核验-创建订单
func (o *openApiClient) VerifyFourElementTransactionCreate(req *toolServiceManageRequestModel.GetBankcardFourElementTransactionIdReq, accessToken string) toolServiceManageResponseModel.GetTransactionIdRes {
	var res toolServiceManageResponseModel.GetTransactionIdRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VerifyFourElementTransactionCreate     //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 手机号三要素核验-创建订单
func (o *openApiClient) VerifyThreeElementTransactionCreate(req *toolServiceManageRequestModel.GetMobileThreeElementTransactionIdReq, accessToken string) toolServiceManageResponseModel.GetTransactionIdRes {
	var res toolServiceManageResponseModel.GetTransactionIdRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VerifyThreeElementTransactionCreate    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 验证码校验
func (o *openApiClient) VerifyAuthCodeCheck(req *toolServiceManageRequestModel.VerifyAuthCodeReq, accessToken string) toolServiceManageResponseModel.VerifyAuthCodeRes {
	var res toolServiceManageResponseModel.VerifyAuthCodeRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VerifyAuthCodeCheck                    //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取验证码
func (o *openApiClient) GetVerifyAuthCode(req *toolServiceManageRequestModel.GetAuthCodeReq, accessToken string) toolServiceManageResponseModel.GetAuthCodeRes {
	var res toolServiceManageResponseModel.GetAuthCodeRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VerifyGetAuthCode                      //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 获取身份核验详情
func (o *openApiClient) VerifyGetDetailByAuthCode(req *toolServiceManageRequestModel.GetVerifyDetailReq, accessToken string) toolServiceManageResponseModel.GetVerifyDetailRes {
	var res toolServiceManageResponseModel.GetVerifyDetailRes
	reqStr, err := json.Marshal(req)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + VerifyGetDetailByAuthCode              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &res)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	res.RequestId = requestId
	return res
}

// 企业组织三要素校验
func (o *openApiClient) GetBusinessThreeElementVerify(getIdentifiedStatusReq *toolServiceManageRequestModel.GetBusinessThreeElementReq, accessToken string) toolServiceManageResponseModel.GetBusinessThreeElementRes {
	var response toolServiceManageResponseModel.GetBusinessThreeElementRes
	reqStr, err := json.Marshal(getIdentifiedStatusReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBusinessThreeElementVerifyUrl       //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &response)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	response.RequestId = requestId
	return response
}

// 企业组织四要素校验
func (o *openApiClient) GetBusinessFourElementVerify(getIdentifiedStatusReq *toolServiceManageRequestModel.GetBusinessFourElementReq, accessToken string) toolServiceManageResponseModel.GetBusinessFourElementRes {
	var response toolServiceManageResponseModel.GetBusinessFourElementRes
	reqStr, err := json.Marshal(getIdentifiedStatusReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBusinessFourElementVerifyUrl        //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &response)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	response.RequestId = requestId
	return response
}

// 企业工商信息查询
func (o *openApiClient) GetBusinessInformation(getIdentifiedStatusReq *toolServiceManageRequestModel.GetBusinessInfoReq, accessToken string) toolServiceManageResponseModel.GetBusinessInfoRes {
	var response toolServiceManageResponseModel.GetBusinessInfoRes
	reqStr, err := json.Marshal(getIdentifiedStatusReq)
	if err != nil {
		fmt.Println("json序列化失败")
	}
	headMap := o.SetReqHeadMap(accessToken, string(reqStr))
	requestUrl := o.serverUrl + GetBusinessInformationUrl              //接口请求api地址
	rspBody, requestId := common.SendPost(requestUrl, headMap, reqStr) //发送post请求
	err = json.Unmarshal(rspBody, &response)
	if err != nil {
		fmt.Println("json字符串转为struct失败")
	}
	response.RequestId = requestId
	return response
}
