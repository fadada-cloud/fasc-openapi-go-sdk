package voucherRequestModel

type VoucherSignTaskCreateReq struct {
	Initiator        *OpenId  `json:"initiator"`
	SignTaskSubject  string   `json:"signTaskSubject"`
	TransReferenceId string   `json:"transReferenceId"`
	Docs             []Doc    `json:"docs"`
	Actors           []Actor  `json:"actors"`
	Attachs          []Attach `json:"attachs"`
}

type OpenId struct {
	IdType string `json:"idType"`
	OpenId string `json:"openId"`
}

type Doc struct {
	DocId         string     `json:"docId"`
	DocName       string     `json:"docName"`
	DocFileId     string     `json:"docFileId"`
	DocTemplateId string     `json:"docTemplateId"`
	DocFields     []DocField `json:"docFields"`
}

type Actor struct {
	ActorId        string          `json:"actorId"`
	ActorName      string          `json:"actorName"`
	FillFields     []FillField     `json:"fillFields"`
	SignFields     []SignField     `json:"signFields"`
	SignConfigInfo *SignConfigInfo `json:"signConfigInfo"`
}

type FillField struct {
	FieldDocId string `json:"fieldDocId"`
	FieldId    string `json:"fieldId"`
	FieldName  string `json:"fieldName"`
	FieldValue string `json:"fieldValue"`
}

type SignField struct {
	FieldDocId string `json:"fieldDocId"`
	FieldId    string `json:"fieldId"`
	FieldName  string `json:"fieldName"`
}

type SignConfigInfo struct {
	SignerSignMethod string `json:"signerSignMethod"`
	ReadingToEnd     bool   `json:"readingToEnd"`
	ReadingTime      int    `json:"readingTime"`
}

type DocField struct {
	FieldId             string               `json:"fieldId"`
	FieldName           string               `json:"fieldName"`
	FieldType           string               `json:"fieldType"`
	FieldKey            string               `json:"fieldKey"`
	Position            *Position            `json:"position"`
	Moveable            bool                 `json:"moveable"`
	FieldCorpSeal       *FieldCorpSeal       `json:"fieldCorpSeal"`
	FieldPersonSign     *FieldPersonSign     `json:"fieldPersonSign"`
	FieldRemarkSign     *FieldRemarkSign     `json:"fieldRemarkSign"`
	FieldTextSingleLine *FieldTextSingleLine `json:"fieldTextSingleLine"`
	FieldTextMultiLine  *FieldTextMultiLine  `json:"fieldTextMultiLine"`
	FieldNumber         *FieldNumber         `json:"fieldNumber"`
	FieldIdCard         *FieldIdCard         `json:"fieldIdCard"`
	FieldFillDate       *FieldFillDate       `json:"fieldFillDate"`
	FieldSelectBox      *FieldSelectBox      `json:"fieldSelectBox"`
	FieldPicture        *FieldPicture        `json:"fieldPicture"`
	FieldTable          *FieldTable          `json:"fieldTable"`
	FieldMultiCheckbox  *FieldMultiCheckbox  `json:"fieldMultiCheckbox"`
}

type Position struct {
	PositionMode    string `json:"positionMode"`
	PositionPageNo  int    `json:"positionPageNo"`
	PositionX       string `json:"positionX"`
	PositionY       string `json:"positionY"`
	PositionKeyword string `json:"positionKeyword"`
	KeywordOffsetX  string `json:"keywordOffsetX"`
	KeywordOffsetY  string `json:"keywordOffsetY"`
}

type FieldCorpSeal struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type FieldPersonSign struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

type FieldRemarkSign struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Editable     bool   `json:"editable"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
}

type FieldTextSingleLine struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type FieldTextMultiLine struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type FieldNumber struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type FieldIdCard struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type FieldFillDate struct {
	DefaultValue string `json:"defaultValue"`
	DateFormat   string `json:"dateFormat"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}
type FieldSelectBox struct {
	DefaultValue string   `json:"defaultValue"`
	Option       []string `json:"option"`
	Required     bool     `json:"required"`
	FontType     string   `json:"fontType"`
	FontSize     int      `json:"fontSize"`
	Width        int      `json:"width"`
	Height       int      `json:"height"`
	Alignment    string   `json:"alignment"`
}

type FieldPicture struct {
	Required     bool   `json:"required"`
	DefaultValue string `json:"defaultValue"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
}

type FieldTable struct {
	Required       bool       `json:"required"`
	RequiredCount  int        `json:"requiredCount"`
	FontType       string     `json:"fontType"`
	FontSize       int        `json:"fontSize"`
	Alignment      string     `json:"alignment"`
	HeaderPosition string     `json:"headerPosition"`
	Rows           int        `json:"rows"`
	Cols           int        `json:"cols"`
	RowHeight      int        `json:"rowHeight"`
	Widths         []int      `json:"widths"`
	DynamicFilling bool       `json:"dynamicFilling"`
	DefaultValue   [][]string `json:"defaultValue"`
	Header         []string   `json:"header"`
	HideHeader     bool       `json:"hideHeader"`
}

type FieldMultiCheckbox struct {
	Required     bool     `json:"required"`
	Option       []string `json:"option"`
	DefaultValue []bool   `json:"defaultValue"`
}

type Attach struct {
	AttachId     string `json:"attachId"`
	AttachName   string `json:"attachName"`
	AttachFileId string `json:"attachFileId"`
}
