package voucherRequestModel

type QueryVoucherTaskListReq struct {
	Initiator    *OpenId     `json:"initiator"`
	ListFilter   *ListFilter `json:"listFilter"`
	ListPageNo   int         `json:"listPageNo"`
	ListPageSize int         `json:"listPageSize"`
}

type ListFilter struct {
	SignTaskSubject string   `json:"signTaskSubject"`
	ActorId         string   `json:"actorId"`
	SignTaskStatus  []string `json:"signTaskStatus"`
}
