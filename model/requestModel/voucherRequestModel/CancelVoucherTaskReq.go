package voucherRequestModel

type CancelVoucherTaskReq struct {
	SignTaskId      string `json:"signTaskId"`
	TerminationNote string `json:"terminationNote"`
}
