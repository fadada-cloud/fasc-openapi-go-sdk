package voucherRequestModel

type GetVoucherTaskDownloadUrlReq struct {
	OwnerId    *OpenId `json:"ownerId"`
	SignTaskId string  `json:"signTaskId"`
	FileType   string  `json:"fileType"`
	Id         string  `json:"id"`
	CustomName string  `json:"customName"`
}
