package voucherRequestModel

type GetVoucherTaskActorUrlReq struct {
	SignTaskId  string `json:"signTaskId"`
	ActorId     string `json:"actorId"`
	RedirectUrl string `json:"redirectUrl"`
}
