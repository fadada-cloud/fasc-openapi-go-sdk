package billManageRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

// GetBillingUrlReq 获取计费链接
type GetBillingUrlReq struct {
	ClientUserId string              `json:"clientUserId,omitempty"`
	OpenId       *commonModel.OpenId `json:"openId,omitempty"`
	UrlType      string              `json:"urlType,omitempty"`
	RedirectUrl  string              `json:"redirectUrl,omitempty"`
}
