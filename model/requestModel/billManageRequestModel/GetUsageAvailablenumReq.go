package billManageRequestModel

type GetUsageAvailablenumReq struct {
	OpenCorpId string `json:"openCorpId"`
	UsageCode  string `json:"usageCode"`
}
