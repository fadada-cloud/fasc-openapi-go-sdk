package orgRequestModel

type GetCorpEntityManageUrlReq struct {
	OpenCorpId   string `json:"openCorpId"`
	ClientUserId string `json:"clientUserId"`
	RedirectUrl  string `json:"redirectUrl"`
}
