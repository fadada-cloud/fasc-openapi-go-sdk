package appRequestModel

type GetAppOpenIdListReq struct {
	IdType          string `json:"idType"`
	Owner           bool   `json:"owner"`
	AvailableStatus string `json:"availableStatus"`
	ListPageNo      int    `json:"listPageNo"`
	ListPageSize    int    `json:"listPageSize"`
}
