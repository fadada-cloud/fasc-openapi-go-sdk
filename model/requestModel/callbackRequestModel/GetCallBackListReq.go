package callbackRequestModel

type GetCallBackListReq struct {
	CallbackType string `json:"callbackType,omitempty"`
	StartTime    string `json:"startTime,omitempty"`
	EndTime      string `json:"endTime,omitempty"`
	ListPageNo   int    `json:"listPageNo,omitempty"`
	ListPageSize int    `json:"listPageSize,omitempty"`
}
