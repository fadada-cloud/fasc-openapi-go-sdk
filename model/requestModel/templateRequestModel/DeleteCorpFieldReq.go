package templateRequestModel

type DeleteCorpFieldReq struct {
	OpenCorpId string             `json:"openCorpId,omitempty"`
	Fields     []DeleteCorpFields `json:"fields"`
}

type DeleteCorpFields struct {
	FieldName string `json:"fieldName"`
	FieldType string `json:"fieldType"`
}
