package templateRequestModel

type CopyCreateDocTemplateReq struct {
	OpenCorpId      string `json:"openCorpId"`
	DocTemplateId   string `json:"docTemplateId"`
	DocTemplateName string `json:"docTemplateName"`
	CreateSerialNo  string `json:"createSerialNo"`
}
