package templateRequestModel

type SetSignTemplateStatusReq struct {
	OpenCorpId         string `json:"openCorpId,omitempty"`
	SignTemplateId     string `json:"signTemplateId,omitempty"`
	SignTemplateStatus string `json:"signTemplateStatus,omitempty"`
}
