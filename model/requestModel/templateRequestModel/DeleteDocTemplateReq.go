package templateRequestModel

type DeleteDocTemplateReq struct {
	OpenCorpId    string `json:"openCorpId,omitempty"`
	DocTemplateId string `json:"docTemplateId,omitempty"`
}
