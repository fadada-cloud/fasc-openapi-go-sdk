package templateRequestModel

type SetDocTemplateStatusReq struct {
	OpenCorpId        string `json:"openCorpId,omitempty"`
	DocTemplateId     string `json:"docTemplateId,omitempty"`
	DocTemplateStatus string `json:"docTemplateStatus,omitempty"`
}
