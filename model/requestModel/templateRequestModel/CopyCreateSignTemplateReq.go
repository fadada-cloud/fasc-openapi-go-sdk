package templateRequestModel

type CopyCreateSignTemplateReq struct {
	OpenCorpId       string `json:"openCorpId"`
	SignTemplateId   string `json:"signTemplateId"`
	SignTemplateName string `json:"signTemplateName"`
	CreateSerialNo   string `json:"createSerialNo"`
}
