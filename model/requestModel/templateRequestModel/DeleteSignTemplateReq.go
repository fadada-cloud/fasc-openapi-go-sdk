package templateRequestModel

type DeleteSignTemplateReq struct {
	OpenCorpId     string `json:"openCorpId,omitempty"`
	SignTemplateId string `json:"signTemplateId,omitempty"`
}
