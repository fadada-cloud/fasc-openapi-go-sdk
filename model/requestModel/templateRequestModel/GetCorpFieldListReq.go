package templateRequestModel

type GetCorpFieldListReq struct {
	OpenCorpId string `json:"openCorpId,omitempty"`
}
