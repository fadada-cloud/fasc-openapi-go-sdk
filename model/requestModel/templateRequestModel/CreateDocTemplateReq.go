package templateRequestModel

type CreateDocTemplateReq struct {
	OpenCorpId      string `json:"openCorpId"`
	DocTemplateName string `json:"docTemplateName"`
	CreateSerialNo  string `json:"createSerialNo"`
	FileId          string `json:"fileId"`
}
