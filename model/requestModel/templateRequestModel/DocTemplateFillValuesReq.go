package templateRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type DocTemplateFillValuesReq struct {
	OwnerId        *commonModel.OpenId    `json:"ownerId"`
	DocTemplateId  string                 `json:"docTemplateId"`
	FileName       string                 `json:"fileName"`
	DocFieldValues []DocTemplateFillValue `json:"docFieldValues"`
}

type DocTemplateFillValue struct {
	FieldId    string `json:"fieldId"`
	FieldValue string `json:"fieldValue"`
	FieldName  string `json:"fieldName"`
}
