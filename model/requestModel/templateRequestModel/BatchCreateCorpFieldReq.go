package templateRequestModel

type BatchCreateCorpFieldReq struct {
	OpenCorpId string       `json:"openCorpId,omitempty"`
	CorpFields []CorpFields `json:"fields,omitempty"`
}

type CorpFields struct {
	FieldName           string                   `json:"fieldName,omitempty"`
	FieldType           string                   `json:"fieldType,omitempty"`
	FieldTextSingleLine *CorpFieldTextSingleLine `json:"fieldTextSingleLine,omitempty"`
	FieldTextMultiLine  *CorpFieldTextMultiLine  `json:"fieldTextMultiLine,omitempty"`
	FieldMultiCheckbox  *CorpFieldMultiCheckbox  `json:"fieldMultiCheckbox,omitempty"`
}

type CorpFieldMultiCheckbox struct {
	Required     bool     `json:"required,omitempty"`
	Option       []string `json:"option,omitempty"`
	DefaultValue []bool   `json:"defaultValue,omitempty"`
}

type CorpFieldTextMultiLine struct {
	DefaultValue string `json:"defaultValue,omitempty"`
	Tips         string `json:"tips,omitempty"`
	Required     bool   `json:"required,omitempty"`
	FontType     string `json:"fontType,omitempty"`
	FontSize     int    `json:"fontSize,omitempty"`
	Width        int    `json:"width,omitempty"`
	Height       int    `json:"height,omitempty"`
	Alignment    string `json:"alignment,omitempty"`
}

type CorpFieldTextSingleLine struct {
	DefaultValue string `json:"defaultValue,omitempty"`
	Tips         string `json:"tips,omitempty"`
	Required     bool   `json:"required,omitempty"`
	FontType     string `json:"fontType,omitempty"`
	FontSize     int    `json:"fontSize,omitempty"`
	Width        int    `json:"width,omitempty"`
	Height       int    `json:"height,omitempty"`
	Alignment    string `json:"alignment,omitempty"`
}
