package sealRequestModel

// GetSealListReq 获取印章列表
type GetSealListReq struct {
	OwnerId       string          `json:"ownerId,omitempty"`
	OpenCorpId    string          `json:"openCorpId,omitempty"`
	GrantFreeSign string          `json:"grantFreeSign,omitempty"`
	ListFilter    *SealListFilter `json:"listFilter,omitempty"`
}

type SealListFilter struct {
	CategoryType []string `json:"categoryType,omitempty"`
	SealTags     []string `json:"sealTags,omitempty"`
}
