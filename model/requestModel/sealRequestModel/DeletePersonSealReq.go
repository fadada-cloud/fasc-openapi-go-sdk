package sealRequestModel

type DeletePersonSealReq struct {
	OpenUserId string `json:"openUserId,omitempty"`
	SealId     int64  `json:"sealId"`
}
