package sealRequestModel

type CreatePersonalSealByImageReq struct {
	OpenUserId     string   `json:"openUserId"`
	SealName       string   `json:"sealName"`
	CreateSerialNo string   `json:"createSerialNo"`
	SealImage      string   `json:"sealImage"`
	SealWidth      *int     `json:"sealWidth,omitempty"`
	SealHeight     *int     `json:"sealHeight,omitempty"`
	SealOldStyle   *float64 `json:"sealOldStyle,omitempty"`
	SealColor      string   `json:"sealColor"`
}
