package sealRequestModel

type CancelPersonSealFreeSignReq struct {
	OpenUserId string `json:"openUserId,omitempty"`
	SealId     *int64 `json:"sealId"`
	BusinessId string `json:"businessId,omitempty"`
}
