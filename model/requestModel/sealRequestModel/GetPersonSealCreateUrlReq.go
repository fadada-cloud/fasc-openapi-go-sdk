package sealRequestModel

type GetPersonSealCreateUrlReq struct {
	ClientUserId   string   `json:"clientUserId,omitempty"`
	CreateMethod   []string `json:"createMethod,omitempty"`
	CreateSerialNo string   `json:"createSerialNo,omitempty"`
	RedirectUrl    string   `json:"redirectUrl,omitempty"`
}
