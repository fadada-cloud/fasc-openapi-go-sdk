package sealRequestModel

type CreatePersonalSealByTemplateReq struct {
	OpenUserId        string `json:"openUserId"`
	SealName          string `json:"sealName"`
	CreateSerialNo    string `json:"createSerialNo"`
	SealTemplateStyle string `json:"sealTemplateStyle"`
	SealSize          string `json:"sealSize"`
	SealSuffix        string `json:"sealSuffix"`
	SealColor         string `json:"sealColor"`
}
