package sealRequestModel

type CreateLegalRepresentativeSealByTemplateReq struct {
	OpenCorpId        string `json:"openCorpId"`
	EntityId          string `json:"entityId"`
	OpenUserId        string `json:"openUserId"`
	SealName          string `json:"sealName"`
	SealTag           string `json:"sealTag"`
	SecurityCode      string `json:"securityCode"`
	CreateSerialNo    string `json:"createSerialNo"`
	SealTemplateStyle string `json:"sealTemplateStyle"`
	SealSize          string `json:"sealSize"`
	SealSuffix        string `json:"sealSuffix"`
	SealColor         string `json:"sealColor"`
}
