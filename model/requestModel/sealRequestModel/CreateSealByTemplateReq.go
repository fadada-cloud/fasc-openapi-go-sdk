package sealRequestModel

type CreateSealByTemplateReq struct {
	OpenCorpId         string `json:"openCorpId"`
	EntityId           string `json:"entityId"`
	SealName           string `json:"sealName"`
	CategoryType       string `json:"categoryType"`
	SealTag            string `json:"sealTag"`
	SealTemplateStyle  string `json:"sealTemplateStyle"`
	SealSize           string `json:"sealSize"`
	SealHorizontalText string `json:"sealHorizontalText"`
	SealBottomText     string `json:"sealBottomText"`
	SealColor          string `json:"sealColor"`
	CreateSerialNo     string `json:"createSerialNo"`
}
