package sealRequestModel

type CancelCorpSealFreeSignReq struct {
	OpenCorpId string `json:"openCorpId,omitempty"`
	SealId     *int64 `json:"sealId"`
	BusinessId string `json:"businessId,omitempty"`
}
