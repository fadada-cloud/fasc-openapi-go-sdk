package sealRequestModel

type GetPersonSealManageUrlReq struct {
	ClientUserId string `json:"clientUserId,omitempty"`
	RedirectUrl  string `json:"redirectUrl,omitempty"`
}
