package sealRequestModel

type CreateSealByImageReq struct {
	OpenCorpId     string   `json:"openCorpId"`
	EntityId       string   `json:"entityId"`
	SealName       string   `json:"sealName"`
	CategoryType   string   `json:"categoryType"`
	SealTag        string   `json:"sealTag"`
	CreateSerialNo string   `json:"createSerialNo"`
	SealImage      string   `json:"sealImage"`
	SealWidth      *int     `json:"sealWidth,omitempty"`
	SealHeight     *int     `json:"sealHeight,omitempty"`
	SealOldStyle   *float64 `json:"sealOldStyle,omitempty"`
	SealColor      string   `json:"sealColor"`
}
