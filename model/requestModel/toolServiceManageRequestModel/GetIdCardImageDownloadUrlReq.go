package toolServiceManageRequestModel

type GetIdCardImageDownloadUrlReq struct {
	VerifyId string `json:"verifyId,omitempty"`
}
