package toolServiceManageRequestModel

type GetIdCardThreeElementVerifyReq struct {
	UserName    string `json:"userName"`
	UserIdentNo string `json:"userIdentNo"`
	ImgBase64   string `json:"imgBase64"`
}
