package toolServiceManageRequestModel

type GetMobileThreeElementTransactionIdReq struct {
	UserName       string `json:"userName"`
	UserIdentNo    string `json:"userIdentNo"`
	Mobile         string `json:"mobile"`
	CreateSerialNo string `json:"createSerialNo"`
}
