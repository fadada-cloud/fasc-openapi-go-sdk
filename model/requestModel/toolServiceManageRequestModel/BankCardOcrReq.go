package toolServiceManageRequestModel

type BankCardOcrReq struct {
	ImageBase64 string `json:"imageBase64"`
}
