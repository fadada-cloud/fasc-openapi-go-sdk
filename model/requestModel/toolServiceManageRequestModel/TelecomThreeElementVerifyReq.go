package toolServiceManageRequestModel

type TelecomThreeElementVerifyReq struct {
	UserName    string `json:"userName,omitempty"`
	UserIdentNo string `json:"userIdentNo,omitempty"`
	Mobile      string `json:"mobile,omitempty"`
}
