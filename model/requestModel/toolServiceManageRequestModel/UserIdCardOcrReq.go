package toolServiceManageRequestModel

type UserIdCardOcrReq struct {
	FaceSide           string `json:"faceSide,omitempty"`
	NationalEmblemSide string `json:"nationalEmblemSide,omitempty"`
}
