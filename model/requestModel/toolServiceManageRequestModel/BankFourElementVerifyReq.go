package toolServiceManageRequestModel

type BankFourElementVerifyReq struct {
	UserName      string `json:"userName,omitempty"`
	UserIdentNo   string `json:"userIdentNo,omitempty"`
	BankAccountNo string `json:"bankAccountNo,omitempty"`
	Mobile        string `json:"mobile,omitempty"`
}
