package toolServiceManageRequestModel

type GetBusinessFourElementReq struct {
	CorpName         string `json:"corpName"`
	CorpIdentNo      string `json:"corpIdentNo"`
	LegalRepName     string `json:"legalRepName"`
	LegalRepIdCertNo string `json:"legalRepIdCertNo"`
}
