package toolServiceManageRequestModel

type GetFaceRecognitionStatusReq struct {
	SerialNo string `json:"serialNo"`
	GetFile  int    `json:"getFile,omitempty"`
}
