package toolServiceManageRequestModel

type GetFaceRecognitionUrlReq struct {
	UserIdentType string `json:"userIdentType"`
	UserName      string `json:"userName"`
	UserIdentNo   string `json:"userIdentNo"`
	FaceAuthMode  string `json:"faceAuthMode"`
	RedirectUrl   string `json:"redirectUrl"`
}
