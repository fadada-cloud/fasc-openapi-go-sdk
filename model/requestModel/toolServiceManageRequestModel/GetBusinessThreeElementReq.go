package toolServiceManageRequestModel

type GetBusinessThreeElementReq struct {
	CorpName     string `json:"corpName"`
	CorpIdentNo  string `json:"corpIdentNo"`
	LegalRepName string `json:"legalRepName"`
}
