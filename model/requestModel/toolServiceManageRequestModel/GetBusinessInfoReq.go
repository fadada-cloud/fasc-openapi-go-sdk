package toolServiceManageRequestModel

type GetBusinessInfoReq struct {
	CorpName    string `json:"corpName"`
	CorpIdentNo string `json:"corpIdentNo"`
}
