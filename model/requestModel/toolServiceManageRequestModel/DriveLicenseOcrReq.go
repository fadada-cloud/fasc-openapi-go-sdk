package toolServiceManageRequestModel

type DriveLicenseOcrReq struct {
	ImageBase64     string `json:"imageBase64"`
	BackImageBase64 string `json:"backImageBase64"`
}
