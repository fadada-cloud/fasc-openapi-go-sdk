package toolServiceManageRequestModel

type VerifyAuthCodeReq struct {
	TransactionId string `json:"transactionId"`
	AuthCode      string `json:"authCode"`
}
