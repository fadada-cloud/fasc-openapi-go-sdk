package toolServiceManageRequestModel

type BankThreeElementVerifyReq struct {
	UserName      string `json:"userName"`
	UserIdentNo   string `json:"userIdentNo"`
	BankAccountNo string `json:"bankAccountNo"`
}
