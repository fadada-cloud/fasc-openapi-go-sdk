package toolServiceManageRequestModel

type IdentityTwoElementVerifyReq struct {
	UserName    string `json:"userName"`
	UserIdentNo string `json:"userIdentNo"`
}
