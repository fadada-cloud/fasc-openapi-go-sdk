package toolServiceManageRequestModel

type GetBankcardFourElementTransactionIdReq struct {
	UserName       string `json:"userName"`
	UserIdentNo    string `json:"userIdentNo"`
	BankAccountNo  string `json:"bankAccountNo"`
	Mobile         string `json:"mobile"`
	CreateSerialNo string `json:"createSerialNo"`
}
