package draftRequestModel

type GetDraftManageUrlReq struct {
	OpenCorpId   string `json:"openCorpId"`
	ClientUserId string `json:"clientUserId"`
	RedirectUrl  string `json:"redirectUrl"`
}
