package draftRequestModel

type GetDraftEditUrlReq struct {
	OpenCorpId        string `json:"openCorpId"`
	ClientUserId      string `json:"clientUserId"`
	ContractConsultId string `json:"contractConsultId"`
	UrlType           string `json:"urlType"`
	RedirectUrl       string `json:"redirectUrl"`
}
