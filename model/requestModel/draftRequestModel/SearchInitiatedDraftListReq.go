package draftRequestModel

type SearchInitiatedDraftListReq struct {
	OpenCorpId        string                          `json:"openCorpId"`
	InitiatorMemberId string                          `json:"initiatorMemberId"`
	ListFilter        *SearchInitiatedDraftListFilter `json:"listFilter"`
	ListPageNo        int                             `json:"listPageNo"`
	ListPageSize      int                             `json:"listPageSize"`
}

type SearchInitiatedDraftListFilter struct {
	ContractSubject string   `json:"contractSubject"`
	ContractStatus  []string `json:"contractStatus"`
	StartTime       string   `json:"startTime"`
	EndTime         string   `json:"endTime"`
}
