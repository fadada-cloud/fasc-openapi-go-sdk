package draftRequestModel

type GetDraftInviteUrlReq struct {
	ContractConsultId string `json:"contractConsultId"`
	ExpireTime        string `json:"expireTime"`
	InnerPermission   string `json:"innerPermission"`
	OuterPermission   string `json:"outerPermission"`
	TouristView       string `json:"touristView"`
	RedirectUrl       string `json:"redirectUrl"`
}
