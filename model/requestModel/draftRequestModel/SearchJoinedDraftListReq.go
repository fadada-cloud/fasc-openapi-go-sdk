package draftRequestModel

type SearchJoinedDraftListReq struct {
	OpenCorpId     string                       `json:"openCorpId"`
	JoinedMemberId string                       `json:"joinedMemberId"`
	ListFilter     *SearchJoinedDraftListFilter `json:"listFilter"`
	ListPageNo     int                          `json:"listPageNo"`
	ListPageSize   int                          `json:"listPageSize"`
	AccessToken    string                       `json:"accessToken"`
}

type SearchJoinedDraftListFilter struct {
	ContractSubject string   `json:"contractSubject"`
	ContractStatus  []string `json:"contractStatus"`
	StartTime       string   `json:"startTime"`
	EndTime         string   `json:"endTime"`
}
