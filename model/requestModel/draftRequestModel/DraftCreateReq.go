package draftRequestModel

type DraftCreateReq struct {
	OpenCorpId        string `json:"openCorpId"`
	InitiatorMemberId string `json:"initiatorMemberId"`
	ContractSubject   string `json:"contractSubject"`
	FileId            string `json:"fileId"`
}
