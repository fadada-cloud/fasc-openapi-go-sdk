package draftRequestModel

type DraftAddMembersReq struct {
	ContractConsultId string  `json:"contractConsultId"`
	OpenCorpId        string  `json:"openCorpId"`
	MemberIds         []int64 `json:"memberIds"`
	Permission        string  `json:"permission"`
}
