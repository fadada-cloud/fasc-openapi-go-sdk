package approvalRequestModel

type GetApprovalFlowDetailReq struct {
	OpenCorpId     string `json:"openCorpId"`
	ApprovalFlowId string `json:"approvalFlowId"`
	ApprovalType   string `json:"approvalType"`
}
