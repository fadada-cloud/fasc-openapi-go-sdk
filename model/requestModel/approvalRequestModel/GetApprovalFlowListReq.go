package approvalRequestModel

type GetApprovalFlowListReq struct {
	OpenCorpId   string `json:"openCorpId"`
	ApprovalType string `json:"approvalType"`
}
