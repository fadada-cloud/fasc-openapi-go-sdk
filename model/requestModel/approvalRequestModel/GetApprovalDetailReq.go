package approvalRequestModel

type GetApprovalDetailReq struct {
	OpenCorpId   string `json:"openCorpId,omitempty"`
	ApprovalType string `json:"approvalType,omitempty"`
	ApprovalId   string `json:"approvalId,omitempty"`
}
