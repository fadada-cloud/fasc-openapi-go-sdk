package approvalRequestModel

type GetApprovalUrlReq struct {
	OpenCorpId   string `json:"openCorpId,omitempty"`
	ClientUserId string `json:"clientUserId,omitempty"`
	ApprovalId   string `json:"approvalId,omitempty"`
	RedirectUrl  string `json:"redirectUrl,omitempty"`
}
