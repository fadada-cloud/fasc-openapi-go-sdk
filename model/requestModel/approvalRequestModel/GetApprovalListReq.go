package approvalRequestModel

type GetApprovalListReq struct {
	OpenCorpId     string   `json:"openCorpId,omitempty"`
	ApprovalType   string   `json:"approvalType,omitempty"`
	ApprovalStatus []string `json:"approvalStatus"`
	ListPageNo     int      `json:"listPageNo,omitempty"`
	ListPageSize   int      `json:"listPageSize,omitempty"`
}
