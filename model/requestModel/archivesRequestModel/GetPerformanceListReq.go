package archivesRequestModel

type GetPerformanceListReq struct {
	OpenCorpId string `json:"openCorpId"`
	MemberId   string `json:"memberId"`
	ArchivesId string `json:"archivesId"`
}
