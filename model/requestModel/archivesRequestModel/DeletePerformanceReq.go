package archivesRequestModel

type DeletePerformanceReq struct {
	OpenCorpId    string `json:"openCorpId"`
	ArchivesId    string `json:"archivesId"`
	PerformanceId string `json:"performanceId"`
}
