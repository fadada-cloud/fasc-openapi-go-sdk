package archivesRequestModel

type ArchivesCatalogListReq struct {
	OpenCorpId string `json:"openCorpId"`
	CatalogId  string `json:"catalogId"`
}
