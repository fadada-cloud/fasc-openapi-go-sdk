package archivesRequestModel

type ContactArchivedReq struct {
	OpenCorpId   string           `json:"openCorpId"`
	ContractType string           `json:"contractType"`
	ContractNo   string           `json:"contractNo"`
	CatalogId    string           `json:"catalogId"`
	SignTaskId   string           `json:"signTaskId"`
	FileId       string           `json:"fileId"`
	MemberId     string           `json:"memberId"`
	Attachments  []AttachmentInfo `json:"attachments"`
}

type AttachmentInfo struct {
	AttachmentId   string `json:"attachmentId"`
	AttachmentName string `json:"attachmentName"`
}
