package archivesRequestModel

type ArchivedListReq struct {
	OpenCorpId   string `json:"openCorpId"`
	ContractType string `json:"contractType"`
	SignTaskId   string `json:"signTaskId"`
	ListPageNo   int    `json:"listPageNo"`
	ListPageSize int    `json:"listPageSize"`
}
