package archivesRequestModel

type ArchivesDetailReq struct {
	OpenCorpId string `json:"openCorpId"`
	ArchivesId string `json:"archivesId"`
}
