package archivesRequestModel

type CreateOrModifyPerformanceReq struct {
	OpenCorpId      string   `json:"openCorpId"`
	ArchivesId      string   `json:"archivesId"`
	PerformanceId   string   `json:"performanceId"`
	PerformanceType string   `json:"performanceType"`
	PerformanceName string   `json:"performanceName"`
	ExpireTime      string   `json:"expireTime"`
	RemindStartDate string   `json:"remindStartDate"`
	RemindFrequency string   `json:"remindFrequency"`
	CycleDays       int      `json:"cycleDays"`
	RemindTime      string   `json:"remindTime"`
	Amount          string   `json:"amount"`
	Reminder        []string `json:"reminder"`
	IsRemind        bool     `json:"isRemind"`
}
