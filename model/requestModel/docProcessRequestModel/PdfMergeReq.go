package docProcessRequestModel

type PdfMergeReq struct {
	FddFileUrlList []string `json:"fddFileUrlList"`
	FileName       string   `json:"fileName"`
}
