package docProcessRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetExamineUrlReq struct {
	Initiator *commonModel.OpenId `json:"initiator,omitempty"`
	FileId    string              `json:"fileId,omitempty"`
}
