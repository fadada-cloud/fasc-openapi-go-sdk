package docProcessRequestModel

type OfdMergeReq struct {
	FileId        string `json:"fileId"`
	InsertFileId  string `json:"insertFileId"`
	MergeFileName string `json:"mergeFileName"`
}
