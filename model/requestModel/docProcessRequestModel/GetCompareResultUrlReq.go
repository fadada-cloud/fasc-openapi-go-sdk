package docProcessRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetCompareResultUrlReq struct {
	Initiator *commonModel.OpenId `json:"initiator,omitempty"`
	CompareId string              `json:"compareId,omitempty"`
}
