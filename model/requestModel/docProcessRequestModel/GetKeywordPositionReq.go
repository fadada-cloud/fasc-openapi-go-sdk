package docProcessRequestModel

type GetKeywordPositionReq struct {
	FileId   string   `json:"fileId"`
	Keywords []string `json:"keywords"`
}
