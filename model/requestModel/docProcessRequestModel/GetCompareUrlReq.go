package docProcessRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetCompareUrlReq struct {
	Initiator    *commonModel.OpenId `json:"initiator,omitempty"`
	OriginFileId string              `json:"originFileId,omitempty"`
	TargetFileId string              `json:"targetFileId,omitempty"`
}
