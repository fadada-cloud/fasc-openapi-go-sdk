package docProcessRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetExamineResultUrlReq struct {
	Initiator *commonModel.OpenId `json:"initiator,omitempty"`
	ExamineId string              `json:"examineId,omitempty"`
}
