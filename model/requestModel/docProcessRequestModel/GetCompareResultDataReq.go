package docProcessRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetCompareResultDataReq struct {
	Initiator *commonModel.OpenId `json:"initiator"`
	CompareId string              `json:"compareId"`
}
