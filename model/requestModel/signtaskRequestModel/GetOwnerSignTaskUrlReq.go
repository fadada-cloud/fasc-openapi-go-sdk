package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

// GetOwnerSignTaskUrlReq 获取指定归属方的签署任务文档下载地址
type GetOwnerSignTaskUrlReq struct {
	OwnerId           *commonModel.OpenId `json:"ownerId,omitempty"`
	SignTaskId        string              `json:"signTaskId,omitempty"`
	FileType          string              `json:"fileType,omitempty"`
	Id                string              `json:"id,omitempty"`
	CustomName        string              `json:"customName,omitempty"`
	Compression       bool                `json:"compression"`
	FolderBySigntask  bool                `json:"folderBySigntask"`
	BatchDownloadInfo *BatchDownloadInfo  `json:"batchDownloadInfo"`
}

type BatchDownloadInfo struct {
	BatchSignTaskId string                            `json:"batchSignTaskId"`
	DocInfos        []BatchDownloadSignTaskDocInfo    `json:"docInfos"`
	AttachInfos     []BatchDownloadSignTaskAttachInfo `json:"attachInfos"`
}

type BatchDownloadSignTaskDocInfo struct {
	DocId         string `json:"docId"`
	CustomDocName string `json:"customDocName"`
}

type BatchDownloadSignTaskAttachInfo struct {
	AttachId         string `json:"attachId"`
	CustomAttachName string `json:"customAttachName"`
}
