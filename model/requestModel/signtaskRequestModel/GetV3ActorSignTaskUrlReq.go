package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetV3ActorSignTaskUrlReq struct {
	SignTaskId  string              `json:"signTaskId"`
	OwnerId     *commonModel.OpenId `json:"ownerId,omitempty"`
	RedirectUrl string              `json:"redirectUrl"`
}
