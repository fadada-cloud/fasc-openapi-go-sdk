package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

// AddSignTaskDocReq 添加签署任务文档信息
type AddSignTaskDocReq struct {
	SignTaskId string            `json:"signTaskId,omitempty"`
	Docs       []commonModel.Doc `json:"docs,omitempty"`
}
