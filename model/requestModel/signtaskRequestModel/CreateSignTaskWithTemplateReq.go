package signtaskRequestModel

import (
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
)

// CreateSignTaskWithTemplateReq 创建签署任务基于签署模板
type CreateSignTaskWithTemplateReq struct {
	SignTaskSubject        string                      `json:"signTaskSubject,omitempty"`
	Initiator              *commonModel.OpenId         `json:"initiator,omitempty"`
	InitiatorMemberId      string                      `json:"initiatorMemberId,omitempty"`
	InitiatorEntityId      string                      `json:"initiatorEntityId,omitempty"`
	SignDocType            string                      `json:"signDocType,omitempty"`
	SignTemplateId         string                      `json:"signTemplateId,omitempty"`
	BusinessTypeId         *int64                      `json:"businessTypeId"`
	BusinessCode           string                      `json:"businessCode,omitempty"`
	StartApprovalFlowId    string                      `json:"startApprovalFlowId,omitempty"`
	FinalizeApprovalFlowId string                      `json:"finalizeApprovalFlowId,omitempty"`
	ExpiresTime            string                      `json:"expiresTime,omitempty"`
	DueDate                string                      `json:"dueDate,omitempty"`
	AutoStart              bool                        `json:"autoStart"`
	AutoFillFinalize       bool                        `json:"autoFillFinalize"`
	AutoFinish             bool                        `json:"autoFinish"`
	CertCAOrg              string                      `json:"certCAOrg,omitempty"`
	CatalogId              string                      `json:"catalogId,omitempty"`
	BusinessId             string                      `json:"businessId,omitempty"`
	TransReferenceId       string                      `json:"transReferenceId,omitempty"`
	BusinessScene          *BusinessSceneInfo          `json:"businessScene,omitempty"`
	Actors                 []commonModel.SignTaskActor `json:"actors,omitempty"`
	Watermarks             []commonModel.Watermark     `json:"watermarks,omitempty"`
}

type BusinessSceneInfo struct {
	BusinessId       string `json:"businessId,omitempty"`
	TransReferenceId string `json:"transReferenceId,omitempty"`
}
