package signtaskRequestModel

import (
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
)

// GetOwnerSignTaskListReq 获取指定归属方的签署任务列表
type GetOwnerSignTaskListReq struct {
	OwnerId      *commonModel.OpenId `json:"ownerId,omitempty"`
	OwnerRole    string              `json:"ownerRole,omitempty"`
	MemberId     string              `json:"memberId"`
	ListFilter   *SignTaskListFilter `json:"listFilter,omitempty"`
	ListPageNo   int                 `json:"listPageNo,omitempty"`
	ListPageSize int                 `json:"listPageSize,omitempty"`
}

type SignTaskListFilter struct {
	SignTaskSubject  string   `json:"signTaskSubject"`
	SignTaskStatus   []string `json:"signTaskStatus"`
	TransReferenceId string   `json:"transReferenceId"`
	StartTimeFrom    string   `json:"startTimeFrom"`
	StartTimeTo      string   `json:"startTimeTo"`
	FinishTimeFrom   string   `json:"finishTimeFrom"`
	FinishTimeTo     string   `json:"finishTimeTo"`
	ExpiresTimeFrom  string   `json:"expiresTimeFrom"`
	ExpiresTimeTo    string   `json:"expiresTimeTo"`
}
