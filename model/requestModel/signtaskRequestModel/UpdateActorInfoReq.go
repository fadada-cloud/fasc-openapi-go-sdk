package signtaskRequestModel

type UpdateActorInfoReq struct {
	SignTaskId string        `json:"signTaskId,omitempty"`
	BusinessId string        `json:"businessId,omitempty"`
	Actors     []UpdateActor `json:"actors,omitempty"`
}

type UpdateActor struct {
	ActorId        string                `json:"actorId"`
	FillFields     []UpdateFillField     `json:"fillFields"`
	SignFields     []UpdateSignField     `json:"signFields"`
	SignConfigInfo *UpdateSignConfigInfo `json:"signConfigInfo"`
}

type UpdateFillField struct {
	FieldDocId string `json:"fieldDocId"`
	FieldId    string `json:"fieldId"`
	FieldName  string `json:"fieldName"`
	FieldValue string `json:"fieldValue"`
}

type UpdateSignField struct {
	FieldDocId string `json:"fieldDocId"`
	FieldId    string `json:"fieldId"`
	FieldName  string `json:"fieldName"`
	SealId     *int64 `json:"sealId"`
	Moveable   bool   `json:"moveable"`
}

type UpdateSignConfigInfo struct {
	RequestVerifyFree bool   `json:"requestVerifyFree"`
	SignerSignMethod  string `json:"signerSignMethod"`
	ReadingToEnd      bool   `json:"readingToEnd"`
	ReadingTime       int    `json:"readingTime"`
	RequestMemberSign bool   `json:"requestMemberSign"`
	ResizeSeal        bool   `json:"resizeSeal"`
	FreeDragSealId    *int64 `json:"freeDragSealId"`
	SignAllDoc        bool   `json:"signAllDoc"`
	AuthorizeFreeSign bool   `json:"authorizeFreeSign"`
}
