package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetSignTaskSlicingTicketIdReq struct {
	OwnerId    *commonModel.OpenId `json:"ownerId,omitempty"`
	SignTaskId string              `json:"SignTaskId,omitempty"`
}
