package signtaskRequestModel

type GetSignTaskFileReq struct {
	SignTaskId string `json:"signTaskId"`
	DocId      string `json:"docId"`
}
