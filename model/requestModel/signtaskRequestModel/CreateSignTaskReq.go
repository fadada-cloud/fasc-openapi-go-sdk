package signtaskRequestModel

import (
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
)

// CreateSignTaskReq 创建签署任务
type CreateSignTaskReq struct {
	SignTaskSubject        string                      `json:"signTaskSubject,omitempty"`
	Initiator              *commonModel.OpenId         `json:"initiator,omitempty"`
	InitiatorMemberId      string                      `json:"initiatorMemberId,omitempty"`
	InitiatorEntityId      string                      `json:"initiatorEntityId,omitempty"`
	SignDocType            string                      `json:"signDocType,omitempty"`
	ExpiresTime            string                      `json:"expiresTime,omitempty"`
	DueDate                string                      `json:"dueDate,omitempty"`
	AutoStart              bool                        `json:"autoStart"`
	AutoFillFinalize       bool                        `json:"autoFillFinalize"`
	BusinessTypeId         *int64                      `json:"businessTypeId"`
	BusinessCode           string                      `json:"businessCode,omitempty"`
	StartApprovalFlowId    string                      `json:"startApprovalFlowId,omitempty"`
	FinalizeApprovalFlowId string                      `json:"finalizeApprovalFlowId,omitempty"`
	SignInOrder            bool                        `json:"signInOrder"`
	AutoFinish             bool                        `json:"autoFinish"`
	CertCAOrg              string                      `json:"certCAOrg,omitempty"`
	CatalogId              string                      `json:"catalogId,omitempty"`
	FileFormat             string                      `json:"fileFormat,omitempty"`
	BusinessId             string                      `json:"businessId,omitempty"`
	TransReferenceId       string                      `json:"transReferenceId,omitempty"`
	BusinessScene          *BusinessSceneInfo          `json:"businessScene,omitempty"`
	Docs                   []commonModel.Doc           `json:"docs,omitempty"`
	Attachs                []commonModel.Attach        `json:"attachs,omitempty"`
	Actors                 []commonModel.SignTaskActor `json:"actors,omitempty"`
	Watermarks             []commonModel.Watermark     `json:"watermarks,omitempty"`
}
