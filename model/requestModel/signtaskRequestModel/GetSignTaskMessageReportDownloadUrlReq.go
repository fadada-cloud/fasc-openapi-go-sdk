package signtaskRequestModel

type GetSignTaskMessageReportDownloadUrlReq struct {
	SignTaskId string `json:"signTaskId,omitempty"`
}
