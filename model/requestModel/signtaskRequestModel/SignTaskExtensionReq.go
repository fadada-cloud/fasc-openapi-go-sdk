package signtaskRequestModel

type SignTaskExtensionReq struct {
	SignTaskId    string `json:"signTaskId,omitempty"`
	ExtensionTime string `json:"extensionTime,omitempty"`
}
