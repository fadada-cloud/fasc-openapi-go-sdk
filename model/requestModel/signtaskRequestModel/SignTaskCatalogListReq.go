package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type SignTaskCatalogListReq struct {
	OwnerId *commonModel.OpenId `json:"ownerId,omitempty"`
}
