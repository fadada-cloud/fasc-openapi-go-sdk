package signtaskRequestModel

type SignTaskGetCerInfoReq struct {
	SignTaskId string `json:"signTaskId"`
	ActorId    string `json:"actorId"`
}
