package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetBatchSignUrlReq struct {
	OwnerId            *commonModel.OpenId `json:"ownerId,omitempty"`
	IdType             string              `json:"idType,omitempty"`
	OpenId             string              `json:"openId,omitempty"`
	AccountName        string              `json:"accountName,omitempty"`
	MemberId           *int64              `json:"memberId,omitempty"`
	ClientUserId       string              `json:"clientUserId,omitempty"`
	SignTaskIds        []string            `json:"signTaskIds,omitempty"`
	SignAllTasks       bool                `json:"signAllTasks"`
	RedirectUrl        string              `json:"redirectUrl,omitempty"`
	RedirectMiniAppUrl string              `json:"redirectMiniAppUrl,omitempty"`
}
