package signtaskRequestModel

type SignTaskActorRemoveReq struct {
	SignTaskId string `json:"signTaskId,omitempty'"`
	ActorId    string `json:"actorId,omitempty"`
}
