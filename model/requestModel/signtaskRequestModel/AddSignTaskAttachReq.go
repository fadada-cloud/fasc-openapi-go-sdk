package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

// AddSignTaskAttachReq 添加签署任务附件

type AddSignTaskAttachReq struct {
	SignTaskId string               `json:"signTaskId,omitempty"`
	Attachs    []commonModel.Attach `json:"attachs,omitempty"`
}
