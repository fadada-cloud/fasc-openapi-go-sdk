package signtaskRequestModel

type IgnoreFillValueSignTaskReq struct {
	SignTaskId string                         `json:"signTaskId"`
	Actors     []IgnoreFillValueSignTaskActor `json:"actors"`
}

type IgnoreFillValueSignTaskActor struct {
	ActorId string `json:"actorId"`
	Reason  string `json:"reason"`
}
