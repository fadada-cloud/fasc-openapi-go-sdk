package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type AddSignTaskField struct {
	DocId     string              `json:"docId,omitempty"`
	DocFields []commonModel.Field `json:"docFields,omitempty"`
}

// AddSignTaskFieldReq 添加签署任务控件
type AddSignTaskFieldReq struct {
	SignTaskId string             `json:"signTaskId,omitempty"`
	ActorId    string             `json:"actorId,omitempty"`
	Fields     []AddSignTaskField `json:"fields,omitempty"`
}
