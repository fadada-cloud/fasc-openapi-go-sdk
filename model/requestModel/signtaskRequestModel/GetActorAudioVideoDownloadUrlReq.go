package signtaskRequestModel

type GetActorAudioVideoDownloadUrlReq struct {
	SignTaskId string `json:"signTaskId,omitempty"`
	ActorId    string `json:"actorId,omitempty"`
}
