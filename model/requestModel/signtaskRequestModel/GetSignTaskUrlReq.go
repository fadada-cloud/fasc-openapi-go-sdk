package signtaskRequestModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetSignTaskUrlReq struct {
	SignTaskId      string              `json:"signTaskId,omitempty"`
	Initiator       *commonModel.OpenId `json:"initiator,omitempty"`
	NonEditableInfo []string            `json:"nonEditableInfo,omitempty"`
	RedirectUrl     string              `json:"redirectUrl,omitempty"`
}
