package corpManageRequestModel

type GetChangeCorpIdentityInfoUrlReq struct {
	OpenCorpId   string `json:"openCorpId"`
	ClientCorpId string `json:"clientCorpId"`
	ClientUserId string `json:"clientUserId"`
	RedirectUrl  string `json:"redirectUrl"`
}
