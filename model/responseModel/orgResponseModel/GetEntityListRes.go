package orgResponseModel

type GetEntityListRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      []GetEntityListData `json:"data"`
}

type GetEntityListData struct {
	EntityId     string `json:"entityId"`
	EntityType   string `json:"entityType"`
	CorpName     string `json:"corpName"`
	CorpIdentNo  string `json:"corpIdentNo"`
	LegalRepName string `json:"legalRepName"`
	IdentStatus  string `json:"identStatus"`
}
