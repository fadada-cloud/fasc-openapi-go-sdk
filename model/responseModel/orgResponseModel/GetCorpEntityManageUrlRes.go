package orgResponseModel

type GetCorpEntityManageUrlRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      GetCorpEntityManageUrlData `json:"data"`
}

type GetCorpEntityManageUrlData struct {
	EntityManageUrl string `json:"entityManageUrl"`
}
