package archivesResponseModel

type ContactArchivedRes struct {
	RequestId string                 `json:"requestId"`
	Code      string                 `json:"code"`
	Msg       string                 `json:"msg"`
	Data      ContactArchivedResData `json:"data"`
}

type ContactArchivedResData struct {
	Archives []ArchiveInfo `json:"archivesRequestModel"`
}

type ArchiveInfo struct {
	ArchivesId string `json:"archivesId"`
}
