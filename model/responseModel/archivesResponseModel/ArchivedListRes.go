package archivesResponseModel

type ArchivedListRes struct {
	RequestId string           `json:"requestId"`
	Code      string           `json:"code"`
	Msg       string           `json:"msg"`
	Data      ArchivedListData `json:"data"`
}

type ArchivedListData struct {
	Archives      []ArchiveListInfo `json:"archivesRequestModel"`
	ListPageNo    int               `json:"listPageNo"`
	ListPageSize  int               `json:"listPageSize"`
	ListPageCount int               `json:"listPageCount"`
	Total         int               `json:"total"`
}

type ArchiveListInfo struct {
	ArchivesId          string               `json:"archivesId"`
	ContractName        string               `json:"contractName"`
	ContractNo          string               `json:"contractNo"`
	ContractType        string               `json:"contractType"`
	CatalogId           string               `json:"catalogId"`
	SignTaskId          string               `json:"signTaskId"`
	ArchiveTime         string               `json:"archiveTime"`
	MemberId            string               `json:"memberId"`
	AssociatedContracts []AssociatedContract `json:"associatedContracts"`
	Attachments         []Attachment         `json:"attachments"`
}

type AssociatedContract struct {
	ArchivesId   string `json:"archivesId"`
	ContractName string `json:"contractName"`
}

type Attachment struct {
	ArchivesId   string `json:"archivesId"`
	ContractName string `json:"contractName"`
}
