package archivesResponseModel

type GetPerformanceListRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      []GetPerformanceListData `json:"data"`
}

type GetPerformanceListData struct {
	ArchivesId      string   `json:"archivesId"`
	PerformanceId   string   `json:"performanceId"`
	PerformanceType string   `json:"performanceType"`
	PerformanceName string   `json:"performanceName"`
	ExpireTime      string   `json:"expireTime"`
	RemindFrequency string   `json:"remindFrequency"`
	CycleDays       int      `json:"cycleDays"`
	RemindTime      string   `json:"remindTime"`
	RemindStartDate string   `json:"remindStartDate"`
	Amount          string   `json:"amount"`
	Reminder        []string `json:"reminder"`
}
