package archivesResponseModel

type CreateOrModifyPerformanceRes struct {
	RequestId string                        `json:"requestId"`
	Code      string                        `json:"code"`
	Msg       string                        `json:"msg"`
	Data      CreateOrModifyPerformanceData `json:"data"`
}

type CreateOrModifyPerformanceData struct {
	PerformanceId string `json:"performanceId"`
}
