package archivesResponseModel

type ArchivedDetailInfoRes struct {
	RequestId string                 `json:"requestId"`
	Code      string                 `json:"code"`
	Msg       string                 `json:"msg"`
	Data      ArchivedDetailInfoData `json:"data"`
}

type ArchivedDetailInfoData struct {
	ArchivesId          string                     `json:"archivesId"`
	ContractName        string                     `json:"contractName"`
	ContractNo          string                     `json:"contractNo"`
	ContractType        string                     `json:"contractType"`
	CatalogId           string                     `json:"catalogId"`
	SignTaskId          string                     `json:"signTaskId"`
	ArchiveTime         string                     `json:"archiveTime"`
	MemberId            string                     `json:"memberId"`
	AssociatedContracts []AssociatedContractDetail `json:"associatedContracts"`
	Attachments         []AttachmentDetail         `json:"attachments"`
}

type AssociatedContractDetail struct {
	ArchivesId   string `json:"archivesId"`
	ContractName string `json:"contractName"`
}

type AttachmentDetail struct {
	ArchivesId   string `json:"archivesId"`
	ContractName string `json:"contractName"`
}
