package archivesResponseModel

type ArchivesCatalogListRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      []ArchivesCatalogListData `json:"data"`
}

type ArchivesCatalogListData struct {
	CatalogId       string `json:"catalogId"`
	CatalogName     string `json:"catalogName"`
	ParentCatalogId string `json:"parentCatalogId"`
}
