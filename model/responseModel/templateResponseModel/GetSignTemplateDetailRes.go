package templateResponseModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

// GetSignTemplateDetailRes 获取签署模板详情
type GetSignTemplateDetailRes struct {
	RequestId string                       `json:"requestId"`
	Code      string                       `json:"code"`
	Msg       string                       `json:"msg"`
	Data      GetSignTemplateDetailResData `json:"data"`
}

type Attach struct {
	AttachId   int    `json:"attachId,omitempty"`
	AttachName string `json:"attachName,omitempty"`
}

type GetSignTemplateDetailResData struct {
	SignTemplateId     string                  `json:"signTemplateId"`
	SignTemplateName   string                  `json:"signTemplateName"`
	BusinessTypeName   string                  `json:"businessTypeName"`
	CatalogName        string                  `json:"catalogName,omitempty"`
	CreateSerialNo     string                  `json:"createSerialNo,omitempty"`
	CreatorMemberName  string                  `json:"creatorMemberName"`
	StorageType        string                  `json:"storageType,omitempty"`
	SignTemplateStatus string                  `json:"signTemplateStatus"`
	CertCAOrg          string                  `json:"certCAOrg"`
	Docs               []Doc                   `json:"docs"`
	Attachs            []Attach                `json:"attachs"`
	SignInOrder        bool                    `json:"signInOrder"`
	Actors             []SignTaskTemActor      `json:"actors,omitempty"`
	Watermarks         []commonModel.Watermark `json:"watermarks,omitempty"`
	ApprovalInfos      []ApprovalInfo          `json:"approvalInfos,omitempty"`
}

type ApprovalInfo struct {
	ApprovalFlowId string `json:"approvalFlowId"`
	ApprovalType   string `json:"approvalType"`
}

type Doc struct {
	DocId     int                 `json:"docId,omitempty"`
	DocName   string              `json:"docName,omitempty"`
	DocFields []commonModel.Field `json:"docFields,omitempty"`
}

type SignTaskTemActor struct {
	ActorInfo      *ActorInfo                  `json:"actorInfo"`
	FillFields     []FillField                 `json:"fillFields"`
	SignFields     []SignField                 `json:"signFields"`
	SignConfigInfo *commonModel.SignConfigInfo `json:"signConfigInfo"`
	Notification   *SignTemplateNotification   `json:"notification"`
}

type ActorInfo struct {
	ActorId           string   `json:"actorId,omitempty"`
	ActorType         string   `json:"actorType"`
	IdentNameForMatch string   `json:"identNameForMatch"`
	CertType          string   `json:"certType"`
	CertNoForMatch    string   `json:"certNoForMatch"`
	MemberIds         []string `json:"memberIds"`
	IsInitiator       bool     `json:"isInitiator,omitempty"`
	Permissions       []string `json:"permissions,omitempty"`
}

type SignTemplateNotification struct {
	NotifyWay     string `json:"notifyWay"`
	NotifyAddress string `json:"notifyAddress"`
}

type FillField struct {
	FieldDocId string `json:"fieldDocId,omitempty"`
	FieldId    string `json:"fieldId,omitempty"`
}

type SignField struct {
	FieldDocId   string `json:"fieldDocId,omitempty"`
	FieldId      string `json:"fieldId,omitempty"`
	SealId       *int64 `json:"sealId,omitempty"`
	CategoryType string `json:"categoryType"`
}
