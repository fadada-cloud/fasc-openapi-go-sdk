package templateResponseModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type AppDocTemplateDetailRes struct {
	RequestId string                   `json:"requestId,omitempty"`
	Code      string                   `json:"code,omitempty"`
	Msg       string                   `json:"msg,omitempty"`
	Data      AppDocTemplateDetailData `json:"data,omitempty"`
}

type AppDocTemplateDetailData struct {
	AppDocTemplateId     string              `json:"appDocTemplateId,omitempty"`
	AppDocTemplateName   string              `json:"appDocTemplateName,omitempty"`
	AppDocTemplateStatus string              `json:"appDocTemplateStatus,omitempty"`
	DocFields            []commonModel.Field `json:"docFields,omitempty"`
}
