package templateResponseModel

import "gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"

type GetDocTemplateDetailRes struct {
	RequestId string                      `json:"requestId"`
	Code      string                      `json:"code"`
	Msg       string                      `json:"msg"`
	Data      GetDocTemplateDetailResData `json:"data"`
}

type GetDocTemplateDetailResData struct {
	DocTemplateId     string              `json:"docTemplateId"`
	DocTemplateName   string              `json:"docTemplateName"`
	CreatorMemberName string              `json:"creatorMemberName"`
	CreateSerialNo    string              `json:"createSerialNo,omitempty"`
	StorageType       string              `json:"storageType,omitempty"`
	CatalogName       string              `json:"catalogName,omitempty"`
	DocTemplateStatus string              `json:"docTemplateStatus"`
	DocFields         []commonModel.Field `json:"docFields"`
}
