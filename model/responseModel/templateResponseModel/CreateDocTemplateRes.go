package templateResponseModel

type CreateDocTemplateRes struct {
	RequestId string                   `json:"requestId,omitempty"`
	Code      string                   `json:"code,omitempty"`
	Msg       string                   `json:"msg,omitempty"`
	Data      CreateDocTemplateResData `json:"data,omitempty"`
}

type CreateDocTemplateResData struct {
	DocTemplateId  string `json:"docTemplateId"`
	CreateSerialNo string `json:"createSerialNo"`
}
