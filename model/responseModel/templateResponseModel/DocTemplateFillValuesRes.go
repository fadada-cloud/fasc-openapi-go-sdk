package templateResponseModel

type DocTemplateFillValuesRes struct {
	RequestId string                    `json:"requestId,omitempty"`
	Code      string                    `json:"code,omitempty"`
	Msg       string                    `json:"msg,omitempty"`
	Data      DocTemplateFillValuesData `json:"data,omitempty"`
}

type DocTemplateFillValuesData struct {
	FileId          string `json:"fileId"`
	FileDownloadUrl string `json:"fileDownloadUrl"`
}
