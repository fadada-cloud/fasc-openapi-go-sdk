package templateResponseModel

type SetDocTemplateStatusRes struct {
	RequestId string `json:"requestId,omitempty"`
	Code      string `json:"code,omitempty"`
	Msg       string `json:"msg,omitempty"`
}
