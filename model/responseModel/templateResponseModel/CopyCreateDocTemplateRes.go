package templateResponseModel

type CopyCreateDocTemplateRes struct {
	RequestId string                    `json:"requestId,omitempty"`
	Code      string                    `json:"code,omitempty"`
	Msg       string                    `json:"msg,omitempty"`
	Data      CopyCreateDocTemplateData `json:"data,omitempty"`
}

type CopyCreateDocTemplateData struct {
	DocTemplateId   string `json:"docTemplateId"`
	DocTemplateName string `json:"docTemplateName"`
	CreateSerialNo  string `json:"createSerialNo"`
}
