package templateResponseModel

type GetCorpFieldListRes struct {
	RequestId string                 `json:"requestId,omitempty"`
	Code      string                 `json:"code,omitempty"`
	Msg       string                 `json:"msg,omitempty"`
	Data      []GetCorpFieldListData `json:"data,omitempty"`
}

type GetCorpFieldListData struct {
	FieldName           string                   `json:"fieldName"`
	FieldType           string                   `json:"fieldType"`
	FieldTextSingleLine *CorpFieldTextSingleLine `json:"fieldTextSingleLine"`
	FieldTextMultiLine  *CorpFieldTextMultiLine  `json:"fieldTextMultiLine"`
	FieldMultiCheckbox  *FieldMultiCheckbox      `json:"fieldMultiCheckbox"`
}

type CorpFieldTextSingleLine struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type CorpFieldTextMultiLine struct {
	DefaultValue string `json:"defaultValue"`
	Tips         string `json:"tips"`
	Required     bool   `json:"required"`
	FontType     string `json:"fontType"`
	FontSize     int    `json:"fontSize"`
	Width        int    `json:"width"`
	Height       int    `json:"height"`
	Alignment    string `json:"alignment"`
}

type FieldMultiCheckbox struct {
	Required     bool     `json:"required"`
	Option       []string `json:"option"`
	DefaultValue []bool   `json:"defaultValue"`
}
