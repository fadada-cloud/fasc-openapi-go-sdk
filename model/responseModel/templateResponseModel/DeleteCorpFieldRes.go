package templateResponseModel

type DeleteCorpFieldRes struct {
	RequestId string `json:"requestId,omitempty"`
	Code      string `json:"code,omitempty"`
	Msg       string `json:"msg,omitempty"`
}
