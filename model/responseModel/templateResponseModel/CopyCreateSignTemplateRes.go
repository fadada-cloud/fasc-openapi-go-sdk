package templateResponseModel

type CopyCreateSignTemplateRes struct {
	RequestId string                     `json:"requestId,omitempty"`
	Code      string                     `json:"code,omitempty"`
	Msg       string                     `json:"msg,omitempty"`
	Data      CopyCreateSignTemplateData `json:"data,omitempty"`
}

type CopyCreateSignTemplateData struct {
	SignTemplateId   string `json:"signTemplateId"`
	SignTemplateName string `json:"signTemplateName"`
	CreateSerialNo   string `json:"createSerialNo"`
}
