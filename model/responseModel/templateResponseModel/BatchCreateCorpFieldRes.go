package templateResponseModel

type BatchCreateCorpFieldRes struct {
	RequestId string                   `json:"requestId,omitempty"`
	Code      string                   `json:"code,omitempty"`
	Msg       string                   `json:"msg,omitempty"`
	Data      BatchCreateCorpFieldData `json:"data,omitempty"`
}

type BatchCreateCorpFieldData struct {
	FailField []FailField `json:"failField"`
}

type FailField struct {
	FieldName  string `json:"fieldName"`
	FieldType  string `json:"fieldType"`
	FailReason string `json:"failReason"`
}
