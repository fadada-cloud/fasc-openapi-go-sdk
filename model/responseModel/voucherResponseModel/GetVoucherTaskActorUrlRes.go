package voucherResponseModel

type GetVoucherTaskActorUrlRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      GetVoucherTaskActorUrlData `json:"data"`
}

type GetVoucherTaskActorUrlData struct {
	ActorSignTaskEmbedUrl string `json:"actorSignTaskEmbedUrl"`
}
