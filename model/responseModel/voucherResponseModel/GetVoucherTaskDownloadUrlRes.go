package voucherResponseModel

type GetVoucherTaskDownloadUrlRes struct {
	RequestId string                        `json:"requestId"`
	Code      string                        `json:"code"`
	Msg       string                        `json:"msg"`
	Data      GetVoucherTaskDownloadUrlData `json:"data"`
}

type GetVoucherTaskDownloadUrlData struct {
	DownloadUrl string `json:"downloadUrl"`
	DownloadId  string `json:"downloadId"`
}
