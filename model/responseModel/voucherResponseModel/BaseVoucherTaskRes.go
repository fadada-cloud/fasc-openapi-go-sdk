package voucherResponseModel

type BaseVoucherTaskRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      BaseVoucherTaskData `json:"data"`
}

type BaseVoucherTaskData struct {
	SignTaskId string `json:"signTaskId"`
}
