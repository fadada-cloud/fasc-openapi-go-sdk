package voucherResponseModel

type VoucherTaskListInfoRes struct {
	RequestId string                  `json:"requestId"`
	Code      string                  `json:"code"`
	Msg       string                  `json:"msg"`
	Data      VoucherTaskListInfoData `json:"data"`
}

type VoucherTaskListInfoData struct {
	List          []SignTaskListInfo `json:"list"`
	ListPageNo    int                `json:"listPageNo,string"`
	CountInPage   int                `json:"countInPage,string"`
	ListPageCount int                `json:"listPageCount,string"`
	TotalCount    int                `json:"totalCount,string"`
}

type SignTaskListInfo struct {
	SignTaskId       string `json:"signTaskId"`
	TransReferenceId string `json:"transReferenceId"`
	SignTaskSubject  string `json:"signTaskSubject"`
	SignTaskStatus   string `json:"signTaskStatus"`
	TerminationNote  string `json:"terminationNote"`
	InitiatorName    string `json:"initiatorName"`
	CreateTime       string `json:"createTime"`
	FinishTime       string `json:"finishTime"`
}
