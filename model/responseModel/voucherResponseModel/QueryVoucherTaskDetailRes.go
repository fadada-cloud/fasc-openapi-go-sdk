package voucherResponseModel

type QueryVoucherTaskDetailRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      QueryVoucherTaskDetailData `json:"data"`
}

type QueryVoucherTaskDetailData struct {
	Initiator       *OpenId     `json:"initiator"`
	SignTaskId      string      `json:"signTaskId"`
	SignTaskSubject string      `json:"signTaskSubject"`
	SignTaskStatus  string      `json:"signTaskStatus"`
	TerminationNote string      `json:"terminationNote"`
	CreateTime      string      `json:"createTime"`
	FinishTime      string      `json:"finishTime"`
	Docs            []Doc       `json:"docs"`
	Attachs         []Attach    `json:"attachs"`
	Actors          []Actor     `json:"actors"`
	SignFields      []SignField `json:"signFields"`
}

type OpenId struct {
	IdType string `json:"idType"`
	OpenId string `json:"openId"`
}

type Doc struct {
	DocId   string `json:"docId"`
	DocName string `json:"docName"`
}

type Attach struct {
	AttachId   string `json:"attachId"`
	AttachName string `json:"attachName"`
}

type Actor struct {
	ActorId               string `json:"actorId"`
	ActorName             string `json:"actorName"`
	SignStatus            string `json:"signStatus"`
	ActorNote             string `json:"actorNote"`
	SignTime              string `json:"signTime"`
	ActorSignTaskEmbedUrl string `json:"actorSignTaskEmbedUrl"`
}

type SignField struct {
	FieldDocId string    `json:"fieldDocId"`
	FieldId    string    `json:"fieldId"`
	FieldName  string    `json:"fieldName"`
	Position   *Position `json:"position"`
}

type Position struct {
	PositionPageNo int    `json:"positionPageNo"`
	PositionX      string `json:"positionX"`
	PositionY      string `json:"positionY"`
}
