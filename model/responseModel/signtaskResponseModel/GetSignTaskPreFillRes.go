package signtaskResponseModel

type GetSignTaskPreFillUrlRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      GetSignTaskPreFillUrlData `json:"data"`
}

type GetSignTaskPreFillUrlData struct {
	SignTaskPrefillUrl string `json:"signTaskPrefillUrl,omitempty"`
}
