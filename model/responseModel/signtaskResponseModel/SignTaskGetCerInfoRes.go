package signtaskResponseModel

type SignTaskGetCerInfoRes struct {
	RequestId string                 `json:"requestId"`
	Code      string                 `json:"code"`
	Msg       string                 `json:"msg"`
	Data      SignTaskGetCerInfoData `json:"data"`
}

type SignTaskGetCerInfoData struct {
	Actors []CerInfoActor `json:"actors"`
}

type CerInfoActor struct {
	ActorId     string   `json:"actorId"`
	ActorType   string   `json:"actorType"`
	ActorName   string   `json:"actorName"`
	IdentName   string   `json:"identName"`
	Permissions []string `json:"permissions"`
	CerFile     []string `json:"cerFile"`
}
