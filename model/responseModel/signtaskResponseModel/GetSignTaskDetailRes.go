package signtaskResponseModel

import (
	"gitee.com/fadada-cloud/fasc-openapi-go-sdk/model/commonModel"
)

type GetSignTaskDetailRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      SignTaskDetailResData `json:"data"`
}

type SignTaskDetailResData struct {
	Initiator           *commonModel.OpenId     `json:"initiator"`
	InitiatorMemberId   string                  `json:"initiatorMemberId,omitempty"`
	InitiatorMemberName string                  `json:"initiatorMemberName,omitempty"`
	InitiatorEntityId   string                  `json:"initiatorEntityId"`
	SignTaskId          string                  `json:"signTaskId"`
	SignTaskSubject     string                  `json:"signTaskSubject"`
	StorageType         string                  `json:"storageType,omitempty"`
	AutoFinish          bool                    `json:"autoFinish"`
	SignDocType         string                  `json:"signDocType,omitempty"`
	SignTaskSource      string                  `json:"signTaskSource,omitempty"`
	CertCAOrg           string                  `json:"certCAOrg,omitempty"`
	AutoFillFinalize    bool                    `json:"autoFillFinalize"`
	SignInOrder         bool                    `json:"signInOrder"`
	ApprovalStatus      string                  `json:"approvalStatus,omitempty"`
	RejectNote          string                  `json:"rejectNote,omitempty"`
	BusinessTypeId      int64                   `json:"businessTypeId,omitempty"`
	BusinessTypeName    string                  `json:"businessTypeName,omitempty"`
	FileFormat          string                  `json:"fileFormat,omitempty"`
	TransReferenceId    string                  `json:"transReferenceId,omitempty"`
	BusinessCode        string                  `json:"businessCode,omitempty"`
	TemplateId          string                  `json:"templateId,omitempty"`
	SignTaskStatus      string                  `json:"signTaskStatus"`
	AbolishedSignTaskId string                  `json:"abolishedSignTaskId,omitempty"`
	OriginalSignTaskId  string                  `json:"originalSignTaskId,omitempty"`
	RevokeReason        string                  `json:"revokeReason,omitempty"`
	RevokeNote          string                  `json:"revokeNote"`
	TerminationNote     string                  `json:"terminationNote,omitempty"`
	CreateTime          string                  `json:"createTime,omitempty"`
	StartTime           string                  `json:"startTime,omitempty"`
	FinishTime          string                  `json:"finishTime,omitempty"`
	DeadlineTime        string                  `json:"deadlineTime,omitempty"`
	DueDate             string                  `json:"dueDate"`
	Docs                []Doc                   `json:"docs"`
	Attachs             []Attach                `json:"attachs"`
	Actors              []SignTaskDetailActor   `json:"actors"`
	Watermarks          []commonModel.Watermark `json:"watermarks,omitempty"`
	ApprovalInfos       []ApprovalInfo          `json:"approvalInfos,omitempty"`
}

type Doc struct {
	DocId     string `json:"docId,omitempty"`
	DocName   string `json:"docName,omitempty"`
	DocFileId int64  `json:"docFileId"`
}

type Attach struct {
	AttachId   string `json:"attachId,omitempty"`
	AttachName string `json:"attachName,omitempty"`
}

type SignTaskDetailActor struct {
	ActorInfo             Actor       `json:"actorInfo,omitempty"`
	SignFields            []SignField `json:"signFields,omitempty"`
	ReadStatus            string      `json:"readStatus,omitempty"`
	ReadTime              string      `json:"readTime,omitempty"`
	JoinStatus            string      `json:"joinStatus,omitempty"`
	JoinTime              string      `json:"joinTime,omitempty"`
	FillStatus            string      `json:"fillStatus,omitempty"`
	FillTime              string      `json:"fillTime,omitempty"`
	SignStatus            string      `json:"signStatus,omitempty"`
	ActorNote             string      `json:"actorNote,omitempty"`
	SignTime              string      `json:"signTime,omitempty"`
	SignOrderNo           int         `json:"signOrderNo,omitempty"`
	BlockStatus           string      `json:"blockStatus,omitempty"`
	ActorSignTaskUrl      string      `json:"actorSignTaskUrl,omitempty"`
	ActorSignTaskEmbedUrl string      `json:"actorSignTaskEmbedUrl,omitempty"`
}

type Actor struct {
	ActorType   string   `json:"actorType,omitempty"`
	ActorId     string   `json:"actorId,omitempty"`
	ActorOpenId string   `json:"actorOpenId,omitempty"`
	ActorName   string   `json:"actorName,omitempty"`
	Permissions []string `json:"permissions,omitempty"`
	IsInitiator bool     `json:"isInitiator"`
}

type SignField struct {
	SignFieldStatus string                     `json:"signFieldStatus,omitempty"`
	FieldDocId      string                     `json:"fieldDocId,omitempty"`
	FieldId         string                     `json:"fieldId,omitempty"`
	FieldName       string                     `json:"fieldName,omitempty"`
	SealId          *int64                     `json:"sealId,omitempty"`
	CategoryType    string                     `json:"categoryType"`
	Moveable        bool                       `json:"moveable"`
	SignRemark      string                     `json:"signRemark"`
	Position        *commonModel.FieldPosition `json:"position,omitempty"`
}

type ApprovalInfo struct {
	ApprovalFlowId string `json:"approvalFlowId"`
	ApprovalId     string `json:"approvalId"`
	ApprovalType   string `json:"approvalType"`
	ApprovalStatus string `json:"approvalStatus"`
	RejectNote     string `json:"rejectNote"`
}
