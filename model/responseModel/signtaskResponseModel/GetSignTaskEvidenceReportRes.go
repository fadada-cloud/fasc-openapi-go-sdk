package signtaskResponseModel

type GetSignTaskEvidenceReportRes struct {
	RequestId string                        `json:"requestId"`
	Code      string                        `json:"code"`
	Msg       string                        `json:"msg"`
	Data      GetSignTaskEvidenceReportData `json:"data"`
}

type GetSignTaskEvidenceReportData struct {
	ReportDownLoadId string `json:"reportDownLoadId"`
	ReportStatus     string `json:"reportStatus"`
}
