package signtaskResponseModel

type ReportDownloadRes struct {
	RequestId   string
	Content     []byte
	ContentType string
	Data        ReportDownloadData `json:"data"`
}

type ReportDownloadData struct {
	ReportDownloadUrl string `json:"reportDownloadUrl"`
}
