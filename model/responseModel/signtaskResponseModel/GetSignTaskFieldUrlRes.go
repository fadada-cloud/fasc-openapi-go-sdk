package signtaskResponseModel

type SignTaskFieldUrlData struct {
	DownloadUrl string `json:"downloadUrl"`
	DownloadId  string `json:"downloadId"`
}

type GetSignTaskFieldUrlRes struct {
	RequestId string               `json:"requestId"`
	Code      string               `json:"code"`
	Msg       string               `json:"msg"`
	Data      SignTaskFieldUrlData `json:"data"`
}
