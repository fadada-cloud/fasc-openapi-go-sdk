package signtaskResponseModel

type GetV3ActorSignTaskUrlRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      GetV3ActorSignTaskUrlData `json:"data"`
}

type GetV3ActorSignTaskUrlData struct {
	SignUrls          []V3SignUrlRes          `json:"signUrls"`
	MiniProgramConfig *V3MiniProgramConfigRes `json:"miniProgramConfig"`
	SignDetails       []V3SignUrlDetail       `json:"signDetails"`
}

type V3SignUrlRes struct {
	SignUrl string `json:"signUrl"`
}

type V3MiniProgramConfigRes struct {
	AppId string `json:"appId"`
	Path  string `json:"path"`
}

type V3SignatoryRes struct {
	SignerId string `json:"signerId"`
}

type V3CorpRes struct {
	CorpId string `json:"corpId"`
}

type V3SignerRes struct {
	Signatory V3SignatoryRes `json:"signatory"`
	Corp      V3CorpRes      `json:"corp"`
}

type V3SignUrlDetail struct {
	Signer     V3SignerRes `json:"signer"`
	SignUrl    string      `json:"signUrl"`
	SignOrder  int         `json:"signOrder,omitempty"`
	SignStatus int         `json:"signStatus,omitempty"`
}
