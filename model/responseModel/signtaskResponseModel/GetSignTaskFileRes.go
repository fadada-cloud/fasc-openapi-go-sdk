package signtaskResponseModel

type GetSignTaskFileRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetSignTaskFileData `json:"data"`
}

type GetSignTaskFileData struct {
	Docs []SignTaskFile `json:"docs"`
}

type SignTaskFile struct {
	DocId      string `json:"docId"`
	DocName    string `json:"docName"`
	FileId     string `json:"fileId"`
	FddFileUrl string `json:"fddFileUrl"`
}
