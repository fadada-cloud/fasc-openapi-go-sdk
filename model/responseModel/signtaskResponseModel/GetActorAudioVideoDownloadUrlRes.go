package signtaskResponseModel

type GetActorAudioVideoDownloadUrlRes struct {
	RequestId string                               `json:"requestId"`
	Code      string                               `json:"code"`
	Msg       string                               `json:"msg"`
	Data      GetActorAudioVideoDownloadUrlResData `json:"data"`
}

type GetActorAudioVideoDownloadUrlResData struct {
	DownloadUrl string `json:"downloadUrl,omitempty"`
}
