package signtaskResponseModel

type GetSignTaskMessageReportDownloadUrlRes struct {
	RequestId string                                  `json:"requestId"`
	Code      string                                  `json:"code"`
	Msg       string                                  `json:"msg"`
	Data      GetSignTaskMessageReportDownloadUrlData `json:"data"`
}

type GetSignTaskMessageReportDownloadUrlData struct {
	ReportUrl string `json:"reportUrl,omitempty"`
}
