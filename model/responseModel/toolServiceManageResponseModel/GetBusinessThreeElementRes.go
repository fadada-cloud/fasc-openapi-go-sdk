package toolServiceManageResponseModel

type GetBusinessThreeElementRes struct {
	RequestId string                      `json:"requestId"`
	Code      string                      `json:"code"`
	Msg       string                      `json:"msg"`
	Data      GetBusinessThreeElementData `json:"data"`
}

type GetBusinessThreeElementData struct {
	SerialNo     string `json:"serialNo"`
	VerifyResult string `json:"verifyResult"`
}
