package toolServiceManageResponseModel

type TelecomThreeElementVerifyRes struct {
	RequestId string                        `json:"requestId"`
	Code      string                        `json:"code"`
	Msg       string                        `json:"msg"`
	Data      TelecomThreeElementVerifyData `json:"data"`
}

type TelecomThreeElementVerifyData struct {
	VerifyResult bool   `json:"verifyResult"`
	SerialNo     string `json:"serialNo"`
}
