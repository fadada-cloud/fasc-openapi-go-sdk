package toolServiceManageResponseModel

type GetTransactionIdRes struct {
	RequestId string               `json:"requestId"`
	Code      string               `json:"code"`
	Msg       string               `json:"msg"`
	Data      GetTransactionIdData `json:"data"`
}

type GetTransactionIdData struct {
	TransactionId string `json:"transactionId"`
}
