package toolServiceManageResponseModel

type GetIdCardThreeElementVerifyRes struct {
	RequestId string                          `json:"requestId"`
	Code      string                          `json:"code"`
	Msg       string                          `json:"msg"`
	Data      GetIdCardThreeElementVerifyData `json:"data"`
}

type GetIdCardThreeElementVerifyData struct {
	SerialNo   string `json:"serialNo"`
	Similarity string `json:"similarity"`
}
