package toolServiceManageResponseModel

type GetBusinessInfoRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetBusinessInfoData `json:"data"`
}

type GetBusinessInfoData struct {
	SerialNo       string `json:"serialNo"`
	OrgName        string `json:"orgName"`
	OrganizationNo string `json:"organizationNo"`
	CompanyCode    string `json:"companyCode"`
	LegalRepName   string `json:"legalRepName"`
	OrgType        string `json:"orgType"`
	OrgStatus      string `json:"orgStatus"`
	Authority      string `json:"authority"`
	Capital        string `json:"capital"`
	Address        string `json:"address"`
	BusinessScope  string `json:"businessScope"`
	EstablishDate  string `json:"establishDate"`
	StartDate      string `json:"startDate"`
	EndDate        string `json:"endDate"`
	IssueDate      string `json:"issueDate"`
}
