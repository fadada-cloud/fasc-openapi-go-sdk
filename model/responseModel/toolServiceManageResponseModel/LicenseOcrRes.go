package toolServiceManageResponseModel

type LicenseOcrRes struct {
	RequestId string         `json:"requestId"`
	Code      string         `json:"code"`
	Msg       string         `json:"msg"`
	Data      LicenseOcrData `json:"data"`
}

type LicenseOcrData struct {
	SerialNo        string   `json:"serialNo"`
	CreditNo        string   `json:"creditNo"`
	CompanyName     string   `json:"companyName"`
	CompanyType     string   `json:"companyType"`
	Address         string   `json:"address"`
	RegisterNo      string   `json:"registerNo"`
	ArtificialName  string   `json:"artificialName"`
	RegCapital      string   `json:"regCapital"`
	PaidinCapital   string   `json:"paidinCapital"`
	StartTime       string   `json:"startTime"`
	OperatingPeriod string   `json:"operatingPeriod"`
	Scope           string   `json:"Scope"`
	WarningMsg      []string `json:"warningMsg"`
}
