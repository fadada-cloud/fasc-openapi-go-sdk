package toolServiceManageResponseModel

type GetBusinessFourElementRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      GetBusinessFourElementData `json:"data"`
}

type GetBusinessFourElementData struct {
	SerialNo     string `json:"serialNo"`
	VerifyResult string `json:"verifyResult"`
}
