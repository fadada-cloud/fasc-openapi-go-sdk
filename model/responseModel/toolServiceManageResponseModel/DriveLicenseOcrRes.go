package toolServiceManageResponseModel

type DriveLicenseOcrRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      DriveLicenseOcrData `json:"data"`
}

type DriveLicenseOcrData struct {
	SerialNo         string   `json:"serialNo"`
	Name             string   `json:"name"`
	Nationality      string   `json:"nationality"`
	Sex              string   `json:"sex"`
	Address          string   `json:"address"`
	Birthday         string   `json:"birthday"`
	FirstIssueDate   string   `json:"firstIssueDate"`
	Number           string   `json:"number"`
	QuasiDrivingType string   `json:"quasiDrivingType"`
	StartDate        string   `json:"startDate"`
	EndDate          string   `json:"endDate"`
	ArchivesCode     string   `json:"archivesCode"`
	Record           string   `json:"record"`
	WarningMsg       []string `json:"warningMsg"`
}
