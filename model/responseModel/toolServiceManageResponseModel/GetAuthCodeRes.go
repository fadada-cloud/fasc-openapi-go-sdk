package toolServiceManageResponseModel

type GetAuthCodeRes struct {
	RequestId string          `json:"requestId"`
	Code      string          `json:"code"`
	Msg       string          `json:"msg"`
	Data      GetAuthCodeData `json:"data"`
}

type GetAuthCodeData struct {
	SendFlag bool `json:"sendFlag"`
}
