package toolServiceManageResponseModel

type GetVerifyDetailRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetVerifyDetailData `json:"data"`
}

type GetVerifyDetailData struct {
	TransactionId string      `json:"transactionId"`
	Status        string      `json:"status"`
	ObjectType    string      `json:"objectType"`
	StartTime     string      `json:"startTime"`
	UpdateTime    string      `json:"updateTime"`
	FailReason    string      `json:"failReason"`
	AuthType      string      `json:"authType"`
	PersonInfo    *PersonInfo `json:"personInfo"`
}

type PersonInfo struct {
	Name          string `json:"name"`
	CertNo        string `json:"certNo"`
	BankAccountNo string `json:"bankAccountNo"`
	Mobile        string `json:"mobile"`
}
