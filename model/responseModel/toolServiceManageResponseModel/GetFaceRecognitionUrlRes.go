package toolServiceManageResponseModel

type GetFaceRecognitionUrlRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      GetFaceRecognitionUrlData `json:"data"`
}

type GetFaceRecognitionUrlData struct {
	SerialNo string `json:"serialNo"`
	ShortUrl string `json:"shortUrl"`
	Url      string `json:"url"`
}
