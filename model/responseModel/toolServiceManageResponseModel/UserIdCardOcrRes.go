package toolServiceManageResponseModel

type UserIdCardOcrRes struct {
	RequestId string               `json:"requestId"`
	Code      string               `json:"code"`
	Msg       string               `json:"msg"`
	Data      UserIdCardOcrResData `json:"data"`
}

type UserIdCardOcrResData struct {
	Name           string `json:"name,omitempty"`
	IdentNo        string `json:"identNo,omitempty"`
	Gender         string `json:"gender,omitempty"`
	Birthday       string `json:"birthday,omitempty"`
	Nation         string `json:"nation,omitempty"`
	Address        string `json:"address,omitempty"`
	IssueAuthority string `json:"issueAuthority,omitempty"`
	ValidPeriod    string `json:"validPeriod,omitempty"`
	SerialNo       string `json:"serialNo"`
}
