package toolServiceManageResponseModel

type BankThreeElementVerifyRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      BankThreeElementVerifyData `json:"data"`
}

type BankThreeElementVerifyData struct {
	VerifyResult bool   `json:"verifyResult"`
	SerialNo     string `json:"serialNo"`
}
