package toolServiceManageResponseModel

type MainlandPermitOcrRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      MainlandPermitOcrData `json:"data"`
}

type MainlandPermitOcrData struct {
	SerialNo       string   `json:"serialNo"`
	Name           string   `json:"name"`
	EnglishName    string   `json:"englishName"`
	Sex            string   `json:"sex"`
	Birthday       string   `json:"birthday"`
	IssueAuthority string   `json:"issueAuthority"`
	ValidDate      string   `json:"validDate"`
	Number         string   `json:"number"`
	IssueAddress   string   `json:"issueAddress"`
	IssueNumber    string   `json:"issueNumber"`
	Type           string   `json:"type"`
	WarningMsg     []string `json:"warningMsg"`
}
