package toolServiceManageResponseModel

type BankFourElementVerifyRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      BankFourElementVerifyData `json:"data"`
}

type BankFourElementVerifyData struct {
	VerifyResult bool   `json:"verifyResult"`
	SerialNo     string `json:"serialNo"`
}
