package toolServiceManageResponseModel

type GetFaceRecognitionStatusRes struct {
	RequestId string                       `json:"requestId"`
	Code      string                       `json:"code"`
	Msg       string                       `json:"msg"`
	Data      GetFaceRecognitionStatusData `json:"data"`
}

type GetFaceRecognitionStatusData struct {
	UserName     string   `json:"userName"`
	UserIdentNo  string   `json:"userIdentNo"`
	FaceAuthMode string   `json:"faceAuthMode"`
	ResultStatus int      `json:"resultStatus"`
	ResultTime   string   `json:"resultTime"`
	FailedReason string   `json:"failedReason"`
	UrlStatus    int      `json:"urlStatus"`
	Similarity   string   `json:"similarity"`
	LiveRate     string   `json:"liveRate"`
	FaceImg      string   `json:"faceImg"`
	FaceImgs     []string `json:"faceImgs"`
}
