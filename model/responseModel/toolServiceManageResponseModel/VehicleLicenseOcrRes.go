package toolServiceManageResponseModel

type VehicleLicenseOcrRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      VehicleLicenseOcrData `json:"data"`
}

type VehicleLicenseOcrData struct {
	SerialNo         string `json:"serialNo"`
	VehicleFrontInfo struct {
		PlateNo          string `json:"plateNo"`
		VehicleType      string `json:"vehicleType"`
		Owner            string `json:"owner"`
		Address          string `json:"address"`
		UseCharacter     string `json:"useCharacter"`
		Model            string `json:"model"`
		Vin              string `json:"vin"`
		EngineNo         string `json:"engineNo"`
		RegisterDate     string `json:"registerDate"`
		IssueDate        string `json:"issueDate"`
		IssuingAuthority string `json:"issuingAuthority"`
	} `json:"vehicleFrontInfo"`
	VehicleBackInfo struct {
		PlateNo        string `json:"plateNo"`
		FileNo         string `json:"fileNo"`
		AllowNum       string `json:"allowNum"`
		TotalMass      string `json:"totalMass"`
		CurbWeight     string `json:"curbWeight"`
		LoadQuality    string `json:"loadQuality"`
		ExternalSize   string `json:"externalSize"`
		TotalQuasiMass string `json:"totalQuasiMass"`
		Marks          string `json:"marks"`
		Record         string `json:"record"`
	} `json:"vehicleBackInfo"`
	WarningMsg []string `json:"warningMsg"`
}
