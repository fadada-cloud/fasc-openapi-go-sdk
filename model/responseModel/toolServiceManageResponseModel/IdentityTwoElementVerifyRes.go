package toolServiceManageResponseModel

type IdentityTwoElementVerifyRes struct {
	RequestId string                       `json:"requestId"`
	Code      string                       `json:"code"`
	Msg       string                       `json:"msg"`
	Data      IdentityTwoElementVerifyData `json:"data"`
}

type IdentityTwoElementVerifyData struct {
	VerifyResult bool   `json:"verifyResult"`
	SerialNo     string `json:"serialNo"`
}
