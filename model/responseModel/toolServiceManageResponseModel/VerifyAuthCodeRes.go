package toolServiceManageResponseModel

type VerifyAuthCodeRes struct {
	RequestId string             `json:"requestId"`
	Code      string             `json:"code"`
	Msg       string             `json:"msg"`
	Data      VerifyAuthCodeData `json:"data"`
}

type VerifyAuthCodeData struct {
	VerifyFlag bool `json:"verifyFlag"`
}
