package toolServiceManageResponseModel

type BankCardOcrRes struct {
	RequestId string          `json:"requestId"`
	Code      string          `json:"code"`
	Msg       string          `json:"msg"`
	Data      BankCardOcrData `json:"data"`
}

type BankCardOcrData struct {
	SerialNo     string   `json:"serialNo"`
	BankName     string   `json:"bankName"`
	BankCardType string   `json:"bankCardType"`
	BankCardNo   string   `json:"bankCardNo"`
	ValidDate    string   `json:"validDate"`
	WarningMsg   []string `json:"warningMsg"`
}
