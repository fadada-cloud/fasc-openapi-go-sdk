package corpManageResponseModel

type GetChangeCorpIdentityInfoUrlRes struct {
	RequestId string                           `json:"requestId"`
	Code      string                           `json:"code"`
	Msg       string                           `json:"msg"`
	Data      GetChangeCorpIdentityInfoUrlData `json:"data"`
}

type GetChangeCorpIdentityInfoUrlData struct {
	ChangeIdentityInfoUrl string `json:"changeIdentityInfoUrl"`
}
