package sealResponseModel

type CreateLegalRepresentativeSealByTemplateRes struct {
	RequestId string                                      `json:"requestId"`
	Code      string                                      `json:"code"`
	Msg       string                                      `json:"msg"`
	Data      CreateLegalRepresentativeSealByTemplateData `json:"data"`
}

type CreateLegalRepresentativeSealByTemplateData struct {
	SealId *int64 `json:"sealId,string"`
}
