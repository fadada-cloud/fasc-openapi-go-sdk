package sealResponseModel

type CreateSealByTemplateRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      CreateSealByTemplateData `json:"data"`
}

type CreateSealByTemplateData struct {
	SealId int64 `json:"sealId,string"`
}
