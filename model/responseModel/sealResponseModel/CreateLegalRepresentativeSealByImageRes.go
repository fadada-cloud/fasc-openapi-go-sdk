package sealResponseModel

type CreateLegalRepresentativeSealByImageRes struct {
	RequestId string                                   `json:"requestId"`
	Code      string                                   `json:"code"`
	Msg       string                                   `json:"msg"`
	Data      CreateLegalRepresentativeSealByImageData `json:"data"`
}

type CreateLegalRepresentativeSealByImageData struct {
	VerifyId int64 `json:"verifyId,string"`
}
