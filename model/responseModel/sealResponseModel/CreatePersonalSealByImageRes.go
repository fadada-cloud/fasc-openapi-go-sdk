package sealResponseModel

type CreatePersonalSealByImageRes struct {
	RequestId string                        `json:"requestId"`
	Code      string                        `json:"code"`
	Msg       string                        `json:"msg"`
	Data      CreatePersonalSealByImageData `json:"data"`
}

type CreatePersonalSealByImageData struct {
	SealId int64 `json:"sealId,string"`
}
