package sealResponseModel

type CreateSealByImageRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      CreateSealByImageData `json:"data"`
}
type CreateSealByImageData struct {
	VerifyId *int64 `json:"verifyId,string"`
}
