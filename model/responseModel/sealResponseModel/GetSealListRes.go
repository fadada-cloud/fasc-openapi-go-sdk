package sealResponseModel

// GetSealListRes 获取印章列表
type GetSealListRes struct {
	RequestId string          `json:"requestId"`
	Code      string          `json:"code"`
	Msg       string          `json:"msg"`
	Data      SealListResData `json:"data"`
}

type SealListResData struct {
	SealInfos    []SealInfo        `json:"sealInfos,omitempty"`
	FreeSignInfo []FreeSignInfoRes `json:"freeSignInfo,omitempty"`
}

type SealInfo struct {
	SealId          string     `json:"sealId"`
	EntityId        string     `json:"entityId"`
	SealName        string     `json:"sealName"`
	SealStatus      string     `json:"sealStatus"`
	CategoryType    string     `json:"categoryType"`
	PicFileUrl      string     `json:"picFileUrl"`
	SealWidth       int        `json:"sealWidth,omitempty"`
	SealHeight      int        `json:"sealHeight,omitempty"`
	CreateTime      string     `json:"createTime"`
	CertCANo        string     `json:"certCANo"`
	CertCAOrg       string     `json:"certCAOrg"`
	CertEncryptType string     `json:"certEncryptType"`
	SealUsers       []SealUser `json:"sealUsers"`
	VerifyId        int64      `json:"verifyId"`
}

type SealUser struct {
	MemberId           string `json:"memberId"`
	MemberName         string `json:"memberName"`
	InternalIdentifier string `json:"internalIdentifier"`
	MemberEmail        string `json:"memberEmail"`
	MemberMobile       string `json:"memberMobile,omitempty"`
	GrantTime          string `json:"grantTime,omitempty"`
	GrantStartTime     string `json:"grantStartTime,omitempty"`
	GrantEndTime       string `json:"grantEndTime,omitempty"`
	GrantStatus        string `json:"grantStatus,omitempty"`
}

type FreeSignInfoRes struct {
	BusinessId       string `json:"businessId,omitempty"`
	BusinessScene    string `json:"businessScene,omitempty"`
	BusinessSceneExp string `json:"businessSceneExp,omitempty"`
	ExpiresTime      string `json:"expiresTime,omitempty"`
}
