package sealResponseModel

type CreatePersonalSealByTemplateRes struct {
	RequestId string                           `json:"requestId"`
	Code      string                           `json:"code"`
	Msg       string                           `json:"msg"`
	Data      CreatePersonalSealByTemplateData `json:"data"`
}

type CreatePersonalSealByTemplateData struct {
	SealId *int64 `json:"sealId,string"`
}
