package sealResponseModel

type GetPersonalSealFreeSignRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      GetPersonSealFreeSignUrl `json:"data"`
}

type GetPersonSealFreeSignUrl struct {
	FreeSignUrl      string `json:"freeSignUrl,omitempty"`
	FreeSignShortUrl string `json:"freeSignShortUrl,omitempty"`
}
