package sealResponseModel

type CancelPersonSealFreeSignRes struct {
	RequestId string `json:"requestId,omitempty"`
	Code      string `json:"code,omitempty"`
	Msg       string `json:"msg,omitempty"`
}
