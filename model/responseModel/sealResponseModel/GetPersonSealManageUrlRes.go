package sealResponseModel

type GetPersonSealManageUrlRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      GetPersonSealManageUrlData `json:"data"`
}

type GetPersonSealManageUrlData struct {
	ResourceUrl string `json:"resourceUrl,omitempty"`
}
