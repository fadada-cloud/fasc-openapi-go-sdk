package sealResponseModel

type GetSealTagListRes struct {
	RequestId string   `json:"requestId"`
	Code      string   `json:"code"`
	Msg       string   `json:"msg"`
	Data      []string `json:"data"`
}
