package sealResponseModel

type GetPersonSealCreateUrlRes struct {
	RequestId string                     `json:"requestId"`
	Code      string                     `json:"code"`
	Msg       string                     `json:"msg"`
	Data      GetPersonSealCreateUrlData `json:"data"`
}

type GetPersonSealCreateUrlData struct {
	SealCreateUrl string `json:"sealCreateUrl,omitempty"`
}
