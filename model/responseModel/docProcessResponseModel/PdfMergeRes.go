package docProcessResponseModel

type PdfMergeRes struct {
	RequestId string       `json:"requestId"`
	Code      string       `json:"code"`
	Msg       string       `json:"msg"`
	Data      PdfMergeData `json:"data"`
}

type PdfMergeData struct {
	FileId         string `json:"fileId"`
	FileType       string `json:"fileType"`
	FddFileUrl     string `json:"fddFileUrl"`
	FileName       string `json:"fileName"`
	FileTotalPages int    `json:"fileTotalPages"`
}
