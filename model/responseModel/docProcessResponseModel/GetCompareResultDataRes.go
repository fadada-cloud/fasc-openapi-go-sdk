package docProcessResponseModel

type GetCompareResultDataRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      GetCompareResultDataData `json:"data"`
}

type GetCompareResultDataData struct {
	CompareData interface{} `json:"compareData,omitempty"`
}
