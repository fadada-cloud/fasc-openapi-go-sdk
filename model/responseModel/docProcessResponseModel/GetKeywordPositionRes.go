package docProcessResponseModel

type GetKeywordPositionRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      []GetKeywordPositionData `json:"data"`
}

type GetKeywordPositionData struct {
	Keyword   string     `json:"keyword"`
	Positions []Position `json:"positions"`
}

type Position struct {
	PositionPageNo int         `json:"positionPageNo"`
	Coordinate     *Coordinate `json:"coordinate"`
}

type Coordinate struct {
	PositionX string `json:"positionX"`
	PositionY string `json:"positionY"`
}
