package docProcessResponseModel

type OfdMergeRes struct {
	RequestId string       `json:"requestId"`
	Code      string       `json:"code"`
	Msg       string       `json:"msg"`
	Data      OfdMergeData `json:"data"`
}

type OfdMergeData struct {
	FileId          string `json:"fileId"`
	FileDownloadUrl string `json:"fileDownloadUrl"`
}
