package drafResponseModel

type GetDraftManageUrlRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      GetDraftManageUrlData `json:"data"`
}

type GetDraftManageUrlData struct {
	DraftManageUrl string `json:"draftManageUrl"`
}
