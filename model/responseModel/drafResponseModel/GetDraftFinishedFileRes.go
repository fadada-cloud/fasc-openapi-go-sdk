package drafResponseModel

type GetDraftFinishedFileRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      GetDraftFinishedFileData `json:"data"`
}

type GetDraftFinishedFileData struct {
	FileId          string `json:"fileId"`
	ContractSubject string `json:"contractSubject"`
	FileUrl         string `json:"fileUrl"`
}
