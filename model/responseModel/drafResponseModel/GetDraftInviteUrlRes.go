package drafResponseModel

type GetDraftInviteUrlRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      GetDraftInviteUrlData `json:"data"`
}

type GetDraftInviteUrlData struct {
	ContractName   string `json:"contractName"`
	CreatedByOwner string `json:"createdByOwner"`
	CreatedByName  string `json:"createdByName"`
	InviteLinkUrl  string `json:"inviteLinkUrl"`
}
