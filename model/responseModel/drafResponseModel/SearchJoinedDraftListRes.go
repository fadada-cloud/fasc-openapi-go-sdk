package drafResponseModel

type SearchJoinedDraftListRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      SearchJoinedDraftListData `json:"data"`
}

type SearchJoinedDraftListData struct {
	ContractConsults []JoinedContractConsultInfo `json:"contractConsults"`
	ListPageNo       int                         `json:"listPageNo"`
	CountInPage      int                         `json:"countInPage"`
	ListPageCount    int                         `json:"listPageCount"`
	TotalCount       int                         `json:"totalCount"`
}

type JoinedContractConsultInfo struct {
	ContractConsultId string `json:"contractConsultId"`
	ContractSubject   string `json:"contractSubject"`
	ContractStatus    string `json:"contractStatus"`
	CooperationStatus string `json:"cooperationStatus"`
	Permission        string `json:"permission"`
	JoinedOpenCorpId  string `json:"joinedOpenCorpId"`
	JoinedMemberId    string `json:"joinedMemberId"`
	CreateTime        string `json:"createTime"`
	UpdateTime        string `json:"updateTime"`
}
