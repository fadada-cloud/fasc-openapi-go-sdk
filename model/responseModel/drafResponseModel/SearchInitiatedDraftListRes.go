package drafResponseModel

type SearchInitiatedDraftListRes struct {
	RequestId string                       `json:"requestId"`
	Code      string                       `json:"code"`
	Msg       string                       `json:"msg"`
	Data      SearchInitiatedDraftListData `json:"data"`
}

type SearchInitiatedDraftListData struct {
	ContractConsults []InitiatedContractConsultInfo `json:"contractConsults"`
	ListPageNo       int                            `json:"listPageNo"`
	CountInPage      int                            `json:"countInPage"`
	ListPageCount    int                            `json:"listPageCount"`
	TotalCount       int                            `json:"totalCount"`
}

type InitiatedContractConsultInfo struct {
	ContractConsultId   string                 `json:"contractConsultId"`
	ContractSubject     string                 `json:"contractSubject"`
	ContractStatus      string                 `json:"contractStatus"`
	CooperationUserNum  int                    `json:"cooperationUserNum"`
	SignTaskId          string                 `json:"signTaskId"`
	InitiatorMemberId   string                 `json:"initiatorMemberId"`
	InitiateTime        string                 `json:"initiateTime"`
	CooperationUserList []DraftCooperationUser `json:"cooperationUserList"`
}

type DraftCooperationUser struct {
	ContractConsultComment string   `json:"contractConsultComment"`
	CooperationEntityType  string   `json:"cooperationEntityType"`
	OpenCorpId             string   `json:"openCorpId"`
	CorpName               string   `json:"corpName"`
	DeptName               []string `json:"deptName"`
	MemberId               string   `json:"memberId"`
	MemberName             string   `json:"memberName"`
	OpenUserId             string   `json:"openUserId"`
	UserName               string   `json:"userName"`
	CooperationUserSource  string   `json:"cooperationUserSource"`
	Permission             string   `json:"permission"`
	Status                 string   `json:"status"`
	UpdateTime             string   `json:"updateTime"`
}
