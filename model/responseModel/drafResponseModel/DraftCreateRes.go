package drafResponseModel

type DraftCreateRes struct {
	RequestId string          `json:"requestId"`
	Code      string          `json:"code"`
	Msg       string          `json:"msg"`
	Data      DraftCreateData `json:"data"`
}

type DraftCreateData struct {
	ContractConsultId string `json:"contractConsultId"`
}
