package drafResponseModel

type GetDraftEditUrlRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetDraftEditUrlData `json:"data"`
}

type GetDraftEditUrlData struct {
	ContractConsultUrl string `json:"contractConsultUrl"`
}
