package approvalResponseModel

type GetApprovalUrlRes struct {
	RequestId string             `json:"requestId"`
	Code      string             `json:"code"`
	Msg       string             `json:"msg"`
	Data      GetApprovalUrlData `json:"data"`
}

type GetApprovalUrlData struct {
	ApprovalUrl string `json:"approvalUrl,omitempty"`
}
