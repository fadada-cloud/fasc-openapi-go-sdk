package approvalResponseModel

type GetApprovalListRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetApprovalListData `json:"data"`
}

type GetApprovalListData struct {
	ListPageNo    int            `json:"listPageNo,omitempty"`
	CountInPage   int            `json:"countInPage,omitempty"`
	ListPageCount int            `json:"listPageCount,omitempty"`
	TotalCount    int            `json:"totalCount,omitempty"`
	ApprovalInfos []ApprovalInfo `json:"approvalInfos,omitempty"`
}

type ApprovalInfo struct {
	ApprovalType   string   `json:"approvalType,omitempty"`
	TemplateId     string   `json:"templateId,omitempty"`
	SignTaskIds    []string `json:"signTaskIds,omitempty"`
	ApprovalId     string   `json:"approvalId,omitempty"`
	ApprovalName   string   `json:"approvalName,omitempty"`
	ApprovalStatus string   `json:"approvalStatus,omitempty"`
}
