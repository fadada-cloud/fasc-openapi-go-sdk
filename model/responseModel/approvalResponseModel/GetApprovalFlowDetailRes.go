package approvalResponseModel

type GetApprovalFlowDetailRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      GetApprovalFlowDetailData `json:"data"`
}

type GetApprovalFlowDetailData struct {
	ApprovalType      string `json:"approvalType"`
	ApprovalFlowId    string `json:"approvalFlowId"`
	ApprovalFlowName  string `json:"approvalFlowName"`
	Description       string `json:"description"`
	LastOprMemberId   string `json:"lastOprMemberId"`
	LastOprMemberName string `json:"lastOprMemberName"`
	UpdateTime        string `json:"updateTime"`
	Status            string `json:"status"`
	ApprovalNode      []struct {
		SortNum          int    `json:"sortNum"`
		ApprovalNodeType string `json:"approvalNodeType"`
		ApproversInfos   []struct {
			ApproverMemberName string `json:"approverMemberName"`
			ApproverMemberId   string `json:"approverMemberId"`
		} `json:"approversInfos"`
	} `json:"approvalNode"`
}
