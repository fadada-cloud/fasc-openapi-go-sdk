package approvalResponseModel

type GetApprovalFlowListRes struct {
	RequestId string                    `json:"requestId"`
	Code      string                    `json:"code"`
	Msg       string                    `json:"msg"`
	Data      []GetApprovalFlowListData `json:"data"`
}

type GetApprovalFlowListData struct {
	ApprovalType     string `json:"approvalType"`
	ApprovalFlowId   string `json:"approvalFlowId"`
	ApprovalFlowName string `json:"approvalFlowName"`
	Description      string `json:"description"`
	Status           string `json:"status"`
}
