package approvalResponseModel

type GetApprovalDetailRes struct {
	RequestId string                `json:"requestId"`
	Code      string                `json:"code"`
	Msg       string                `json:"msg"`
	Data      GetApprovalDetailData `json:"data"`
}

type GetApprovalDetailData struct {
	ApprovalType      string         `json:"approvalType,omitempty"`
	TemplateId        string         `json:"templateId,omitempty"`
	SignTaskId        string         `json:"signTaskId,omitempty"`
	ApprovalId        string         `json:"approvalId,omitempty"`
	ApprovalName      string         `json:"approvalName,omitempty"`
	ApplicantName     string         `json:"applicantName,omitempty"`
	ApplicantMemberId string         `json:"applicantMemberId,omitempty"`
	ApprovalStatus    string         `json:"approvalStatus,omitempty"`
	ApprovalNode      []ApprovalNode `json:"approvalNode,omitempty"`
}

type ApprovalNode struct {
	NodeId         int64            `json:"nodeId,string"`
	ApprovalType   string           `json:"approvalType,omitempty"`
	NodeStatus     string           `json:"nodeStatus,omitempty"`
	ApproversInfos []ApproversInfos `json:"approversInfos,omitempty"`
}

type ApproversInfos struct {
	ApproverMemberName string `json:"approverMemberName,omitempty"`
	ApproverMemberId   int64  `json:"approverMemberId,string"`
	ApproverStatus     string `json:"approverStatus,omitempty"`
	OperateTime        string `json:"operateTime,omitempty"`
	RejectNote         string `json:"rejectNote,omitempty"`
}
