package callbackResponseModel

type GetCallBackListRes struct {
	RequestId string              `json:"requestId"`
	Code      string              `json:"code"`
	Msg       string              `json:"msg"`
	Data      GetCallBackListData `json:"data"`
}

type GetCallBackListData struct {
	CallbackInfos []CallbackInfos `json:"callbackInfos"`
	TotalCount    int             `json:"totalCount"`
	ListPageCount int             `json:"listPageCount"`
	ListPageNo    int             `json:"listPageNo"`
	CountInPage   int             `json:"countInPage"`
}
type CallbackInfos struct {
	EventId   string `json:"eventId,omitempty"`
	EventInfo string `json:"eventInfo,omitempty"`
	Success   bool   `json:"success"`
}
