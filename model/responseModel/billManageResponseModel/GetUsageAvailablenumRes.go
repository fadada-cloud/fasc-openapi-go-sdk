package billManageResponseModel

type GetUsageAvailablenumRes struct {
	RequestId string                   `json:"requestId"`
	Code      string                   `json:"code"`
	Msg       string                   `json:"msg"`
	Data      GetUsageAvailablenumData `json:"data"`
}

type GetUsageAvailablenumData struct {
	UsageCode        string             `json:"usageCode"`
	UsageName        string             `json:"usageName"`
	UsageUnit        string             `json:"usageUnit"`
	AvailableNum     string             `json:"availableNum"`
	UsageShareDetail []UsageShareDetail `json:"usageShareDetail"`
}

type UsageShareDetail struct {
	AmountUsable    string `json:"amountUsable"`
	BillAccountName string `json:"billAccountName"`
}
