package appResponseModel

type GetAppOpenIdListRes struct {
	RequestId string               `json:"requestId"`
	Code      string               `json:"code"`
	Msg       string               `json:"msg"`
	Data      GetAppOpenIdListData `json:"data"`
}

type GetAppOpenIdListData struct {
	OpenIdInfos   []OpenIdInfo `json:"openIdInfos"`
	ListPageNo    int          `json:"listPageNo"`
	CountInPage   int          `json:"countInPage"`
	ListPageCount int          `json:"listPageCount"`
	TotalCount    int          `json:"totalCount"`
}

type OpenIdInfo struct {
	Name            string `json:"name"`
	OpenId          string `json:"openId"`
	ClientId        string `json:"clientId"`
	AvailableStatus string `json:"availableStatus"`
}
