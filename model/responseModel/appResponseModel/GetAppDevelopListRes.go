package appResponseModel

type GetAppDevelopListRes struct {
	RequestId string          `json:"requestId"`
	Code      string          `json:"code"`
	Msg       string          `json:"msg"`
	Data      []GetAppDevelop `json:"data"`
}
type GetAppDevelop struct {
	OpenCorpId string `json:"openCorpId"`
	AppId      string `json:"appId"`
	AppStatus  string `json:"appStatus"`
}
