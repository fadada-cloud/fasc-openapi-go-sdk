package commonModel

type FieldMultiCheckbox struct {
	Required     bool   `json:"required"`
	Option       string `json:"option"`
	DefaultValue []bool `json:"defaultValue,omitempty"`
}
