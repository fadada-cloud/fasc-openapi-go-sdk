package commonModel

type SignConfigInfo struct {
	OrderNo           int               `json:"orderNo,omitempty"`
	RequestMemberSign bool              `json:"requestMemberSign"`
	VerifyMethods     []string          `json:"verifyMethods,omitempty"`
	AudioVideoInfo    []string          `json:"audioVideoInfo,omitempty"`
	ReadingToEnd      bool              `json:"readingToEnd"`
	ReadingTime       string            `json:"readingTime,omitempty"`
	FreeLogin         bool              `json:"freeLogin"`
	BlockHere         bool              `json:"blockHere"`
	RequestVerifyFree bool              `json:"requestVerifyFree"`
	SignerSignMethod  string            `json:"signerSignMethod,omitempty"`
	JoinByLink        bool              `json:"joinByLink"`
	IdentifiedView    bool              `json:"identifiedView"`
	ResizeSeal        bool              `json:"resizeSeal"`
	ViewCompletedTask bool              `json:"viewCompletedTask"`
	FreeDragSealId    *int64            `json:"freeDragSealId"`
	ActorAttachInfos  []ActorAttachInfo `json:"actorAttachInfos,omitempty"`
	SignAllDoc        bool              `json:"signAllDoc,omitempty"`
	AuthorizeFreeSign bool              `json:"authorizeFreeSign"`
}

type ActorAttachInfo struct {
	ActorAttachName string `json:"actorAttachName,omitempty"`
	Required        string `json:"required"`
}
