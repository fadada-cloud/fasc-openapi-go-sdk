package commonModel

// Actor 签署任务参与方
type Actor struct {
	ActorType         string            `json:"actorType,omitempty"`
	ActorId           string            `json:"actorId,omitempty"`
	ActorName         string            `json:"actorName,omitempty"`
	Permissions       []string          `json:"permissions,omitempty"`
	ActorOpenId       string            `json:"actorOpenId,omitempty"`
	ClientUserId      string            `json:"clientUserId,omitempty"`
	AuthScopes        []string          `json:"authScopes,omitempty"`
	ActorFDDId        string            `json:"actorFDDId,omitempty"`
	IdentNameForMatch string            `json:"identNameForMatch,omitempty"`
	CertType          string            `json:"certType,omitempty"`
	CertNoForMatch    string            `json:"certNoForMatch,omitempty"`
	AccountName       string            `json:"accountName,omitempty"`
	AccountEditable   bool              `json:"accountEditable"`
	ActorCorpMember   []ActorCorpMember `json:"actorCorpMembers,omitempty"`
	ActorEntityId     string            `json:"actorEntityId,omitempty"`
	SendNotification  bool              `json:"sendNotification"`
	NotifyType        []string          `json:"notifyType"`
	NotifyAddress     string            `json:"notifyAddress"`
	Notification      *Notification     `json:"notification,omitempty"`
}
