package commonModel

type Watermark struct {
	Type         string `json:"type"`
	PicBase64    string `json:"picBase64"`
	PicFileId    string `json:"picFileId"`
	PicWidth     int    `json:"picWidth"`
	PicHeight    int    `json:"picHeight"`
	Content      string `json:"content"`
	FontSize     int    `json:"fontSize"`
	FontColor    string `json:"fontColor"`
	Rotation     int    `json:"rotation"`
	Transparency int    `json:"transparency"`
	Position     string `json:"position"`
	Density      string `json:"density"`
}
