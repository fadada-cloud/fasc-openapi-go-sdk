package commonModel

type FieldDateSign struct {
	DateFormat string `json:"dateFormat,omitempty"`
	FontSize   int    `json:"fontSize"`
}
